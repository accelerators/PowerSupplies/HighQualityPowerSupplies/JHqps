# Project JHqps

Maven Java project

Java application to control HQPS (High quality Power Supply).
This application has been written by Olivier Goudard (PSG).


## Cloning

```
git clone git@gitlab.esrf.fr:Accelerator-PowerSupplies/JHqps
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Jive.jar
* tool_panels.jar
* MainsAnalyzerSystemGUI.jar
* JTango.jar
* ATKCore.jar
* ATKWidget.jar
* atkpanel.jar
  

#### Toolchain Dependencies 

* javac 8 or higher
* maven
  

### Build


Instructions on building the project.

```
cd JHqps
mvn package
```

