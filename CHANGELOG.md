# Changelog

#### JHqps-3.3 - 26/09/23:
    set caret on top for hqps History

#### JHqps-3.2 - 04/09/23:
    Change attribute for power display in main jdraw process/facility
    add new Main Analysis in menu Application

#### JHqps-3.1 - 03/05/19:
    Read history fron HDB++ (TACO hdb does not exist any more !)

#### JHqps-3.0 - 07/03/19:
    Change device domain name according to the new device naming convention.
    
#### JHqps-2.2 - 07/03/19:
    Improve error message at startup

#### JHqps-2.1 - 06/02/18:
    Fix a bug in JDraw file for new package name 

#### JHqps-2.0 - 31/01/18:
    Initial Revision on GIT
