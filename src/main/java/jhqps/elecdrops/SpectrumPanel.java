//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/SpectrumPanel.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: SpectrumPanel.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:19  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:51  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:02  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
/**
 * SpectrumPanel.java
 * <p>
 * Created on June 11, 2002, 9:42 AM
 */
package jhqps.elecdrops;

/**
 * @author goudard
 */

//	Import Standart Classes
//=========================

import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//	Import ATK Classes
//====================

public class SpectrumPanel extends JPanel implements ElecDropDefs, IJLChartListener {
    private JScrollPane jScrollPane1;
    private JPanel jPanel1;
    private JTable jTable1;
    private JCheckBox displayTheoChB;
    private JCheckBox displayErrorChB;
    private NumberSpectrumViewer lastDropChart;
    private JLDataView theoDataPh1;
    private JLDataView theoDataPh2;
    private JLDataView theoDataPh3;
    private JLDataView errorCurvePh1;
    private JLDataView errorCurvePh2;
    private JLDataView errorCurvePh3;
    //private NumberSpectrumViewer 		numberSpectrumViewer1;
    //private NumberImageJTableAdapter 	numberImageJTableAdapter1;
    private JLabel deltav;
    //  Phase calculation variables
    private double fitted_phase;
    private double[] d;
    private double[] errResult;
    private double[][] theo;
    private double[][] fitTable;
    private int best_phi;
    private int phase = 0;
    /*-------------------------------------------------------------------------*/

    /**
     * Creates new form SpectrumPanel .
     */
    /*-------------------------------------------------------------------------*/
    public SpectrumPanel() {

    }

    /*-------------------------------------------------------------------------*/

    /**
     * Creates new form SpectrumPanel to display a NumberSpectrum attribute.
     */
    /*-------------------------------------------------------------------------*/
    public SpectrumPanel(INumberSpectrum nsAtt, int phaseNumber) {
        //Retreive phase number
        phase = phaseNumber;

        initTheoricalPhase(nsAtt, phaseNumber);
        initTheoricalCurve(nsAtt, phaseNumber);
        initComponents(phaseNumber);
        lastDropChart.setModel(nsAtt);
        //numberImageJTableAdapter1.setViewer(jTable1);
        //numberSpectrumViewer1.setModel(nsAtt);
        //numberImageJTableAdapter1.setModel(nsAtt);
        jTable1.setVisible(false);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public static void main(String[] args) {
        new SpectrumPanel().setVisible(true);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void initTheoricalPhase(INumberSpectrum nsAtt, int phaseNumber) {
        d = new double[nsAtt.getXDimension()];

        fitTable = new double[nsAtt.getXDimension()][1000];
        errorCurvePh1 = new JLDataView();
        errorCurvePh2 = new JLDataView();
        errorCurvePh3 = new JLDataView();
        errorCurvePh1.setColor(Color.magenta);
        errorCurvePh2.setColor(Color.magenta);
        errorCurvePh3.setColor(Color.magenta);
        errorCurvePh1.setName("Error curve");
        errorCurvePh2.setName("Error curve");
        errorCurvePh3.setName("Error curve");

        // Getting parctical data
        nsAtt.refresh();
        d = nsAtt.getSpectrumValue();

        //  Caluclating phase of recorded data
        double add = 0;
        errResult = new double[1000];
        for (int phi = 0 ; phi<1000 ; phi++) {
            for (int i = 0 ; i<nsAtt.getXDimension() ; i++) {
                fitTable[i][phi] = Math.abs((d[i] * Math.sqrt(2)) -
                        20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * i * 50 / 1000 * 0.8 + (2 * Math.PI * phi / 1000)));
                add += fitTable[i][phi];
            }
            errResult[phi] = add;
            add = 0;
        }

        // Search phase
        double min = errResult[0];
        best_phi = 0;
        for (int phi = 0 ; phi<1000 ; phi++) {
            if (errResult[phi]<min) {
                min = errResult[phi];
                best_phi = phi;
            }
        }
        fitted_phase = 2 * Math.PI * best_phi / 1000;
        if (ElecDropsGUI.trace)
            System.out.println("Fitted phase " + phaseNumber + " = " + fitted_phase + " radians.");
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void initTheoricalCurve(INumberSpectrum nsAtt, int phaseNumber) {
        theoDataPh1 = new JLDataView();
        theoDataPh2 = new JLDataView();
        theoDataPh3 = new JLDataView();

        theoDataPh1.setColor(Color.black);
        theoDataPh2.setColor(Color.black);
        theoDataPh3.setColor(Color.black);
        theoDataPh1.setName("Theorical analogic values");
        theoDataPh2.setName("Theorical analogic values");
        theoDataPh3.setName("Theorical analogic values");

        //  Fill a table to calculate error
        theo = new double[3][nsAtt.getXDimension()];

        //  Filling theorical curve
        for (int i = 1 ; i<nsAtt.getXDimension() ; i++) {
            double yPh1 = 20000 * Math.sqrt(2) * Math.sin((2 * Math.PI * 50 * i / 1000 * 0.8) + fitted_phase);
            double yPh2 = 20000 * Math.sqrt(2) * Math.sin((2 * Math.PI * 50 * i / 1000 * 0.8) + fitted_phase);
            double yPh3 = 20000 * Math.sqrt(2) * Math.sin((2 * Math.PI * 50 * i / 1000 * 0.8) + fitted_phase);
            // /1000->0.8ms because, in this mode, the dsp take 1 point  every 160 micro
            // and 10 period and then read 1 point on 5 160*5=800 micro
            theo[0][i] = yPh1;
            theo[1][i] = yPh2;
            theo[2][i] = yPh3;
            theoDataPh1.add((double) i * 0.8, yPh1);
            theoDataPh2.add((double) i * 0.8, yPh2);
            theoDataPh3.add((double) i * 0.8, yPh3);

            //  Trace error
            if (phaseNumber == 1)
                errorCurvePh1.add((double) i * 0.8, fitTable[i][best_phi]);
            if (phaseNumber == 2)
                errorCurvePh2.add((double) i * 0.8, fitTable[i][best_phi]);
            if (phaseNumber == 3)
                errorCurvePh3.add((double) i * 0.8, fitTable[i][best_phi]);
        }
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void initComponents(int phaseNumber) {
        jScrollPane1 = new JScrollPane();
        jPanel1 = new JPanel();
        jTable1 = new JTable();
        lastDropChart = new NumberSpectrumViewer();
        displayTheoChB = new JCheckBox("Display theorical curve", true);
        displayErrorChB = new JCheckBox("Display error", true);
        deltav = new JLabel("\u0394U/U = -.- volts");
        //numberSpectrumViewer1 		= new NumberSpectrumViewer();
        //numberImageJTableAdapter1 	= new NumberImageJTableAdapter();

        setLayout(new GridBagLayout());

        lastDropChart.setXAxisAffineTransform(0.0, 0.8);
        lastDropChart.setXAxisUnit("ms");
        lastDropChart.setBackground(Color.white);
        lastDropChart.setChartBackground(new Color(238, 232, 170));
        lastDropChart.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        lastDropChart.setToolTipText("Click on chart to display values.");
        lastDropChart.getXAxis().setGridVisible(true);
        lastDropChart.getXAxis().setSubGridVisible(true);
        lastDropChart.getXAxis().setName("Time (ms)");
        lastDropChart.getY1Axis().setAutoScale(true);
        lastDropChart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        lastDropChart.getY1Axis().setGridVisible(true);
        lastDropChart.getY1Axis().setSubGridVisible(true);
        lastDropChart.getY1Axis().setName("Phase voltage (V)");
        lastDropChart.setJLChartListener(this);

        //  Setting sqrt2 factor to all the data of Inumberspectrum
        lastDropChart.getY1Axis().getDataView(0).setA1(Math.sqrt(2));
        double delta_v = lastDropChart.getY1Axis().getMaximum() -
                lastDropChart.getY2Axis().getMaximum();

        if (delta_v>0)
            lastDropChart.setHeader("Over voltage analogic values");
        else
            lastDropChart.setHeader("Drop voltage analogic values");

        lastDropChart.setHeaderFont(new Font("Times", Font.BOLD, 18));
        lastDropChart.setLabelFont(new Font("Times", Font.BOLD, 12));

        if (phaseNumber == 1) {
            lastDropChart.getY1Axis().addDataView(theoDataPh1);
            lastDropChart.getY1Axis().addDataView(errorCurvePh1);
            //  Choose to display other curves or not on Phase 1 Graph
            displayTheoChB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    phase1viewTheoGraphActionPerformed(evt);
                }
            });

            displayErrorChB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    phase1viewErrorGraphActionPerformed(evt);
                }
            });
        }
        if (phaseNumber == 2) {
            lastDropChart.getY1Axis().addDataView(theoDataPh2);
            lastDropChart.getY1Axis().addDataView(errorCurvePh2);
            //  Choose to display other curves or not on Phase 2 Graph
            displayTheoChB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    phase2viewTheoGraphActionPerformed(evt);
                }
            });

            displayErrorChB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    phase2viewErrorGraphActionPerformed(evt);
                }
            });
        }
        if (phaseNumber == 3) {
            lastDropChart.getY1Axis().addDataView(theoDataPh3);
            lastDropChart.getY1Axis().addDataView(errorCurvePh3);
            //  Choose to display other curves or not on Phase 3 Graph
            displayTheoChB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    phase3viewTheoGraphActionPerformed(evt);
                }
            });

            displayErrorChB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    phase3viewErrorGraphActionPerformed(evt);
                }
            });
        }
        lastDropChart.getY1Axis().setAutoScale(true);

        jPanel1.setLayout(new GridBagLayout());
        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.gridx = 0;
        gridBC.gridy = 0;
        gridBC.fill = GridBagConstraints.BOTH;
        gridBC.weightx = 1.0;
        gridBC.weighty = 1.0;
        jPanel1.add(lastDropChart, gridBC);

        //  This creating a Table to display results in table (unuseful because JLChart
        //  is already doing that stuff.

        /*jTable1.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
		{   {null, null, null, null},
            {null, null, null, null},
            {null, null, null, null},
            {null, null, null, null}
        },
		new String [] { "Title 1", "Title 2", "Title 3", "Title 4"})
		{
        	Class[] types = new Class []
			{
                java.lang.Object.class,
				java.lang.Object.class,
				java.lang.Object.class,
				java.lang.Object.class
           	};

        	public Class getColumnClass(int columnIndex)
			{
            	return types [columnIndex];
        	}
        });

        gridBC.gridx = 0;
        gridBC.gridy = 1;
        jPanel1.add(jTable1, gridBC);
        */
        jScrollPane1.setViewportView(jPanel1);
        add(jScrollPane1, gridBC);

        gridBC.weightx = 0.5;
        gridBC.weighty = 0.0;
        gridBC.gridx = 0;
        gridBC.gridy = 1;
        jPanel1.add(displayTheoChB, gridBC);

        gridBC.gridx = 0;
        gridBC.gridy = 2;
        jPanel1.add(displayErrorChB, gridBC);

        gridBC.gridx = 0;
        gridBC.gridy = 3;
        jPanel1.add(deltav, gridBC);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public String[] clickOnChart(JLChartEvent evt) {
        String ret[] = new String[3];

        ret[0] = evt.searchResult.dataView.getName();
        ret[1] = new Double(evt.getTransformedXValue()).toString().toString().substring(0, 3) + " ms";
        ret[2] = new Double(evt.getTransformedYValue()).toString().substring(0, 7) + " Volt";

        //  Displaying dU/U at graph bottom
        if (phase == 1)
            deltav.setText("\u0394U/U = "// \u0394 = small delta greek letter
                    + new Double(errorCurvePh1.getYValueByIndex(evt.searchResult.clickIdx)).toString().substring(0, 7)
                    + " volts");
        if (phase == 2)
            deltav.setText("\u0394U/U = "
                    + new Double(errorCurvePh2.getYValueByIndex(evt.searchResult.clickIdx)).toString().substring(0, 7)
                    + " volts");
        if (phase == 3)
            deltav.setText("\u0394U/U = "
                    + new Double(errorCurvePh3.getYValueByIndex(evt.searchResult.clickIdx)).toString().substring(0, 7)
                    + " volts");
        deltav.repaint();
        return ret;
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void phase1viewTheoGraphActionPerformed(ActionEvent evt) {
        if (displayTheoChB.isSelected())
            lastDropChart.getY1Axis().addDataView(theoDataPh1);
        else
            lastDropChart.getY1Axis().removeDataView(theoDataPh1);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void phase1viewErrorGraphActionPerformed(ActionEvent evt) {
        if (displayErrorChB.isSelected())
            lastDropChart.getY1Axis().addDataView(errorCurvePh1);
        else
            lastDropChart.getY1Axis().removeDataView(errorCurvePh1);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void phase2viewTheoGraphActionPerformed(ActionEvent evt) {
        if (displayTheoChB.isSelected())
            lastDropChart.getY1Axis().addDataView(theoDataPh2);
        else
            lastDropChart.getY1Axis().removeDataView(theoDataPh2);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void phase2viewErrorGraphActionPerformed(ActionEvent evt) {
        if (displayErrorChB.isSelected())
            lastDropChart.getY1Axis().addDataView(errorCurvePh2);
        else
            lastDropChart.getY1Axis().removeDataView(errorCurvePh2);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void phase3viewTheoGraphActionPerformed(ActionEvent evt) {
        if (displayTheoChB.isSelected())
            lastDropChart.getY1Axis().addDataView(theoDataPh3);
        else
            lastDropChart.getY1Axis().removeDataView(theoDataPh3);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void phase3viewErrorGraphActionPerformed(ActionEvent evt) {
        if (displayErrorChB.isSelected())
            lastDropChart.getY1Axis().addDataView(errorCurvePh3);
        else
            lastDropChart.getY1Axis().removeDataView(errorCurvePh3);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void extractData() {

    }
}    //End of class
