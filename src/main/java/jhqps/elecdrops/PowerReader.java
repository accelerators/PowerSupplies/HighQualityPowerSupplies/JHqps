//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/PowerReader.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: PowerReader.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:02  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================

package jhqps.elecdrops;

/**
 * @author goudard
 */

//	Import Tango Classes

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;

/*-------------------------------------------------------------------------*/

/**
 * This class defines Taco device access
 */
/*-------------------------------------------------------------------------*/

public class PowerReader implements ElecDropDefs {

    //	Link to the Tango device server
//=================================
    public static DeviceProxy dev;

    //	Display debugging tips
//========================
    public static boolean trace = false;

    /*-------------------------------------------------------------------------*/

    /**
     * Default constructor :
     * Creates a connection for Taco infra/power/1 Device Server using JTaco API.
     */
    /*-------------------------------------------------------------------------*/
    public PowerReader() throws DevFailed {
        try {
            dev = new DeviceProxy(ss1_power_devname);
        } catch (DevFailed e) {
            if (ElecDropsGUI.trace)
                Except.print_exception(e);
            dev = null;
        }
    }
    /*-------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------*/
/*  Main Program
/*-------------------------------------------------------------------------*/
    public static void main(String[] args) {

	/* Search for -trace argument
	-----------------------------*/
        for (int i = 1 ; i<=args.length ; i++) {
            if (args[i - 1].compareTo("-trace") == 0)
                trace = true;
            else
                System.out.println("Bad argument in the command line.");
        }
        try {
            PowerReader gt = new PowerReader();
            gt.readPowers();
        } catch (DevFailed e) {
            System.out.println("Unable to access device server.");
        }

    }

    /**
     * returns active power, apparent power and cos phi values on electrical networks.
     */
    /*-------------------------------------------------------------------------*/
    public short[] readPowers() throws DevFailed {
        short[] result = new short[3];
        if (dev != null) {
            try {
                DeviceData argout = new DeviceData();
                ;
                //CommandInfo cmdCode = dev.commandQuery("DevReadValues");
                DeviceData ret = dev.command_inout("DevReadValues", argout);
                result = ret.extractShortArray();
            } catch (DevFailed e) {
                ElecDropsGUI.trace = true;
                System.out.println("command_inout failed.");
                if (ElecDropsGUI.trace)
                    Except.print_exception(e);
            }
            if (trace) {
                for (int i = 0 ; i<result.length ; i++)
                    System.out.println("result[" + i + "]= " + result[i]);
            }
            return result;
        } else
            return result;
    }
}
