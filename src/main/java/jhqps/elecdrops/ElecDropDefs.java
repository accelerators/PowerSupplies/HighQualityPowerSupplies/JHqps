//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/ElecDropDefs.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: ElecDropDefs.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.elecdrops;

public interface ElecDropDefs {

    public final boolean ReadDsp = false;
    public final boolean WriteDsp = true;

    //	JList Constant
    //=================
    public final int FISRT_ELEMENT = 0;

    //	State constants
    //=================
    public final int NOTOK = -1;
    public final int OK = 0;
    public final int STATE_OK = 0;
    public final int STATE_ALARM = 1;
    public final int STATE_FAULTY = 2;
    public final int STATE_UNKNOWN = 3;
    public final int NB_STATES = STATE_UNKNOWN + 1;
    // Analogic values modes constants
    //================================
    public final short SETMODE_LIVE = 0;
    public final short SETMODE_10PERIOD = 1;
    public final short SETMODE_2PERIOD = 2;

    //	DPRAM mapping
    //===============
    public final int MILLI_SEC = 2;
    public final int NB_PHASES = 3;
    public final int RMS = 3;
    public final int FREQ = 6;
    public final int MAX_NB_DROP = 6;
    public final int MEAN = 9;
    public final int NB_PARAM = 19;
    public final int DURATION = 12;
    public final int OVERFLOW = 15;
    public final int CODING = 19;
    public final int ONE_DROP_NB_DATA = 19;
    public final int TRIG_LEVELS = 12;
    public final int NUMBER_OF_DROPS = 20;
    public final int NEW_DROPS_OFFSET = 21;
    public final int DROP1_OFFSET = 24;
    public final int DROP2_OFFSET = 43;
    public final int DROP3_OFFSET = 62;
    public final int DROP4_OFFSET = 81;
    public final int DROP5_OFFSET = 100;
    public final int DROP6_OFFSET = 119;
    public final int NB_TRIG_LEVELS = 8;

    //	Time constants
    //================
    public final int ONE_SECOND = 1000;
    public final int THREE_SECOND = 3000;

    // Number of drops to read
    //========================
    public final short ONE_DROP = 1;
    public final short TWO_DROPS = 2;
    public final short THREE_DROPS = 3;


	/*public final File fid = new File (	File.separator+"tango"+
										File.separator+"jclient"+
										File.separator+"elecdrops"+
										File.separator+"drops0.log");

*/
    //"drops0.log";
    //"/segfs/tango/jclient/elecdrops/drops0.log";
    //"G:/tango/jclient/elecdrops/drops0.log";
    //	Device name on Main Building cluster
    //======================================
    //public final String devname = "pf/elec-drop/1";

    /**
     *	Device name of the TANGO C++ server to access DPRAM values
     */
    //public final String devname = "infra/d-drops/ss1";

    /**
     * Device name of the TANGO java Logger to record drops from DPRAM to a File
     */
    //public final String logname = "infra/log-drops/ss1";

    /**
     * Power device to be imported in the Live Mode Frame.
     */
    public final String ss1_power_devname = "infra/power/ss1";

    /**
     * Message for the About textArea
     */
    public final String aboutMessage =
            "DROP DETECTION PRINCIPLE\n\n" +
                    "VDSP VME board reads an image of 20kV.\n" +
                    "The vdsp71_cs TACO device server reads values\n" +
                    "of the 20kV on the VDSP board.\n" +
                    "The Elecdrop TANGO device server read the values\n" +
                    "sent by the TACO Device server.\n\n" +
                    "This application read the 20kV image to detect drops,\n" +
                    "and display them on a panel.\n\n" +
                    "Sources of the client application are located in /segfs/tango/jclient/elecdrops\n" +
                    "Sources of the java logger are located in /segfs/tango/jserver/ElecLogger\n" +
                    "Sources of the cpp server are located in /segfs/tango/cppserver/machine/powersupply/elecdrops/\n";

    public final String[] loggerHeader = {
            "*** WARNING! DO NOT ERASE ANY DATA ***\n",
            "You are editing the logging file for Electric Drops detection on 20kV electrical network..\n",
            "\n_________________________________________________________________________________________________________________________________",
            "\nDate		Time			RMS1	RMS2	RMS3	Freq1	Freq2	Freq3	Coding	Duration	UnixTime	Mean1	Mean2	Mean3 ",
            "\n__________________________________________________________________________________________________________________________________"
    };
    public final String TrendSettings = "graph_title:'DFT History' " +
            "label_visible:true " +
            "label_placement:0 " +
            "label_font:Dialog,0,12 " +
            "chart_background:255,255,153 " +
            "title_font:Dialog,0,12 " +
            "display_duration:300000.0 " +
            "precision:0 " +
            "toolbar_visible:false " +
            "tree_visible:false " +
            "window_pos:5,260 " +
            "window_size:600,300 " +
            "show_device_name:0 " +
            "refresh_time:1000 " +
            "xvisible:true " +
            "xgrid:true " +
            "xsubgrid:false " +
            "xgrid_style:1 " + "toolbar_visible:false " +
            "tree_visible:false " +
            "dv_number:1 " +
            "dv0_name:'infra/d-drops/ss1/DFTLive' " +
            "dv0_selected:2 " +
            "dv0_linecolor:255,0,0 ";

    /**
     * Coefficient to convert to 20kV (20000/13530 ->see Test6.h)
     */
    double coeff = 1.478196;
    public final int UNIX_TIME_OFFEST = 18;
    public final int FILE_NB_DATA = 19;
    public final int PH1_RMS = 0x0001;
    /**
     * < Coding for drop RMS on phase 1
     */
    public final int PH1_PERIOD = 0x0002;
    /**
     * < Coding for drop on period phase 1
     */
    public final int PH2_RMS = 0x0010;
    /**
     * <Coding for drop RMS on phase 2
     */
    public final int PH2_PERIOD = 0x0020;
    /**
     * < Coding for drop on period phase 2
     */
    public final int PH3_RMS = 0x0100;
    /**
     * <Coding for drop RMS on phase 3
     */
    public final int PH3_PERIOD = 0x0200;        /**< Coding for drop on period phase 3	*/

}// End of Interface