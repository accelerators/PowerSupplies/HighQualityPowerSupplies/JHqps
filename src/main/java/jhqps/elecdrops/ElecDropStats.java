//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/ElecDropStats.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: ElecDropStats.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.elecdrops;

//	Import Standard Java Classes
//==============================

import fr.esrf.tangoatk.widget.util.chart.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ElecDropStats extends JFrame implements ElecDropDefs, IJLChartListener {
    final JLDataView[] DistriDv = new JLDataView[3];
    final JLDataView[] DuOnUDv = new JLDataView[7];
    final JLDataView[] DurationDv = new JLDataView[9];
    final JLDataView DuUtDv = new JLDataView();
    final JLDataView HQPSilotage = new JLDataView();
    public float max_duration = 0;
    public double[] all_delta_u_on_u;
    public double[] all_durations;
    public jhqps.elecdrops.Drop[] local_drops;
    //  Distribution Legend Labels
    private JLabel distriLegend1Lbl;
    private JLabel distriLegend2Lbl;
    private JLabel distriLegend3Lbl;
    private JLabel distrisigName1Lbl;
    private JLabel distrisigName2Lbl;
    private JLabel dsitrisigName3Lbl;
    //  Duration Legend Labels
    private JLabel[] durationLegendLbl = new JLabel[9];
    private JLabel[] durationsigNameLbl = new JLabel[9];
    //  DU/U Legend Labels
    private JLabel[] duOnuLegendLbl = new JLabel[7];
    private JLabel[] duOnusigNameLbl = new JLabel[7];
    private JPanel DuOnULegendPanel;
    private JPanel DistriLegendPanel;
    private JPanel DurationLegendPanel;
    //  GUI Panels
    private JPanel DuOnUPl;
    private JPanel DurationPl;
    //private JPanel  	NorthPl;
    private JPanel SouthPl;
    private JPanel DistriPl;
    private JPanel DuUtPl;
    //  Charts & Dataviews
    private JLChart DistriChart;
    private JLChart DuOnUChart;
    private JLChart DurationChart;
    private JLChart DuUtChart;
    private JButton CloseBtn;

    /**
     * Constructor for drop statistic window.
     */
    public ElecDropStats(jhqps.elecdrops.Drop[] the_drops) {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        local_drops = the_drops;
        all_delta_u_on_u = new double[local_drops.length * 3];
        all_durations = new double[local_drops.length * 3];
        importDropList(local_drops);
        analyse(local_drops);
        initComponents();
        pack();
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    /**
     * Analyse all the data of the logger file.
     */
    private void analyse(jhqps.elecdrops.Drop[] local_drops) {
        int[] distrib = getDistribution(local_drops);
        int[] durations = getDurations(local_drops);
        int[] duOnu = getDropValues(local_drops);
        createCharts(distrib, durations, duOnu);
    }

    /**
     * Graphical components initialisation.
     */
    private void initComponents() {
        setTitle("Drop Statistics");
        setBackground(Color.white);
        setLocation(200, 200);

        CloseBtn = new JButton("Close");
        //   NorthPl = new JPanel();
        DistriPl = new JPanel();
        DuUtPl = new JPanel();
        DuOnUPl = new JPanel();
        DurationPl = new JPanel();
        SouthPl = new JPanel();
        DistriChart = new JLChart();
        DuOnUChart = new JLChart();
        DurationChart = new JLChart();
        DuUtChart = new JLChart();
        distriLegend1Lbl = new JLabel();
        distriLegend2Lbl = new JLabel();
        distriLegend3Lbl = new JLabel();
        distrisigName1Lbl = new JLabel("Single phase drops", SwingConstants.LEFT);
        distrisigName2Lbl = new JLabel("Two-phase drops", SwingConstants.LEFT);    //TODO : Verify why label alignment is bad
        dsitrisigName3Lbl = new JLabel("Three-phase drops", SwingConstants.LEFT);

        //  Distribution chart setup
        DistriChart.setHeader("Distribution");
        DistriChart.setHeaderVisible(true);
        DistriChart.setSize(300, 300);
        DistriChart.setBackground(Color.white);
        DistriChart.setChartBackground(new Color(238, 232, 170));
        DistriChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        DistriChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        DistriChart.getY1Axis().setName("Events");
        DistriChart.getY1Axis().setAutoScale(true);
        DistriChart.getY1Axis().setMinimum(0);
        DistriChart.getY1Axis().setGridVisible(true);
        DistriChart.getY1Axis().setSubGridVisible(true);
        DistriChart.getXAxis().setName("Type of drop");
        DistriChart.setPreferredSize(new Dimension(300, 300));
        DistriChart.getXAxis().setAutoScale(false);
        DistriChart.getXAxis().setMaximum(4);
        DistriChart.getXAxis().setMinimum(0);
        DistriChart.getY1Axis().setLabelFormat(JLAxis.DECINT_FORMAT);
        //  Set clickOnChart local method (override)
        DistriChart.setJLChartListener(this);

        //  Distribution dataview setup
        for (int i = 0 ; i<3 ; i++) {
            DistriDv[i].setUnit("Event");
            DistriDv[i].setLineWidth(1);
            DistriDv[i].setBarWidth(10);
            DistriDv[i].setFillStyle(JLDataView.FILL_STYLE_SOLID);
            DistriDv[i].setColor(Color.black);
            DistriDv[i].setViewType(JLDataView.TYPE_BAR);
            DistriDv[i].setLabelVisible(false);
            DistriChart.getY1Axis().addDataView(DistriDv[i]);
        }

        DistriDv[0].setFillColor(Color.orange);
        DistriDv[1].setFillColor(Color.blue);
        DistriDv[2].setFillColor(Color.red);
        DistriChart.setVisible(true);

        // Distrbution chart legend setup
        distriLegend1Lbl.setOpaque(true);
        distriLegend2Lbl.setOpaque(true);
        distriLegend3Lbl.setOpaque(true);
        distriLegend1Lbl.setPreferredSize(new Dimension(25, 10));
        distriLegend2Lbl.setPreferredSize(new Dimension(25, 10));
        distriLegend3Lbl.setPreferredSize(new Dimension(25, 10));
        distriLegend1Lbl.setBackground(Color.orange);
        distriLegend2Lbl.setBackground(Color.blue);
        distriLegend3Lbl.setBackground(Color.red);
        distriLegend1Lbl.setBorder(BorderFactory.createLineBorder(Color.black));
        distriLegend2Lbl.setBorder(BorderFactory.createLineBorder(Color.black));
        distriLegend3Lbl.setBorder(BorderFactory.createLineBorder(Color.black));
        distrisigName1Lbl.setFont(new Font("Dialog", Font.BOLD, 10));
        distrisigName2Lbl.setFont(new Font("Dialog", Font.BOLD, 10));
        dsitrisigName3Lbl.setFont(new Font("Dialog", Font.BOLD, 10));

        // DuOnu chart setup
        DuOnUChart.setHeader("\u0394U/U");
        DuOnUChart.setHeaderVisible(true);
        DuOnUChart.setBackground(Color.white);
        DuOnUChart.setChartBackground(new Color(238, 232, 170));
        DuOnUChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        DuOnUChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        DuOnUChart.getY1Axis().setName("Events");
        DuOnUChart.getY1Axis().setAutoScale(true);
        DuOnUChart.getY1Axis().setMinimum(0);
        DuOnUChart.getY1Axis().setGridVisible(true);
        DuOnUChart.getY1Axis().setSubGridVisible(true);
        DuOnUChart.getXAxis().setName("\u0394Urms/Urms (%)");
        DuOnUChart.getXAxis().setGridVisible(true);
        DuOnUChart.getXAxis().setAutoScale(false);
        DuOnUChart.getXAxis().setMinimum(0);
        DuOnUChart.getXAxis().setMaximum(DuOnUDv.length + 1);
        DuOnUChart.setPreferredSize(new Dimension(300, 300));
        //  Set clickOnChart local method (override)
        DuOnUChart.setJLChartListener(this);

        //  Du/u Dataview setup
        for (int i = 0 ; i<DuOnUDv.length ; i++) {
            DuOnUDv[i].setUnit("Event");
            DuOnUDv[i].setLineWidth(1);
            DuOnUDv[i].setBarWidth(10);
            DuOnUDv[i].setFillStyle(JLDataView.FILL_STYLE_SOLID);
            DuOnUDv[i].setColor(Color.black);
            DuOnUDv[i].setViewType(JLDataView.TYPE_BAR);
            DuOnUDv[i].setLabelVisible(false);
            DuOnUChart.getY1Axis().addDataView(DuOnUDv[i]);
        }
        DuOnUDv[0].setFillColor(Color.orange);
        DuOnUDv[1].setFillColor(Color.cyan);
        DuOnUDv[2].setFillColor(Color.blue);
        DuOnUDv[3].setFillColor(Color.green);
        DuOnUDv[4].setFillColor(Color.magenta);
        DuOnUDv[5].setFillColor(Color.pink);
        DuOnUDv[6].setFillColor(Color.red);
        // DeltaU/U chart legend setup
        for (int i = 0 ; i<DuOnUDv.length ; i++) {
            duOnuLegendLbl[i] = new JLabel();
            duOnuLegendLbl[i].setOpaque(true);
            duOnuLegendLbl[i].setPreferredSize(new Dimension(25, 10));
            duOnuLegendLbl[i].setBorder(BorderFactory.createLineBorder(Color.black));
            duOnusigNameLbl[i] = new JLabel();
            duOnusigNameLbl[i].setHorizontalTextPosition(SwingConstants.LEFT);
            duOnusigNameLbl[i].setFont(new Font("Dialog", Font.BOLD, 10));
        }

        duOnuLegendLbl[0].setBackground(Color.orange);
        duOnuLegendLbl[1].setBackground(Color.cyan);
        duOnuLegendLbl[2].setBackground(Color.blue);
        duOnuLegendLbl[3].setBackground(Color.green);
        duOnuLegendLbl[4].setBackground(Color.magenta);
        duOnuLegendLbl[5].setBackground(Color.pink);
        duOnuLegendLbl[6].setBackground(Color.red);
        duOnusigNameLbl[0].setText("Less than -5%");
        duOnusigNameLbl[1].setText("-5% to -10%");
        duOnusigNameLbl[2].setText("-10% to -20%");
        duOnusigNameLbl[3].setText("-20% to -40%");
        duOnusigNameLbl[4].setText("-40% to -60%");
        duOnusigNameLbl[5].setText("-60% to -80%");
        duOnusigNameLbl[6].setText("-80% to -100%");
        DuOnUChart.setVisible(true);

        //  Duration chart setup
        DurationChart.setHeader("Duration");
        DurationChart.setSize(300, 300);
        DurationChart.setHeaderVisible(true);
        DurationChart.setBackground(Color.white);
        DurationChart.setChartBackground(new Color(238, 232, 170));
        DurationChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        DurationChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        DurationChart.getY1Axis().setName("Events");
        DurationChart.getY1Axis().setAutoScale(true);
        DurationChart.getY1Axis().setGridVisible(true);
        DurationChart.getY1Axis().setSubGridVisible(true);
        DurationChart.getXAxis().setAutoScale(false);
        DurationChart.getXAxis().setMaximum(DurationDv.length + 1);
        DurationChart.getXAxis().setName("Drop duration");
        DurationChart.getXAxis().setGridVisible(true);
        DurationChart.setPreferredSize(new Dimension(300, 300));

        //  Set clickOnChart local method (override)
        DurationChart.setJLChartListener(this);

        //  Du/u(t) chart setup
        DuUtChart.setHeader("\u0394Urms/Urms vs Time");
        DuUtChart.setSize(300, 300);
        DuUtChart.setHeaderVisible(true);
        DuUtChart.setBackground(Color.white);
        DuUtChart.setChartBackground(new Color(238, 232, 170));
        DuUtChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        DuUtChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        DuUtChart.getY1Axis().setName("\u0394Urms/Urms (%)");
        DuUtChart.getY1Axis().setAutoScale(false);
        DuUtChart.getY1Axis().setMaximum(10);
        DuUtChart.getY1Axis().setMinimum(-100);
        DuUtChart.getY2Axis().setMaximum(100);
        DuUtChart.getY2Axis().setMinimum(-10);
        DuUtChart.getY1Axis().setGridVisible(true);
        DuUtChart.getY1Axis().setSubGridVisible(true);
        DuUtChart.getXAxis().setAutoScale(false);
        DuUtChart.getXAxis().setMaximum(5000);
        DuUtChart.getXAxis().setMinimum(0);
        DuUtChart.getXAxis().setName("Time (ms)");
        DuUtChart.getXAxis().setGridVisible(true);
        DuUtChart.getXAxis().setSubGridVisible(true);
        DuUtChart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        DuUtChart.setPreferredSize(new Dimension(300, 300));
        DuUtChart.setJLChartListener(this);

        //  Duration Dataview setup
        for (int i = 0 ; i<DurationDv.length ; i++) {
            DurationDv[i].setUnit("Event");
            DurationDv[i].setLineWidth(1);
            DurationDv[i].setBarWidth(10);
            DurationDv[i].setFillStyle(JLDataView.FILL_STYLE_SOLID);
            DurationDv[i].setColor(Color.black);
            DurationDv[i].setViewType(JLDataView.TYPE_BAR);
            DurationDv[i].setLabelVisible(false);
            DurationChart.getY1Axis().addDataView(DurationDv[i]);
        }
        DurationDv[0].setFillColor(Color.orange);
        DurationDv[1].setFillColor(Color.cyan);
        DurationDv[2].setFillColor(Color.blue);
        DurationDv[3].setFillColor(Color.green);
        DurationDv[4].setFillColor(Color.magenta);
        DurationDv[5].setFillColor(Color.pink);
        DurationDv[6].setFillColor(Color.red);
        DurationDv[7].setFillColor(Color.gray);
        DurationDv[8].setFillColor(Color.white);


        // Duration chart legend setup
        for (int i = 0 ; i<DurationDv.length ; i++) {
            durationLegendLbl[i] = new JLabel();
            durationLegendLbl[i].setOpaque(true);
            durationLegendLbl[i].setPreferredSize(new Dimension(25, 10));
            durationLegendLbl[i].setBorder(BorderFactory.createLineBorder(Color.black));
            durationsigNameLbl[i] = new JLabel();
            durationsigNameLbl[i].setHorizontalTextPosition(SwingConstants.LEFT);
            durationsigNameLbl[i].setFont(new Font("Dialog", Font.BOLD, 10));
        }

        durationLegendLbl[0].setBackground(Color.orange);
        durationLegendLbl[1].setBackground(Color.cyan);
        durationLegendLbl[2].setBackground(Color.blue);
        durationLegendLbl[3].setBackground(Color.green);
        durationLegendLbl[4].setBackground(Color.magenta);
        durationLegendLbl[5].setBackground(Color.pink);
        durationLegendLbl[6].setBackground(Color.red);
        durationLegendLbl[7].setBackground(Color.gray);
        durationLegendLbl[8].setBackground(Color.white);


        durationsigNameLbl[0].setText("1ms to 40ms");
        durationsigNameLbl[1].setText("41ms to 80ms");
        durationsigNameLbl[2].setText("81ms to 120ms");
        durationsigNameLbl[3].setText("121ms to 200ms");
        durationsigNameLbl[4].setText("201ms to 360ms");
        durationsigNameLbl[5].setText("361ms to 620ms");
        durationsigNameLbl[6].setText("621ms to 1040ms");
        durationsigNameLbl[7].setText("1041ms to 2080ms");
        durationsigNameLbl[8].setText("2081ms to " + max_duration + "ms");

        DurationChart.setVisible(true);

        double[] y = new double[6000];
        double[] x = new double[6000];

        for (int i = 0 ; i<5000 ; i += 10) {
            y[i] = (double) (80 * (1 - java.lang.Math.pow((double) 10, (double) -i / 200)));
            x[i] = (double) i;
            HQPSilotage.add(x[i], y[i]);
        }
        //HQPSilotage.setData(x,y);
        //HQPSilotage.
        DuUtDv.setUnit("%");
        DuUtDv.setName("\u0394U/U versus Time");
        DuUtDv.setLineWidth(0);
        DuUtDv.setMarker(JLDataView.MARKER_DOT);
        DuUtDv.setLabelVisible(false);
        DuUtChart.setMaxDisplayDuration(5000);
        DuUtChart.getY1Axis().addDataView(DuUtDv);

        HQPSilotage.setColor(Color.BLUE);
        HQPSilotage.setUnit("%");
        HQPSilotage.setName("HQPS isolation limit");
        HQPSilotage.setLineWidth(1);
        //HQPSilotage.setMarker(JLDataView.MARKER_STAR);
        HQPSilotage.setLabelVisible(true);

        DuUtChart.getY2Axis().addDataView(HQPSilotage);
        DuUtChart.setVisible(true);

        CloseBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                CloseBtnActionPerformed(evt);
            }
        });

        //  Placing close button
        SouthPl.setLayout(new BorderLayout());
        SouthPl.add(CloseBtn);

        //  Placing Distri Chart & Legend Panels
        DistriLegendPanel = new JPanel();
        DistriLegendPanel.setMinimumSize(new Dimension(400, 400));
        DistriLegendPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new java.awt.Insets(2, 2, 2, 2);
        c.gridx = 0;
        c.gridy = 0;
        DistriLegendPanel.add(distriLegend1Lbl, c);
        c.gridx = 1;
        DistriLegendPanel.add(distrisigName1Lbl, c);
        c.gridx = 0;
        c.gridy = 1;
        DistriLegendPanel.add(distriLegend2Lbl, c);
        c.gridx = 1;
        DistriLegendPanel.add(distrisigName2Lbl, c);
        c.gridx = 0;
        c.gridy = 2;
        DistriLegendPanel.add(distriLegend3Lbl, c);
        c.gridx = 1;
        DistriLegendPanel.add(dsitrisigName3Lbl, c);
        DistriLegendPanel.setBackground(Color.white);

        DistriPl = new JPanel();
        DistriPl.setLayout(new BorderLayout());
        DistriPl.add(DistriChart, BorderLayout.CENTER);
        DistriPl.add(DistriLegendPanel, BorderLayout.SOUTH);
        //  Placing dU/U Chart and Legend Panels
        DuOnULegendPanel = new JPanel();
        DuOnULegendPanel.setMinimumSize(new Dimension(400, 400));
        DuOnULegendPanel.setLayout(new GridBagLayout());
        DuOnULegendPanel.setBackground(Color.white);
        GridBagConstraints b = new GridBagConstraints();
        b.insets = new java.awt.Insets(2, 2, 2, 2);

        for (int i = 0 ; i<duOnuLegendLbl.length ; i++) {
            b.gridx = 0;
            b.gridy = i;
            DuOnULegendPanel.add(duOnuLegendLbl[i], b);
        }
        for (int i = 0 ; i<duOnuLegendLbl.length ; i++) {
            b.gridx = 1;
            b.gridy = i;
            DuOnULegendPanel.add(duOnusigNameLbl[i], b);
        }
        DuOnUPl = new JPanel();
        DuOnUPl.setMinimumSize(new Dimension(0, 200));
        DuOnUPl.setLayout(new BorderLayout());
        DuOnUPl.add(DuOnUChart, BorderLayout.CENTER);
        DuOnUPl.add(DuOnULegendPanel, BorderLayout.SOUTH);

        //  Placing duration Chart and Legend Panels
        DurationLegendPanel = new JPanel();
        DurationLegendPanel.setMinimumSize(new Dimension(400, 400));
        DurationLegendPanel.setLayout(new GridBagLayout());
        DurationLegendPanel.setBackground(Color.white);
        GridBagConstraints a = new GridBagConstraints();
        a.insets = new java.awt.Insets(2, 2, 2, 2);

        for (int i = 0 ; i<durationLegendLbl.length ; i++) {
            a.gridx = 0;
            a.gridy = i;
            DurationLegendPanel.add(durationLegendLbl[i], a);
        }
        for (int i = 0 ; i<durationLegendLbl.length ; i++) {
            a.gridx = 1;
            a.gridy = i;
            DurationLegendPanel.add(durationsigNameLbl[i], a);
        }
        DurationPl = new JPanel();
        DurationPl.setMinimumSize(new Dimension(0, 200));
        DurationPl.setLayout(new BorderLayout());
        DurationPl.add(DurationChart, BorderLayout.CENTER);
        DurationPl.add(DurationLegendPanel, BorderLayout.SOUTH);

        DuUtPl = new JPanel();
        DuUtPl.setMinimumSize(new Dimension(0, 200));
        DuUtPl.setLayout(new BorderLayout());
        DuUtPl.add(DuUtChart, BorderLayout.CENTER);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(DuUtPl, BorderLayout.NORTH);
        getContentPane().add(DistriPl, BorderLayout.CENTER);
        getContentPane().add(DurationPl, BorderLayout.EAST);
        getContentPane().add(DuOnUPl, BorderLayout.WEST);
        getContentPane().add(SouthPl, BorderLayout.SOUTH);
        setVisible(true);
        pack();
    }

    private void CloseBtnActionPerformed(ActionEvent evt) {
        this.setVisible(false);
    }

    /**
     * Create Charts for ElecDropStats Dialog.
     *
     * @param distrib
     * @param durations
     * @param duOnu
     */
    private void createCharts(int[] distrib, int[] durations, int[] duOnu) {
        //  Filling Distribution DataView
        for (int i = 0 ; i<distrib.length ; i++) {
            DistriDv[i] = new JLDataView();
            DistriDv[i].add(i + 1, (double) distrib[i]);
        }
        //  Filling DU/U DataView
        for (int i = 0 ; i<duOnu.length ; i++) {
            DuOnUDv[i] = new JLDataView();
            DuOnUDv[i].add(i + 1, duOnu[i]);
        }
        // Filling Duration DataView
        for (int i = 0 ; i<durations.length ; i++) {
            DurationDv[i] = new JLDataView();
            DurationDv[i].add(i + 1, durations[i]);
        }

        // Inverting DuUtDv DataView
        for (int i = 0 ; i<all_delta_u_on_u.length ; i++) {
            all_delta_u_on_u[i] = -all_delta_u_on_u[i];
            //System.out.println("[x,y]"+i+"="+all_durations[i] +","+all_delta_u_on_u[i]);
        }
        DuUtDv.setData(all_durations, all_delta_u_on_u);
    }

    /**
     * Get phase distribution of each drop.
     *
     * @return
     */
    private int[] getDistribution(jhqps.elecdrops.Drop[] local_drops) {
        int[] res = new int[3];
        int drop_type = 0;
        for (int i = 0 ; i<local_drops.length ; i++) {
            // Initialize colors with DSP coding
            //===================================
            int ph1 = local_drops[i].coding & 0x00F;
            int ph2 = local_drops[i].coding & 0x0F0;
            int ph3 = local_drops[i].coding & 0xF00;

            if (ph1 != 0)
                drop_type++;
            if (ph2 != 0)
                drop_type++;
            if (ph3 != 0)
                drop_type++;

            switch (drop_type) {
                case 1:
                    res[0]++;
                    break;
                case 2:
                    res[1]++;
                    break;
                case 3:
                    res[2]++;
                    break;
            }
            drop_type = 0;
        }
        if (ElecDropsGUI.trace) {
            for (int i = 0 ; i<res.length ; i++)
                System.out.println("res[" + i + "]=" + res[i]);
        }
        return res;
    }

    /**
     * Get DU/U values in percent of each drop.
     * Calulation method : first, calculation of delta_u/u of each phase.
     * Then keep the biggest delta_u/u. Is it a good solution ?
     */
    private int[] getDropValues(jhqps.elecdrops.Drop[] local_drops) {
        int[] res = new int[7];
        float u_nom = 20200;
        float[] sorted = new float[local_drops.length];
        int j = 0;

        for (int i = 0 ; i<local_drops.length ; i++) {
            float delta_u_ph1 = (Math.abs(u_nom - local_drops[i].rmsPh1) / u_nom) * 100;
            all_delta_u_on_u[j] = (double) delta_u_ph1;
            j++;
            float delta_u_ph2 = (Math.abs(u_nom - local_drops[i].rmsPh2) / u_nom) * 100;
            all_delta_u_on_u[j] = (double) delta_u_ph2;
            j++;
            float delta_u_ph3 = (Math.abs(u_nom - local_drops[i].rmsPh3) / u_nom) * 100;
            all_delta_u_on_u[j] = (double) delta_u_ph3;
            j++;

            if (delta_u_ph1>delta_u_ph2)
                if (delta_u_ph1>delta_u_ph3)
                    sorted[i] = delta_u_ph1;
                else
                    sorted[i] = delta_u_ph3;
            else if (delta_u_ph2>delta_u_ph3)
                sorted[i] = delta_u_ph2;
            else
                sorted[i] = delta_u_ph3;
            if (ElecDropsGUI.trace)
                System.out.println("Drop " + i + " DU/U = " + sorted[i]);

            if (sorted[i]<5)
                res[0]++;
            if (sorted[i]>5 && sorted[i]<10)
                res[1]++;
            if (sorted[i]>10 && sorted[i]<20)
                res[2]++;
            if (sorted[i]>20 && sorted[i]<40)
                res[3]++;
            if (sorted[i]>40 && sorted[i]<60)
                res[4]++;
            if (sorted[i]>60 && sorted[i]<80)
                res[5]++;
            if (sorted[i]>80 && sorted[i]<=100)
                res[6]++;
        }
        if (ElecDropsGUI.trace) {
            for (int i = 0 ; i<res.length ; i++)
                System.out.println("Result " + i + " = " + res[i]);
        }
        return res;
    }

    /**
     * Get duration of each drop.
     *
     * @return
     */
    private int[] getDurations(jhqps.elecdrops.Drop[] local_drops) {
        int[] res = new int[DurationDv.length];
        float[] durations = new float[local_drops.length * 3];
        boolean first = true;

        //  Retrieve drop durations
        for (int i = 0, j = 0, k = 0 ; i<local_drops.length ; i++, j++, k++) {
            // * 20 to have value in millisecond
            durations[i] = local_drops[i].durationPh1 * 20;
            //System.out.println("durationPh1["+i+"]="+durations[i]);
            durations[i + 1] = local_drops[i].durationPh2 * 20;
            //System.out.println("durationPh2["+i+"]="+durations[i+1]);
            durations[i + 2] = local_drops[i].durationPh3 * 20;
            //System.out.println("durationPh3["+i+"]="+durations[i+2]);

            if (first) {
                max_duration = durations[i];
                first = false;
            } else {
                if (durations[i]>max_duration) {
                    max_duration = durations[i];
                }
            }
            if (ElecDropsGUI.trace)
                System.out.println("max_duration=" + max_duration);
        }

        //	filling data struct for delta u/u (t)
        for (int i = 0 ; i<(local_drops.length) * 3 ; i++) {
            all_durations[i] = (double) durations[i];
            //System.out.println("duration["+i+"]="+durations[i]);
            //System.out.println("all_duration["+i+"]="+all_durations[i]);
        }


        // Sort durations
        for (int i = 0 ; i<durations.length ; i++) {
            if (durations[i]<=40)
                res[0]++;
            if (durations[i]>40 && durations[i]<=80)
                res[1]++;
            if (durations[i]>80 && durations[i]<=120)
                res[2]++;
            if (durations[i]>120 && durations[i]<=200)
                res[3]++;
            if (durations[i]>200 && durations[i]<=360)
                res[4]++;
            if (durations[i]>3600 && durations[i]<=620)
                res[5]++;
            if (durations[i]>620 && durations[i]<=1040)
                res[6]++;
            if (durations[i]>1400 && durations[i]<2080)
                res[7]++;
            if (durations[i]>2000 && durations[i]<=max_duration)
                res[8]++;

        }
        return res;
    }

    private void importDropList(jhqps.elecdrops.Drop[] local_drops) {
        if (ElecDropsGUI.trace) {
            for (int i = 0 ; i<local_drops.length ; i++)
                System.out.println(local_drops.toString());
        }
    }

    public String[] clickOnChart(JLChartEvent evt) {
        String ret[] = new String[2];
        if (evt.getDataView().getName() == "\u0394U/U versus Time") {
            ret[0] = "Drop magnitude : " + (int) evt.getTransformedYValue() + " %";
            ret[1] = "Duration : " + (int) evt.getTransformedXValue() + " ms";
        } else {
            ret[0] = (int) evt.getTransformedYValue() + " events";
            ret[1] = (" ");
        }
		/*try {
			Applet.newAudioClip(new File("/segfs/tango/jclient/elecdrops/src.wav")).play();
        }
		catch (Exception e){System.out.println("Exxception while playing sound...");}
		System.out.println(" sound played");
		*/
        return ret;
    }
}

