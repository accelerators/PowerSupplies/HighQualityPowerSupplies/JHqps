//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/ElecDropUtil.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: ElecDropUtil.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================

/**
 * @author goudard
 */
package jhqps.elecdrops;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ElecDropUtil implements ElecDropDefs {

//private	static ElecDropUtil	instance = null;

    private ElecDropUtil() {
    }
//===============================================================

    /**
     * Execute a shell command and throw exception if command failed.
     *
     * @param cmd shell command to be executed.
     */
//===============================================================
    public static String executeShellCmd(String cmd) throws Exception {
        Process proc = Runtime.getRuntime().exec(cmd);
        System.out.println("Command line is : " + cmd);

        // get command's output stream and
        // put a buffered reader input stream on it.
        //-------------------------------------------
        InputStream istr = proc.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(istr));

        // read output lines from command
        //-------------------------------------------
        StringBuffer sb = new StringBuffer("");
        String str;
        while ((str = br.readLine()) != null) {
            //System.out.println("Return is : " + str);
            sb.append(str + "\n");
        }

        // wait for end of command
        //---------------------------------------
        proc.waitFor();

        // check its exit value
        //------------------------
        int retVal;
        if ((retVal = proc.exitValue()) != 0 || (retVal = proc.exitValue()) == 1) {
            System.out.println("retVal =" + retVal);
            //	An error occured try to read it
            InputStream errstr = proc.getErrorStream();
            br = new BufferedReader(new InputStreamReader(errstr));
            while ((str = br.readLine()) != null) {
                System.out.println("retVal =" + retVal);
                System.out.println(str);
                sb.append(str + "\n");
            }
            throw new Exception(
                    "The shell command " + cmd + "\nreturned : " + retVal +
                            " !\n\nsb = " + sb.substring(sb.length() - 156));
        }

        return sb.substring(sb.length() - 156);//sb.toString();
    }


} // End of Class
