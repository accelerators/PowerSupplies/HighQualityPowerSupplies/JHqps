///+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/devPopup.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: devPopup.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:19  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:51  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:02  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
/**
 * @author goudard
 * @Revision 1.1
 */
package jhqps.elecdrops;

//	Import Standard Java Classes
//==============================

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.IORdump;
import fr.esrf.tangoatk.core.IDevice;

import javax.swing.*;
import java.awt.*;

//	Import Tango Classes
//======================

//=======================================================
/*
 * State viewer popup
 */
//=======================================================

public class devPopup extends JFrame implements
        fr.esrf.tangoatk.widget.device.IDevicePopUp {

    static Font textFont = null;
    static Font devFont = null;
    boolean isVisible = false;
    fr.esrf.tangoatk.core.Device ds = null;
    JLabel devNameLb;
    JTextArea devCmdTxt;
    JButton closeBtn;
    JButton ReStartBtn;
    JButton startBtn;
    JScrollPane scrollPane;
    JLabel devCmdComboLb;
    fr.esrf.tangoatk.widget.command.CommandComboViewer devCmdCombo;
    fr.esrf.tangoatk.core.CommandList devCmdList;
    JPanel southPanel;

    public devPopup(final String instance_name, final String dev) {

        if (textFont == null) textFont = new Font("Dialog", Font.PLAIN, 14);
        if (devFont == null) devFont = new Font("Times", 1, 18);

        getContentPane().setLayout(new BorderLayout());
        devNameLb = new JLabel();
        devNameLb.setFont(devFont);
        devNameLb.setHorizontalAlignment(JLabel.CENTER);

        getContentPane().add(devNameLb, BorderLayout.NORTH);

        devCmdTxt = new JTextArea();
        scrollPane = new JScrollPane();

        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        devCmdTxt.setLineWrap(true);
        devCmdTxt.setEditable(false);
        devCmdTxt.setFont(textFont);
        devCmdTxt.setText("Unknown");
        devCmdTxt.setBackground(java.awt.Color.white);
        scrollPane.setViewportView(devCmdTxt);
        devCmdComboLb = new JLabel();
        devCmdComboLb.setText("Commands");

        getContentPane().add(scrollPane, BorderLayout.CENTER);

        closeBtn = new JButton();
        closeBtn.setText("Close");
        closeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setVisible(false);
            }
        });

        ReStartBtn = new JButton();
        ReStartBtn.setText("Restart Device");
        ReStartBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                //	Accessing administration device
                //	of the serveur to make a devRestart of the server
                //====================================================
                if (JOptionPane.showConfirmDialog(null,
                        "Are you sure that you want to restart this server ?",
                        "Warning",
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.INFORMATION_MESSAGE) == JOptionPane.OK_OPTION) {
                    try {
                        DeviceData argin = new DeviceData();
                        argin.insert(dev);
                        DeviceProxy dev = new DeviceProxy("dserver/" + instance_name);
                        dev.command_inout("DevRestart", argin);
                    } catch (DevFailed e) {
                        JOptionPane.showMessageDialog(null,
                                e.errors[0].desc,
                                e.errors[0].reason,
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        startBtn = new JButton();
        startBtn.setText("Start Device");
        startBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try {
                    String tmp = new IORdump(dev).get_host();
                    String host = tmp.substring(0, tmp.indexOf("."));
                    DeviceData argin = new DeviceData();
                    argin.insert(instance_name);
                    DeviceProxy dev = new DeviceProxy("tango/admin/" + host);
                    dev.command_inout("DevStart", argin);
                } catch (DevFailed e) {
                    JOptionPane.showMessageDialog(null,
                            e.errors[0].desc,
                            e.errors[0].reason,
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        devCmdCombo = new fr.esrf.tangoatk.widget.command.CommandComboViewer();
        devCmdList = new fr.esrf.tangoatk.core.CommandList();

        southPanel = new JPanel();
        southPanel.setLayout(new FlowLayout());
        southPanel.add(devCmdComboLb);
        southPanel.add(devCmdCombo);
        southPanel.add(ReStartBtn);
        southPanel.add(startBtn);
        southPanel.add(closeBtn);
        getContentPane().add(southPanel, BorderLayout.SOUTH);

    }

    public void setModel(fr.esrf.tangoatk.core.Device device) {
        ds = device;
        if (ds != null) {
            try {
                devCmdList.add(ds.getName() + "/*");
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            }
            devCmdCombo.setModel(devCmdList);
            devCmdCombo.setDescriptionVisible(true);
            devCmdCombo.setCancelButtonVisible(true);
            setTitle(device.getName());
        }
    }

    public void setVisible(boolean visible) {
        if (visible) {
            if (ds != null) {
                devNameLb.setText(ds.getName());
                devCmdTxt.setText(ds.getStatus());
            }
            setBounds(200, 150, 600, 300);
            super.setVisible(true);
        } else {
            super.setVisible(false);
        }
        isVisible = visible;
    }

    public void setModel(IDevice arg0) {
        // TODO Auto-generated method stub

    }
}
