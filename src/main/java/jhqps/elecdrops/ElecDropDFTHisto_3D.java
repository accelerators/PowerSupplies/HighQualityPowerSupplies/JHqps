package jhqps.elecdrops;
/**
 * @author : O. Goudard
 */

import fr.esrf.tangoatk.widget.util.Gradient;
import fr.esrf.tangoatk.widget.util.chart.*;

import javax.swing.*;
import java.awt.*;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

public class ElecDropDFTHisto_3D extends tool_panels.spectrum_histo.Histo_3D {
    private static final int DefaultMaxSize = 1200;
    protected Component parent_chart;
    protected String[] y_labels = null;
    int y_step = 1;
    int NbMeasMax;
    private Gradient gradient;
    //===============================================================

    /**
     * Creates new Image Viewer to be used as a 3D history viewer
     *
     * @param parent Parent component.
     */
    //===============================================================
    public ElecDropDFTHisto_3D(Component parent) {
        super(parent, DefaultMaxSize);
        setGradient();
        this.setZoom(3);
        this.setBestFitMinMax(0.0, 1500);
        parent_chart = parent;
    }
    //===============================================================

    /**
     * Creates new Image Viewer to be used as a 3D history viewer
     *
     * @param parent    Parent component.
     * @param nbMeasMax Nb measure maximum
     */
    //===============================================================
    public ElecDropDFTHisto_3D(Component parent, int nbMeasMax) {
        super(parent, nbMeasMax);
        setGradient();
        this.setZoom(3);
        this.setBestFitMinMax(0.0, 1500);
        parent_chart = parent;
    }

    //===============================================================
    //===============================================================
    protected void setGradient() {
        gradient = super.getGradient();
        gradient.removeEntry(2);
        gradient.removeEntry(3);
        gradient.removeEntry(4);
        gradient.removeEntry(5);
        gradient.addEntry(Color.green, 0.015);
        gradient.addEntry(Color.yellow, 0.03);
        super.setGradient(gradient);
    }

    //===============================================================
    //===============================================================
    protected void displayHistoValues(Point p, _3D_Record rec) {
        if (rec == null)
            return;

        //  And build message to display
        String strval = String.format("%.3f", getData(p.x, p.y));

        String strdate = formatDate(rec.time);
        String str;
        int idx = p.y * 10;// / y_step; -> should be f(sampling period).

        str = strdate + " | Harmonic tension :  " + strval + " V at  " + idx + " Hz";
        setMarkerText(str);
    }

    //===============================================================
    //===============================================================
    protected void displaySpectrum(int t_idx) {
        _3D_Record rec = record3D(t_idx);
        String title = "Values at " + new Date(rec.time);
        //	check if y_labels has been set
        int nb = ((y_labels == null) ? rec.values.length : y_labels.length);
        String[] labels = new String[nb];
        for (int i = 0 ; i<nb ; i++) {
            if (y_labels == null)
                labels[i] = "Harmonic " + i / 5 + ", Frequency : " + i * 10 + "Hz";
            else
                labels[i] = y_labels[i];
        }

        //	Add apply to chart
        PopupChart panel;

        if (parent_chart instanceof JDialog)
            panel = new PopupChart((JDialog) parent_chart, title);
        else
            panel = new PopupChart((JFrame) parent_chart, title);

        panel.setLabelVisible(false);
        panel.setY1Data(labels, rec.values);
        //panel.setAsBarGraph();

        panel.setVisible(true);
        panel.setBackground(java.awt.Color.white);
        panel.setLogScale();
        panel.setY1GridVisible(true);
        panel.setY1SubGridVisible(true);

    }
    //===============================================================

    /**
     * Display a 2D history for specified name
     *
     * @param name The specified name
     */
    //===============================================================
    protected void displayHistory(String name) {
        displayHistory(new String[]{name});
    }

    //===============================================================
    //===============================================================
    public class PopupChart extends JDialog {
        protected Component parent;
        protected JLDataView[] data = null;
        private OwnChart chart;
        //===============================================================
        //===============================================================
        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JButton cancelBtn;
        private javax.swing.JPanel jPanel1;
        private javax.swing.JPanel jPanel2;
        private javax.swing.JRadioButton legendBtn;
        private javax.swing.JLabel titleLabel;
        private MyGradient gradient = null;

        /**
         * Creates new form PopupChart
         */
        //===============================================================
        public PopupChart(JFrame parent, String title) {
            super(parent, false);
            this.parent = parent;
            init(title);
        }

        //===============================================================
        //===============================================================
        public PopupChart(JDialog parent, String title) {
            super(parent, false);
            this.parent = parent;
            init(title);
        }

        //===============================================================
        //===============================================================
        private void init(String title) {
            initComponents();
            chart = new OwnChart();
            chart.setPreferredSize(new Dimension(640, 480));
            getContentPane().add(chart, BorderLayout.CENTER);
            titleLabel.setText(title);
            pack();
        }

        //===============================================================
        //===============================================================
        public void setPreferredSize(Dimension d) {
            chart.setPreferredSize(d);
            pack();
        }

        //===============================================================
        //===============================================================
        public void setY1Data(String[] names, long[][] t, double[][] values) {
            chart.setData(names, t, values, 0, names.length);
        }

        //===============================================================
        //===============================================================
        public void setY1Data(String[] names, long[] t, double[] v) {
            long[][] time = new long[1][];
            double[][] values = new double[1][];
            time[0] = t;
            values[0] = v;
            chart.setData(names, time, values, 0, names.length);
        }

        //===============================================================
        //===============================================================
        public void setY1Data(String[] names, long[] t, double[][] values) {
            long[][] time = new long[values.length][];
            for (int i = 0 ; i<values.length ; i++)
                time[i] = t;
            time[0] = t;
            chart.setData(names, time, values, 0, names.length);
        }

        //===============================================================
        //===============================================================
        public void setY2Data(String[] names, long[] t, double[] v) {
            long[][] time = new long[1][];
            double[][] values = new double[1][];
            time[0] = t;
            values[0] = v;
            chart.setData(names, time, values, 1, names.length);
        }

        //===============================================================
        //===============================================================
        public void setY2Data(String[] names, long[] t, double[][] values) {
            long[][] time = new long[values.length][];
            for (int i = 0 ; i<values.length ; i++)
                time[i] = t;
            chart.setData(names, time, values, 1, names.length);
        }

        //===============================================================
        //===============================================================
        public void setY1Data(String[] names, double[] v) {
            double[][] values = new double[1][];
            values[0] = v;

            int nb = v.length;
            long[][] x = new long[1][nb];
            for (int i = 0 ; i<nb ; i++)
                x[0][i] = i;

            chart.setData(names, x, values, 0, 1);
            chart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        }

        //===============================================================
        //===============================================================
        public void setY1Data(String[] names, double[] x, double[] v) {
            double[][] values = new double[1][];
            values[0] = v;

            int nb = x.length;
            double[][] x2 = new double[1][x.length];
            for (int i = 0 ; i<nb ; i++)
                x2[0][i] = x[i];

            chart.setData(names, x2, values, 0, 1);
            chart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        }

        //===============================================================
        //===============================================================
        public void setY2Data(String[] names, double[] v) {
            double[][] values = new double[1][];
            values[0] = v;

            int nb = v.length;
            long[][] x = new long[1][nb];
            for (int i = 0 ; i<nb ; i++)
                x[0][i] = i;

            chart.setData(names, x, values, 1, 1);
            chart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        }

        //===============================================================
        //===============================================================
        public void setY2Data(String[] names, double[] x, double[] v) {
            double[][] values = new double[1][];
            values[0] = v;

            int nb = x.length;
            double[][] x2 = new double[1][x.length];
            for (int i = 0 ; i<nb ; i++)
                x2[0][i] = x[i];

            chart.setData(names, x2, values, 1, 1);
            chart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        }
        //===============================================================

        //===============================================================
        //===============================================================
        public void setXGridVisible(boolean b) {
            chart.getXAxis().setGridVisible(true);
        }

        //===============================================================
        //===============================================================
        public void setXSubGridVisible(boolean b) {
            chart.getXAxis().setSubGridVisible(b);
        }

        //===============================================================
        //===============================================================
        public void setY1GridVisible(boolean b) {
            chart.getY1Axis().setGridVisible(true);
        }

        //===============================================================
        //===============================================================
        public void setY1SubGridVisible(boolean b) {
            chart.getY1Axis().setSubGridVisible(b);
        }

        //===============================================================
        //===============================================================
        public void setY2GridVisible(boolean b) {
            chart.getY2Axis().setGridVisible(true);
        }

        //===============================================================
        //===============================================================
        public void setY2SubGridVisible(boolean b) {
            chart.getY2Axis().setSubGridVisible(b);
        }

        /**
         * This method is called from within the constructor to
         * initialize the form.
         * WARNING: Do NOT modify this code. The content of this method is
         * always regenerated by the Form Editor.
         */
        //===============================================================
        // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
        private void initComponents() {
            jPanel1 = new javax.swing.JPanel();
            cancelBtn = new javax.swing.JButton();
            legendBtn = new javax.swing.JRadioButton();
            jPanel2 = new javax.swing.JPanel();
            titleLabel = new javax.swing.JLabel();

            addWindowListener(new java.awt.event.WindowAdapter() {
                public void windowClosing(java.awt.event.WindowEvent evt) {
                    closeDialog(evt);
                }
            });

            cancelBtn.setText("Dismiss");
            cancelBtn.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    cancelBtnActionPerformed(evt);
                }
            });

            jPanel1.add(cancelBtn);

            legendBtn.setText("Legend");
            legendBtn.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
            legendBtn.setMargin(new java.awt.Insets(0, 0, 0, 0));
            legendBtn.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    legendBtnActionPerformed(evt);
                }
            });

            jPanel1.add(legendBtn);

            getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

            titleLabel.setFont(new java.awt.Font("Dialog", 1, 18));
            titleLabel.setText("Dialog Title");
            jPanel2.add(titleLabel);

            getContentPane().add(jPanel2, java.awt.BorderLayout.NORTH);

            pack();
        }// </editor-fold>//GEN-END:initComponents

        //===============================================================
        //===============================================================
        public void setAsBarGraph() {
            chart.setAsBarGraph();
        }

        //===============================================================
        //===============================================================
        public void setLogScale() {
            chart.setLogScale();
        }

        //===============================================================

        //===============================================================
        //===============================================================
        public void setBarFromZero() {
            chart.setBarFromZero();
        }

        //===============================================================

        //===============================================================
        //===============================================================
        public void setLabelVisible(boolean b) {
            chart.setLabelVisible(b);
            legendBtn.setSelected(b);
        }

        //===============================================================
        //===============================================================
        public void setY1Name(String name) {
            chart.getY1Axis().setName(name);
        }

        //===============================================================
        //===============================================================
        public void setY2Name(String name) {
            chart.getY2Axis().setName(name);
        }

        //===============================================================
        //===============================================================
        private void legendBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_legendBtnActionPerformed

            chart.setLabelVisible(legendBtn.getSelectedObjects() != null);
            chart.repaint();
        }//GEN-LAST:event_legendBtnActionPerformed

        //===============================================================
        //===============================================================
        private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
            doClose();
        }//GEN-LAST:event_cancelBtnActionPerformed

        /**
         * Closes the dialog
         */
        //===============================================================
        private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
            doClose();
        }//GEN-LAST:event_closeDialog
        // End of variables declaration//GEN-END:variables
        //===============================================================

        /**
         * Closes the dialog
         */
        //===============================================================
        private void doClose() {
            if (parent.getWidth() == 0)
                System.exit(0);
            else {
                setVisible(false);
                dispose();
            }
        }

        //===============================================================
        //===============================================================
        Color getNewColor() {
            if (gradient == null)
                gradient = new MyGradient();
            return gradient.getNextColor();
        }

        //===============================================================
        //===============================================================
        class OwnChart extends JLChart implements IJLChartListener {
            protected String[] names;

            //===========================================================
            public OwnChart() {
                setBackground(Color.white);
                setChartBackground(Color.white);
                setHeaderFont(new Font("Dialog", Font.BOLD, 12));
                setLabelVisible(false);
                setJLChartListener(this);
            }

            //===============================================================
            private void setBarFromZero() {
                for (JLDataView dv : data)
                    dv.setFillMethod(JLDataView.METHOD_FILL_FROM_ZERO);
                getXAxis().setPosition(JLAxis.HORIZONTAL_ORG1);
            }

            //===============================================================
            private void setAsBarGraph() {
                for (JLDataView dv : data)
                    dv.setViewType(JLDataView.TYPE_BAR);
            }

            //===============================================================
            private void setLogScale() {
                for (JLDataView dv : data) {
                    dv.getAxis().setScale(JLAxis.LOG_SCALE);
                    dv.setColor(Color.blue);

                }
            }

            //===========================================================
            private void setData(String[] names, long[][] t, double[][] values, int axis, int nb_curves) {
                double[][] x = new double[t.length][];
                for (int i = 0 ; i<t.length ; i++) {
                    x[i] = new double[t[i].length];
                    for (int j = 0 ; j<t[i].length ; j++)
                        x[i][j] = t[i][j];
                }
                setData(names, x, values, axis, nb_curves);
            }

            //===========================================================
            private void setData(String[] names, double[][] x, double[][] values, int axis, int nb_curves) {
                this.names = names;
                data = new JLDataView[nb_curves];
                for (int i = 0 ; i<nb_curves ; i++) {
                    data[i] = new JLDataView();
                    data[i].setViewType(JLDataView.TYPE_LINE);
                    data[i].setBarWidth(1);
                    if (nb_curves == 1 && names.length>1)
                        data[i].setName("Values");
                    else
                        data[i].setName(names[i]);

                    data[i].setLineWidth(1);
                    data[i].setColor(getNewColor());
                    if (axis == 0)
                        getY1Axis().addDataView(data[i]);
                    else
                        getY2Axis().addDataView(data[i]);
                }
                for (int i = 0 ; i<values.length ; i++) {
                    for (int j = 0 ; j<x[i].length ; j++)
                        data[i].add(x[i][j] * 10, values[i][j]);
                }
                getY1Axis().setAutoScale(true);
                getY2Axis().setAutoScale(true);
                repaint();
            }

            //===========================================================
            public String[] clickOnChart(JLChartEvent event) {

                JLDataView dv = event.getDataView();
                int idx = event.getDataViewIndex();
                double x = dv.getXValueByIndex(idx);
                double y = dv.getYValueByIndex(idx);
                int anno = getXAxis().getAnnotation();
                Double y_formatted = new Double(y);

                String[] retval = new String[3];
                retval[0] = dv.getName();

                if (anno == JLAxis.VALUE_ANNO) {
                    if (names != null && names.length>idx)
                        retval[1] = names[idx];
                    else
                        retval[1] = "Index " + idx;
                } else
                    retval[1] = formatDate((long) x);

                retval[2] = "Harmonic tension =  " + y_formatted.shortValue() + " V";
                return retval;
            }

            //===========================================================
            private String formatDate(long ms) {
                StringTokenizer st = new StringTokenizer(new Date(ms).toString());
                Vector<String> v = new Vector<String>();
                while (st.hasMoreTokens())
                    v.add(st.nextToken());

                String month = (String) v.get(1);
                String day = (String) v.get(2);
                String time = (String) v.get(3);
                String year = (String) v.get(v.size() - 1);
                if (year.indexOf(')')>0) year = year.substring(0, year.indexOf(')'));

                return day + " " + month + " " + year + "  " + time;
            }
        }

        //===============================================================
        //===============================================================
        class MyGradient extends Gradient {
            private final int nbColors = 256;
            private int step = nbColors / 2;
            private int colorIdx = -step;
            private int[] colorMap;
            private Vector<Integer> done = new Vector<Integer>();

            //===========================================================
            private MyGradient() {
                buildRainbowGradient();
                colorMap = buildColorMap(nbColors);
            }

            //===========================================================
            private boolean alreadyUsed(int idx) {
                for (int i : done)
                    if (i == idx)
                        return true;
                return false;
            }

            //===========================================================
            private Color getNextColor() {
                if (done.size() == nbColors) {
                    done.clear();
                    step = nbColors / 2;
                    colorIdx = -step;
                }
                do {
                    colorIdx += step;
                    if (colorIdx >= colorMap.length) {
                        colorIdx = step;
                        step /= 2;
                    }
                }
                while (alreadyUsed(colorIdx));

                done.add(colorIdx);
                return new Color(colorMap[colorIdx]);
            }
        }
    }
}
