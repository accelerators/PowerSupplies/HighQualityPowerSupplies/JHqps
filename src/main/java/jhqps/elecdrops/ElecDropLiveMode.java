//+======================================================================
//$Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/ElecDropLiveMode.java,v $

//Project:   JHqps

//Description:  HQPS java application for CTRM. .

//$Author: goudard $

//$Revision: 1.55 $

//$Log: ElecDropLiveMode.java,v $
//Revision 1.55  2009/08/25 13:53:22  goudard
//Modifications of HqpsBinIn and HqpsBinOut reading.
//Added :
// - Incoherent state
// - wago uninitialized state
// - conditions to normal and economy mode detection.
//
//Modification of ElecDropStat : isolation curve added.
//
//Revision 1.54  2009/08/25 11:39:18  goudard
//Change behaviour when wago plc is unreachable (network problem)
//
//Revision 1.53  2009/08/18 11:33:50  goudard
//File JHqpsParameters.java added to CVS
//elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
//Revision 1.51  2009/06/15 13:43:01  goudard
//Modifications in
//- state management of HQPSGlobal state button.
//- FFT for jhqps.elecdrops.
//- Main Parameters window.

//Revision 1.2  2009/01/28 13:11:14  goudard
//Added cvs header.


//Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//All Rights Reversed
//-======================================================================

/*
 * ElecDropLiveMode.java
 *
 * Created on 14 mai 2002, 08:41
 */

/**
 * @author goudard
 */
package jhqps.elecdrops;

//Import Standart Classes
//=========================

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.chart.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

//Import Tango Classes
//=========================
//Import ATK Classes
//====================

public class ElecDropLiveMode extends JDialog implements ElecDropDefs, IJLChartListener {
    //	Charts elements
//	=================
    final JFrame rmsFrame = new JFrame();
    final JFrame freqFrame = new JFrame();
    final JFrame dftFrame = new JFrame();
    final JFrame meanFrame = new JFrame();
    final JLChart rmsChart = new JLChart();
    final JLChart freqChart = new JLChart();
    final JLChart dftChart = new JLChart();
    final JLChart meanChart = new JLChart();
    final JLChart liveChart = new JLChart();
    final JLDataView minRMS = new JLDataView();
    final JLDataView maxRMS = new JLDataView();
    final JLDataView v1 = new JLDataView();
    final JLDataView v2 = new JLDataView();
    final JLDataView v3 = new JLDataView();
    final JLDataView f1 = new JLDataView();
    final JLDataView f2 = new JLDataView();
    final JLDataView f3 = new JLDataView();
    final JLDataView dftx = new JLDataView();
    final JLDataView mean1 = new JLDataView();
    final JLDataView mean2 = new JLDataView();
    final JLDataView mean3 = new JLDataView();
    final JLDataView live1 = new JLDataView();
    final JLDataView live2 = new JLDataView();
    final JLDataView live3 = new JLDataView();
    final JLDataView theo1 = new JLDataView();
    final JLDataView theo2 = new JLDataView();
    final JLDataView theo3 = new JLDataView();
    final JLDataView[] dft = new JLDataView[40];
    public double[] mean_rms = new double[3];
    public int nb_dataviews;
    public boolean phase1positive;
    public boolean phase2positive;
    public boolean phase3positive;
    public JLDataView[] dotarray1;
    public JLDataView[] dotarray2;
    public JLDataView[] dotarray3;
    public double[] datatheo1 = new double[125];
    public double[] datatheo2 = new double[125];
    public double[] datatheo3 = new double[125];
    public boolean first = true;
    public boolean readThersholds = true;
    public int minRMS_read = 0;
    public int maxRMS_read = 0;
    public double phi0;
    public int max = 0;
    public ElecDrops Elecdev;
    public String local_devname;
    int delay = 1000; //	in millisecond
    int curDft = 0;  //	Current Dft
    int mean_rms_idx = 0;
    double sum1 = 0;
    double sum2 = 0;
    double sum3 = 0;
    short[] ss1_powers = new short[3]; //	infra/power/1 data
    double[] rms_sum1 = new double[61];
    double[] rms_sum2 = new double[61];
    double[] rms_sum3 = new double[61];
    Timer T1;
    fr.esrf.TangoApi.DeviceProxy dev;
    /*-------------------------------------------------------------------------*/
    private JLabel jLabel1;
    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
	/*private void TheoActionPerformed(ActionEvent evt)
	{
		if (displayTheoChB.isSelected()){
            liveChart.getY1Axis().addDataView(theo1);
            liveChart.getY1Axis().addDataView(theo2);
            liveChart.getY1Axis().addDataView(theo3);
		}
		else{
            liveChart.getY1Axis().removeDataView(theo1);
            liveChart.getY1Axis().removeDataView(theo2);
            liveChart.getY1Axis().removeDataView(theo3);
		}
	}*/
    private JLabel jLabel3;

    /*-------------------------------------------------------------------------*/
    private JLabel jLabel2;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel12;
    private JLabel jLabel13;
    private JLabel jLabel14;
    private JLabel jLabel15;
    private JButton freqHistoBtn;
    private JButton exitBtn;
    private JButton rmsHistoBtn;
    /*-------------------------------------------------------------------------*/
    private JButton dftLiveBtn;
    private JButton meanLiveBtn;

    /*-------------------------------------------------------------------------*/
    private JLabel date;
    private JLabel activePower;
    private JLabel apparentPower;
    private JLabel cosPhi;
    private JLabel activePowerValue;
    private JLabel apparentPowerValue;
    private JLabel cosPhiValue;
    private JPanel LivePl;

    /**
     * Creates new form ElecDropLiveMode
     */
    /*-------------------------------------------------------------------------*/
    public ElecDropLiveMode(java.awt.Frame parent, boolean modal, fr.esrf.TangoApi.DeviceProxy device) {
        super(parent, modal);
        dev = device;
        local_devname = dev.get_name();

        //	Set Analogic values to live values
        // ElecDropsGUI.DSP_device.setAnalogicMode(SETMODE_LIVE);
        //	Determine phase1 sens.
        boolean isPositive = readSensAndOffset();
        if (isPositive)
            System.out.println("Phase 1 is positive.");
        else
            System.out.println("Phase 1 is negative.");
        /**
         *	A timer is reading RMS data, H0 data, frequency data,
         *	active, apparent powers andcos phi (on taco device)
         *	andLive values.
         */
        //if(isPositive){
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                short[] data = new short[2];
                data[0] = 3;
                data[1] = 14;

                // First, read nb_new_drop
                //short [] addr1 	= { NEW_DROPS_OFFSET, 1 };
                //short [] nb_new_drop = new short[addr1[1]];
                //nb_new_drop = readValues2(addr1);

				/*if (nb_new_drop[0] != 0) {
					try {
					//	new ElecLogger.Logger(ElecDropsGUI.file.extractString()).addNewDrop(ElecDropsGUI.file.extractString(), nb_new_drop[0]);
					}
					catch (Exception e) {
						System.out.println("Exception caucht while adding drop" + e);
					}
					System.out.println("Adding " + nb_new_drop[0] + " drop in logger drop file.");
				}
				else {*/
                readDSPTime(local_devname);
                readValues(data);
                /*readLiveH0();*/
                readLiveValues();
                try {
                    ss1_powers = new PowerReader().readPowers();
                    activePowerValue.setText(ss1_powers[0] + " kW");
                    apparentPowerValue.setText(ss1_powers[1] + " kVA");
                    cosPhiValue.setText("" + (float) ss1_powers[2] / 1000);
                } catch (DevFailed e) {
                    System.out.println("Unable to access infra/power/1.");
                }
                pack();
            }
            /*}*/
        };
        T1 = new Timer(delay, taskPerformer);
//		}
        //initLiveGraph();
        initComponents();
        // pack();

		/*	dftChart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
	dftChart.setJLChartListener(this);

	for(int i=0 ; i<40 ; i++)
	{
		dft[i] = new JLDataView();
		dft[i].setName("Harmonic rank " + Integer.toString(i+1));
		dft[i].setUnit("Volts");
		dft[i].setColor(Color.blue);
		dft[i].setLineWidth(0);
		dft[i].setBarWidth(8);
		dft[i].setFillStyle(JLDataView.FILL_STYLE_SOLID);
		dft[i].setFillColor(Color.blue);
		dft[i].setViewType(JLDataView.TYPE_BAR);
		dftChart.getY1Axis().addDataView(dft[i]);
	}
    readLiveDFT();*/    // Reads DFT once and then wait for click on chart to refresh.
        pack();
    }

    /**
     * @param args the command line arguments
     */
    /*-------------------------------------------------------------------------*/
    public static void main(String args[]) {
        // ElecDropLiveMode L1 = new ElecDropLiveMode(new javax.swing.JFrame(), false, device);
        //L1.setVisible(true);
        //System.out.println("Starting scan");
        //L1.scan();
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public String[] clickOnChart(JLChartEvent evt) {

        String ret[] = new String[3];

        ret[0] = evt.searchResult.dataView.getName();
        ret[1] = evt.getTransformedXValue() + " Hz";
        ret[2] = evt.getTransformedYValue() + " Volt";

        return ret;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    /*-------------------------------------------------------------------------*/
    private void initComponents() {
        Color darkGreen = new Color(0, 153, 102);
        jLabel1 = new JLabel("Phase 1");
        jLabel2 = new JLabel("Phase 2");
        jLabel3 = new JLabel("Phase 3");
        jLabel4 = new JLabel("00000");
        jLabel5 = new JLabel("00000");
        jLabel6 = new JLabel("00000");
        jLabel7 = new JLabel("00000");
        jLabel8 = new JLabel("00000");
        jLabel9 = new JLabel("00000");
        jLabel10 = new JLabel("00000");
        jLabel11 = new JLabel("00000");
        jLabel12 = new JLabel("00000");
        jLabel13 = new JLabel("RMS Value");
        jLabel14 = new JLabel("H0 Value");
        jLabel15 = new JLabel("Period");
        rmsHistoBtn = new JButton("RMS History");
        freqHistoBtn = new JButton("Frequency History");
        dftLiveBtn = new JButton("DFT Live");
        meanLiveBtn = new JButton("H0 Live");
        exitBtn = new JButton("Close");
        date = new JLabel("reading dsp_time");
        activePower = new JLabel("Active   power");
        apparentPower = new JLabel("Apparent power");
        cosPhi = new JLabel("Cosinus  phi");
        activePowerValue = new JLabel("---- kW");
        apparentPowerValue = new JLabel("---- kVA");
        cosPhiValue = new JLabel("-.---");
        Insets inset = new Insets(10, 10, 10, 10);

        getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints gBC = new GridBagConstraints();
        this.setLocation(100, 100);
        this.setTitle("Live Mode");

        rmsFrame.setLocation(200, 200);
        freqFrame.setLocation(200, 200);
        dftFrame.setLocation(200, 200);
        meanFrame.setLocation(200, 200);

        getContentPane().setBackground(Color.white);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel1.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 2;
        gBC.insets = inset;
        getContentPane().add(jLabel1, gBC);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel2.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 2;
        gBC.insets = inset;
        getContentPane().add(jLabel2, gBC);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel3.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 4;
        gBC.gridy = 2;
        gBC.insets = inset;
        getContentPane().add(jLabel3, gBC);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel4.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 3;
        gBC.insets = inset;
        getContentPane().add(jLabel4, gBC);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel5.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 3;
        gBC.insets = inset;
        getContentPane().add(jLabel5, gBC);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel6.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 4;
        gBC.gridy = 3;
        gBC.insets = inset;
        getContentPane().add(jLabel6, gBC);

        jLabel7.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel7.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 5;
        gBC.insets = inset;
        getContentPane().add(jLabel7, gBC);

        jLabel8.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel8.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 5;
        gBC.insets = inset;
        getContentPane().add(jLabel8, gBC);

        jLabel9.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel9.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 4;
        gBC.gridy = 5;
        gBC.insets = inset;
        getContentPane().add(jLabel9, gBC);

        jLabel10.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel10.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 4;
        gBC.insets = inset;
        getContentPane().add(jLabel10, gBC);

        jLabel11.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel11.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 4;
        gBC.insets = inset;
        getContentPane().add(jLabel11, gBC);

        jLabel12.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel12.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 4;
        gBC.gridy = 4;
        gBC.insets = inset;
        getContentPane().add(jLabel12, gBC);

        jLabel13.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel13.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 1;
        gBC.gridy = 3;
        gBC.insets = inset;
        getContentPane().add(jLabel13, gBC);

        jLabel14.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel14.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 1;
        gBC.gridy = 5;
        gBC.insets = inset;
        getContentPane().add(jLabel14, gBC);

        jLabel15.setFont(new java.awt.Font("Arial", 1, 14));
        jLabel15.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 1;
        gBC.gridy = 4;
        gBC.insets = inset;
        getContentPane().add(jLabel15, gBC);

        exitBtn.setBackground(Color.white);
        exitBtn.setFocusPainted(false);
        exitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitBtnActionPerformed(evt);
            }
        });

        gBC = new GridBagConstraints();
        gBC.gridx = 1;
        gBC.gridy = 10;
        gBC.insets = inset;
        gBC.gridwidth = 4;
        getContentPane().add(exitBtn, gBC);

        rmsHistoBtn.setFocusPainted(false);
        rmsHistoBtn.setBackground(Color.white);
        rmsHistoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rmsHistoBtnActionPerformed(evt);
            }
        });
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 9;
        gBC.insets = inset;
        getContentPane().add(rmsHistoBtn, gBC);

        freqHistoBtn.setBackground(Color.white);
        freqHistoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                freqHistoBtnActionPerformed(evt);
            }
        });
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 9;
        gBC.insets = inset;
        getContentPane().add(freqHistoBtn, gBC);

        dftLiveBtn.setBackground(Color.white);
        dftLiveBtn.setFocusPainted(false);
        dftLiveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dftLiveBtnActionPerformed(evt);
            }
        });
        gBC = new GridBagConstraints();
        gBC.gridx = 4;
        gBC.gridy = 9;
        gBC.insets = inset;
        getContentPane().add(dftLiveBtn, gBC);

        meanLiveBtn.setBackground(Color.white);
        meanLiveBtn.setFocusPainted(false);
        meanLiveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                meanLiveBtnActionPerformed(evt);
            }
        });
        gBC = new GridBagConstraints();
        gBC.gridx = 1;
        gBC.gridy = 9;
        gBC.insets = inset;
        getContentPane().add(meanLiveBtn, gBC);
        activePower.setFont(new java.awt.Font("Arial", 1, 14));
        activePower.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 6;
        getContentPane().add(activePower, gBC);

        apparentPower.setFont(new java.awt.Font("Arial", 1, 14));
        apparentPower.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 7;
        getContentPane().add(apparentPower, gBC);

        cosPhi.setFont(new java.awt.Font("Arial", 1, 14));
        cosPhi.setForeground(Color.BLUE);
        gBC = new GridBagConstraints();
        gBC.gridx = 2;
        gBC.gridy = 8;
        getContentPane().add(cosPhi, gBC);

        activePowerValue.setFont(new java.awt.Font("Arial", 1, 14));
        activePowerValue.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 6;
        getContentPane().add(activePowerValue, gBC);

        apparentPowerValue.setFont(new java.awt.Font("Arial", 1, 14));
        apparentPowerValue.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 7;
        getContentPane().add(apparentPowerValue, gBC);

        cosPhiValue.setFont(new java.awt.Font("Arial", 1, 14));
        cosPhiValue.setForeground(darkGreen);
        gBC = new GridBagConstraints();
        gBC.gridx = 3;
        gBC.gridy = 8;
        getContentPane().add(cosPhiValue, gBC);

        LivePl = new JPanel();
        LivePl.setLayout(new BorderLayout());
        LivePl.setBackground(Color.white);
        LivePl.add(liveChart, BorderLayout.CENTER);
        JPanel SouthPl = new JPanel();
        //displayTheoChB.setBackground(Color.white);
        //SouthPl.add(displayTheoChB);
        SouthPl.setBackground(Color.white);
        LivePl.add(SouthPl, BorderLayout.SOUTH);

        gBC = new GridBagConstraints();
        gBC.gridx = 5;
        gBC.gridy = 1;
        gBC.gridheight = 10;
        getContentPane().add(LivePl, gBC);

        date.setFont(new java.awt.Font("Arial", 0, 18));
        gBC = new GridBagConstraints();
        gBC.gridx = 1;
        gBC.gridy = 1;
        gBC.gridwidth = 4;
        gBC.insets = inset;
        getContentPane().add(date, gBC);

		/*displayTheoChB.addActionListener(new ActionListener() {
	 public void actionPerformed(ActionEvent evt) {
		 TheoActionPerformed(evt);
	 }
	});*/
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void meanLiveBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Color darkGreen = new Color(0, 153, 102);

        mean1.setColor(darkGreen);
        mean1.setName("H0 Phase 1");
        mean1.setLineWidth(3);
        mean2.setColor(Color.blue);
        mean2.setName("H0 Phase 2");
        mean2.setLineWidth(3);
        mean3.setColor(Color.red);
        mean3.setName("H0 Phase 3");
        mean3.setLineWidth(3);

        meanChart.setBackground(Color.white);
        meanChart.setChartBackground(new Color(238, 232, 170));
        meanChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        meanChart.setHeader("20kV H0 Live");
        meanChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));

        meanChart.getY1Axis().setName("Volts");
        //meanChart.getY1Axis().setScale(JLAxis.LOG_SCALE);
        meanChart.getY1Axis().setGridVisible(true);
        meanChart.getY1Axis().setSubGridVisible(true);
        meanChart.getY1Axis().setAutoScale(true);

        meanChart.getXAxis().setName("Time");
        meanChart.getXAxis().setGridVisible(true);
        meanChart.getXAxis().setMaximum(40);

        meanChart.getY1Axis().addDataView(mean1);
        meanChart.getY1Axis().addDataView(mean2);
        meanChart.getY1Axis().addDataView(mean3);

        JPanel bot = new JPanel();
        bot.setLayout(new FlowLayout());
        bot.setBackground(Color.white);

        //	Exit Button
        JButton b = new JButton("Close");
        b.setBackground(Color.white);
        b.setFocusPainted(false);
        b.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                meanFrame.setVisible(false);
            }
        });
        bot.add(b);

        JButton c = new JButton("Options");
        c.setFocusPainted(false);
        c.setBackground(Color.white);
        c.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                meanChart.showOptionDialog();
            }
        });
        bot.add(c);

        meanFrame.getContentPane().setLayout(new BorderLayout());
        meanFrame.getContentPane().add(meanChart, BorderLayout.CENTER);
        meanFrame.getContentPane().add(bot, BorderLayout.SOUTH);
        meanFrame.setSize(500, 500);
        meanFrame.setVisible(true);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void readLiveDFT() {

        short[] data = new short[121];

        try {
            DeviceAttribute devattr = dev.read_attribute("DFTLive");
            data = devattr.extractShortArray();

            // extract dft1 2 or 3
            int j = 0;
            for (int i = 40 * curDft ; i<40 * (curDft + 1) ; i++) {
                dft[j].reset();
                double tmp = Math.sqrt(2) * (data[i] * coeff); // TODO with PF : verify if it's the good cooficient
                dft[j].add((double) ((j + 1) * 50), tmp);
                j++;
            }

            dftChart.repaint();
        } catch (DevFailed e) {
            ErrorPane.showErrorMessage(null, dev.get_name(), e);
        }
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void dftLiveBtnActionPerformed(java.awt.event.ActionEvent evt) {

        dftx.setName("Frequency");
        dftx.setUnit("Hz");

        dftChart.setBackground(Color.white);
        dftChart.setChartBackground(new Color(238, 232, 170));
        dftChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        dftChart.setHeader("20kV Frequency Live DFT");
        dftChart.setLabelVisible(false);

        dftChart.getY1Axis().setName("Amplitude (V)");
        dftChart.getY1Axis().setScale(JLAxis.LOG_SCALE);
        dftChart.getY1Axis().setGridVisible(true);
        dftChart.getY1Axis().setSubGridVisible(true);
        dftChart.getY1Axis().setMaximum(22000);

        dftChart.getXAxis().setGridVisible(true);
        dftChart.getXAxis().setAutoScale(false);
        dftChart.getXAxis().setMaximum(2100);
        dftChart.getXAxis().setMinimum(0);
        dftChart.getXAxis().setName("Frequency (Hz)");

        JPanel bot = new JPanel();
        bot.setLayout(new FlowLayout());
        bot.setBackground(Color.white);

        //	Exit Button
        //=================================
        readLiveDFT();
        JButton b = new JButton("Close");
        b.setFocusPainted(false);
        b.setBackground(Color.white);
        b.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                dftFrame.setVisible(false);
            }
        });
        bot.add(b);

        JButton c = new JButton("Options");
        c.setFocusPainted(false);
        c.setBackground(Color.white);
        c.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                dftChart.showOptionDialog();
            }
        });
        bot.add(c);

        final JCheckBox ph1 = new JCheckBox("Phase 1");
        final JCheckBox ph2 = new JCheckBox("Phase 2");
        final JCheckBox ph3 = new JCheckBox("Phase 3");

        ph1.setBackground(Color.white);
        ph2.setBackground(Color.white);
        ph3.setBackground(Color.white);
        ph1.setFocusPainted(false);
        ph2.setFocusPainted(false);
        ph3.setFocusPainted(false);

        ph1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                curDft = 0;
                readLiveDFT();
                ph2.setSelected(false);
                ph3.setSelected(false);
            }
        });
        bot.add(ph1);
        ph2.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                curDft = 1;
                readLiveDFT();
                ph1.setSelected(false);
                ph3.setSelected(false);
            }
        });
        bot.add(ph2);
        ph3.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                curDft = 2;
                readLiveDFT();
                ph1.setSelected(false);
                ph2.setSelected(false);
            }
        });
        bot.add(ph3);

        ph1.setSelected(true);

        dftFrame.getContentPane().setLayout(new BorderLayout());
        dftFrame.getContentPane().add(dftChart, BorderLayout.CENTER);
        dftFrame.getContentPane().add(bot, BorderLayout.SOUTH);
        dftFrame.setSize(700, 500);
        dftFrame.setVisible(true);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void freqHistoBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Color darkGreen = new Color(0, 153, 102);

        f1.setColor(Color.blue);
        f1.setName("Phase 1");

        f2.setColor(darkGreen);
        f2.setName("Phase 2");

        f3.setColor(Color.red);
        f3.setName("Phase 3");

        freqChart.setBackground(Color.white);
        freqChart.setChartBackground(new Color(238, 232, 170));
        freqChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        freqChart.setHeader("20kV Frequency History");
        freqChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));

        freqChart.getY1Axis().setName("Hertz");
        freqChart.getY1Axis().setGridVisible(true);
        freqChart.getY1Axis().setMaximum(50.5);
        freqChart.getY1Axis().setMinimum(49.5);

        freqChart.getXAxis().setName("Time");
        freqChart.getXAxis().setGridVisible(true);
        freqChart.getXAxis().setMaximum(100);

        freqChart.setDisplayDuration(30000);
        freqChart.getY1Axis().addDataView(f1);
        freqChart.getY1Axis().addDataView(f2);
        freqChart.getY1Axis().addDataView(f3);

        JPanel bot = new JPanel();
        bot.setLayout(new FlowLayout());
        bot.setBackground(Color.white);

        //	Exit Button
        JButton exitBtn = new JButton("Close");
        exitBtn.setBackground(Color.white);
        exitBtn.setFocusPainted(false);
        exitBtn.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                freqFrame.setVisible(false);
            }
        });
        bot.add(exitBtn);

        JButton c = new JButton("Options");
        c.setBackground(Color.white);
        c.setFocusPainted(false);
        c.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                freqChart.showOptionDialog();
            }
        });
        bot.add(c);

        freqFrame.getContentPane().setLayout(new BorderLayout());
        freqFrame.getContentPane().add(freqChart, BorderLayout.CENTER);
        freqFrame.getContentPane().add(bot, BorderLayout.SOUTH);
        freqFrame.setSize(500, 500);
        freqFrame.setVisible(true);

    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void exitBtnActionPerformed(java.awt.event.ActionEvent evt) {
        this.setVisible(false);
        //ElecDropsGUI.DSP_device.setAnalogicMode(SETMODE_10PERIOD);
        T1.stop();
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void rmsHistoBtnActionPerformed(java.awt.event.ActionEvent evt) {

        Color darkGreen = new Color(0, 153, 102);

        minRMS.setColor(Color.DARK_GRAY);
        minRMS.setName("Min RMS");
        minRMS.setLineWidth(3);

        maxRMS.setColor(Color.DARK_GRAY);
        maxRMS.setName("Max RMS");
        maxRMS.setLineWidth(3);

        v1.setColor(Color.blue);
        v1.setName("Phase 1");
        v1.setLineWidth(3);

        v2.setColor(darkGreen);
        v2.setName("Phase 2");
        v2.setLineWidth(3);

        v3.setColor(Color.red);
        v3.setName("Phase 3");
        v3.setLineWidth(3);

        rmsChart.setBackground(Color.white);
        rmsChart.setChartBackground(new Color(238, 232, 170));
        rmsChart.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        rmsChart.setHeader("20kV RMS History");
        rmsChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));

        rmsChart.getY1Axis().setName("Volts");
        rmsChart.getY1Axis().setAutoScale(true);
        rmsChart.getY1Axis().setGridVisible(true);
        rmsChart.getY1Axis().setSubGridVisible(true);

        rmsChart.getXAxis().setName("Time");
        rmsChart.getXAxis().setGridVisible(true);
        rmsChart.getXAxis().setMaximum(100);

        rmsChart.setDisplayDuration(30000);
        rmsChart.getY1Axis().addDataView(minRMS);
        rmsChart.getY1Axis().addDataView(maxRMS);
        rmsChart.getY1Axis().addDataView(v1);
        rmsChart.getY1Axis().addDataView(v2);
        rmsChart.getY1Axis().addDataView(v3);

        JPanel bot = new JPanel();
        bot.setLayout(new FlowLayout());
        bot.setBackground(Color.white);

        JButton exitBtn = new JButton("Close");
        exitBtn.setBackground(Color.white);
        exitBtn.setFocusPainted(false);
        exitBtn.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                rmsFrame.setVisible(false);
            }
        });
        bot.add(exitBtn);

        JButton graphOpt = new JButton("Options");
        graphOpt.setBackground(Color.white);
        graphOpt.setFocusPainted(false);
        graphOpt.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                rmsChart.showOptionDialog();
            }
        });
        bot.add(graphOpt);

        JLabel rmsMean1Lbl = new JLabel();
        rmsMean1Lbl.setText("RMS mean value on phase 1 = ");
        JLabel rmsMean2Lbl = new JLabel();
        rmsMean2Lbl.setText("RMS mean value on phase 2 = ");
        JLabel rmsMean3Lbl = new JLabel();
        rmsMean3Lbl.setText("RMS mean value on phase 3 = ");

        bot.add(rmsMean1Lbl);
        bot.add(rmsMean2Lbl);
        bot.add(rmsMean3Lbl);


        rmsFrame.getContentPane().setLayout(new BorderLayout());
        rmsFrame.getContentPane().add(rmsChart, BorderLayout.CENTER);
        rmsFrame.getContentPane().add(bot, BorderLayout.SOUTH);
        rmsFrame.setSize(500, 500);
        rmsFrame.setVisible(true);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void exitForm(java.awt.event.WindowEvent evt) {
        this.setVisible(false);
        //ElecDropsGUI.DSP_device.setAnalogicMode(SETMODE_10PERIOD);
        T1.stop();
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public short[] readValues(short[] data) {
        short[] values = new short[data[1]];
        short[] values1 = new short[data[1]];

        try {
            // Extract rms and frequency values.
            DeviceData argin = new DeviceData();
            argin.insert(data);
            DeviceAttribute argout = dev.read_attribute("RMSLive");
            values = argout.extractShortArray();
            int rms1 = values[0];
            int rms2 = values[1];
            int rms3 = values[2];
            argout = dev.read_attribute("MEANLive");
            values = argout.extractShortArray();
            int mean1 = values[0];
            int mean2 = values[1];
            int mean3 = values[2];
            argout = dev.read_attribute("FREQLive");
            values = argout.extractShortArray();
            int freq1 = values[0];
            int freq2 = values[1];
            int freq3 = values[2];
            double time = (double) System.currentTimeMillis();

            if (readThersholds) {
                short[] levels = new short[2];
                levels[0] = TRIG_LEVELS;
                levels[1] = NB_TRIG_LEVELS;
                DeviceData argin1 = new DeviceData();
                argin1.insert(levels);
                DeviceData minmaxThersholds = dev.command_inout("ReadValues", argin1);
                values1 = minmaxThersholds.extractShortArray();
                minRMS_read = values1[0];
                maxRMS_read = values1[1];
                if (ElecDropsGUI.trace) {
                    System.out.println("Value1[0] = " + values1[0]);
                    System.out.println("Value1[1] = " + values1[1]);
                }
                readThersholds = false;
            }


            date.setForeground(java.awt.Color.black);
            jLabel4.setText(rms1 + " V");
            jLabel5.setText(rms2 + " V");
            jLabel6.setText(rms3 + " V");
            jLabel7.setText(mean1 + " V");
            jLabel8.setText(mean2 + " V");
            jLabel9.setText(mean3 + " V");
            jLabel10.setText(freq1 + " us");
            jLabel11.setText(freq2 + " us");
            jLabel12.setText(freq3 + " us");


            rmsChart.addData(minRMS, time, minRMS_read);
            rmsChart.addData(maxRMS, time, maxRMS_read);
            rmsChart.addData(v1, time, rms1);
            rmsChart.addData(v2, time, rms2);
            rmsChart.addData(v3, time, rms3);
            freqChart.addData(f1, time, freq1);
            freqChart.addData(f2, time, freq2);
            freqChart.addData(f3, time, freq3);
            rmsChart.repaint();
            freqChart.repaint();
        } catch (DevFailed e) {
            date.setForeground(java.awt.Color.red);
            date.setText("Server Error");
            jLabel4.setText("----- V ");
            jLabel5.setText("----- V ");
            jLabel6.setText("----- V ");
            jLabel7.setText("----- Hz");
            jLabel8.setText("----- Hz");
            jLabel9.setText("----- Hz");
            jLabel10.setText("----- V ");
            jLabel11.setText("----- V ");
            jLabel12.setText("----- V ");
            ErrorPane.showErrorMessage(null, dev.get_name(), e);
        }
        return values;
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public double[] caculateMeanRMS(double rms1, double rms2, double rms3, int index) {

        //	Filling table
        if (index<60) {
            rms_sum1[index] = rms1;
            rms_sum2[index] = rms2;
            rms_sum3[index] = rms3;
        }

        if (index == 60) {
            // Calculates sum
            for (int i = 0 ; i<60 ; i++) {
                sum1 += rms_sum1[i];
                sum2 += rms_sum2[i];
                sum3 += rms_sum3[i];
            }
            // 	Calculates  mean value every minuts
            mean_rms[0] = sum1 / (double) index;
            mean_rms[1] = sum2 / (double) index;
            mean_rms[2] = sum3 / (double) index;
            index = 0;
            if (ElecDropsGUI.trace) {
                System.out.println("Mean RMS Ph1 = " + mean_rms[0] +
                        " Volts\nMean RMS Ph2 = " + mean_rms[1] +
                        " Volts\nMean RMS Ph3 = " + mean_rms[2] +
                        " Volts\n_____________________________");
            }
        }
        return mean_rms;
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void readDSPTime(String devname) {
        Date d = new Date(new ElecDrops(devname).DspTimeAttr(ReadDsp));
        String str = new String();
        str += DateFormat.getDateInstance(DateFormat.SHORT,
                Locale.FRANCE).format(d) + "   ";
        str += DateFormat.getTimeInstance(DateFormat.MEDIUM,
                Locale.FRANCE).format(d);
        date.setText(str);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void readLiveH0() {
        short[] data = new short[120];
        double time = (double) System.currentTimeMillis();

        try {
            DeviceAttribute devattr = dev.read_attribute("MEANLive");
            data = devattr.extractShortArray();

            meanChart.addData(mean1, time, (double) data[0]);
            meanChart.addData(mean2, time, (double) data[1]);
            meanChart.addData(mean3, time, (double) data[2]);
            meanChart.repaint();
        } catch (DevFailed e) {
            ErrorPane.showErrorMessage(null, dev.get_name(), e);
            T1.stop();
            this.setVisible(false);
        }


    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void initLiveGraph() {
        Color darkGreen = new Color(0, 153, 102);

        live1.setColor(Color.blue);
        live1.setName("Phase 1");
        live1.setLineWidth(2);

        live2.setColor(darkGreen);
        live2.setName("Phase 2");
        live2.setLineWidth(2);

        live3.setColor(Color.red);
        live3.setName("Phase 3");
        live3.setLineWidth(2);

        // Setup and trace theorical curves
        theo1.setColor(Color.blue);
        theo1.setName("Theorical Phase 1");
        theo1.setLineWidth(1);
        theo1.setColor(Color.blue);
        theo1.setStyle(JLDataView.STYLE_LONG_DASH);

        theo2.setColor(darkGreen);
        theo2.setName("Theorical Phase 2");
        theo2.setLineWidth(1);
        theo2.setStyle(JLDataView.STYLE_LONG_DASH);

        theo3.setColor(Color.red);
        theo3.setName("Theorical Phase 3");
        theo3.setLineWidth(1);
        theo3.setStyle(JLDataView.STYLE_LONG_DASH);

        // Reads Thersholds
        short[] data = new short[2];
        data[0] = TRIG_LEVELS;
        data[1] = 8;
        short[] values = new short[data[1]];
        try {
            DeviceData argin = new DeviceData();
            argin.insert(data);
            DeviceData argout = dev.command_inout("ReadValues", argin);
            values = argout.extractShortArray();
        } catch (DevFailed e) {
            Except.print_exception(e);
            JOptionPane.showMessageDialog(this,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
        }
        for (int i = 0 ; i<values.length ; i++)
            System.out.println("Thersholds[" + i + "]=" + values[i]);
        setChartTitlesAndAxis();
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void setChartTitlesAndAxis() {
        liveChart.setBackground(Color.white);
        liveChart.setChartBackground(new Color(238, 232, 170));
        liveChart.setHeaderFont(new Font("Dialog", Font.BOLD, 12));
        liveChart.setHeader("Volatge live values");
        liveChart.setLabelFont(new Font("Dialog", Font.PLAIN, 8));
        liveChart.setLabelPlacement(JLChart.LABEL_RIGHT);
        liveChart.getY1Axis().setName("Tension (Volts)");
        //liveChart.getY1Axis().setMaximum(max+500);
        //liveChart.getY1Axis().setMinimum(-max-500);
        liveChart.getY1Axis().setAutoScale(true);
        liveChart.getY1Axis().setGridVisible(true);
        liveChart.getY1Axis().setSubGridVisible(true);
        liveChart.getY1Axis().setAnnotation(JLAxis.VALUE_ANNO);
        liveChart.getXAxis().setName("number of samples");
        liveChart.getXAxis().setGridVisible(true);
        liveChart.getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        liveChart.getY1Axis().addDataView(live1);
        liveChart.getY1Axis().addDataView(live2);
        liveChart.getY1Axis().addDataView(live3);
        liveChart.setPreferredSize(new Dimension(700, 450));
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void readLiveValues() {
        short[] dataPh1 = new short[250];
        short[] dataPh2 = new short[250];
        short[] dataPh3 = new short[250];

        live1.reset();
        live2.reset();
        live3.reset();

        try {
            DeviceAttribute devattr1 = dev.read_attribute("AnalogicValuesPhase1");
            DeviceAttribute devattr2 = dev.read_attribute("AnalogicValuesPhase2");
            DeviceAttribute devattr3 = dev.read_attribute("AnalogicValuesPhase3");

            dataPh1 = devattr1.extractShortArray();
            dataPh2 = devattr2.extractShortArray();
            dataPh3 = devattr3.extractShortArray();

            // Calculate phi0 to match with dsp data
            phi0 = Math.asin((double) dataPh1[0] / 20000);

            if (dataPh1[1]>dataPh1[0]) {
                phase1positive = true;
                if (dataPh2[1]>dataPh2[0]) {
                    phase2positive = true;
                } else {
                    phase2positive = false;
                    for (int i = 0 ; i<dataPh2.length ; i++) {
                        dataPh2[i] = (short) -dataPh2[i];
                    }
                }

                if (dataPh3[1]>dataPh3[0]) {
                    phase3positive = true;
                } else {
                    phase3positive = false;
                    for (int i = 0 ; i<dataPh3.length ; i++) {
                        dataPh3[i] = (short) -dataPh3[i];
                    }
                }
            } else {
                phase1positive = false;
                for (int i = 0 ; i<dataPh1.length ; i++) {
                    dataPh1[i] = (short) -dataPh1[i];
                }
                if (dataPh2[1]>dataPh2[0]) {
                    phase2positive = true;
                } else {
                    phase2positive = false;
                    for (int i = 0 ; i<dataPh2.length ; i++) {
                        dataPh2[i] = (short) -dataPh2[i];
                    }
                }

                if (dataPh3[1]>dataPh3[0]) {
                    phase3positive = true;
                } else {
                    phase3positive = false;
                    for (int i = 0 ; i<dataPh3.length ; i++) {
                        dataPh3[i] = (short) -dataPh3[i];
                    }
                }

            }
            for (int i = 0 ; i<(dataPh1.length / 2) ; i++) {
                liveChart.addData(live1, i, (double) (dataPh1[i] * coeff));
                if (dataPh1[i]>max) {
                    max = (int) dataPh1[i];
                }
            }
            for (int i = 0 ; i<(dataPh2.length / 2) ; i++)
                liveChart.addData(live2, i, (double) (dataPh2[i + 41] * coeff)); // 125/3
            for (int i = 0 ; i<(dataPh3.length / 2) ; i++)
                liveChart.addData(live3, i, (double) (dataPh3[i + 83] * coeff)); // (125/3)*2
            //System.out.println("phase1positive="+phase1positive);
            //System.out.println("phase2positive="+phase2positive);
            //System.out.println("phase3positive="+phase3positive);
            if (first) {
                for (int i = 0 ; i<125 ; i++) {
                    if (phase1positive) {
                        datatheo1[i] = 20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * 50 * i * 0.00016 + phi0);
                        datatheo2[i] = 20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * 50 * i * 0.00016 + 2 * Math.PI / 3 + phi0);
                        datatheo3[i] = 20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * 50 * i * 0.00016 + 4 * Math.PI / 3 + phi0);
                    } else {
                        datatheo1[i] = 20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * 50 * i * 0.00016 - phi0);
                        datatheo2[i] = 20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * 50 * i * 0.00016 + 2 * Math.PI / 3 - phi0);
                        datatheo3[i] = 20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * 50 * i * 0.00016 + 4 * Math.PI / 3 - phi0);
                    }
                    liveChart.addData(theo1, i, datatheo1[i]);
                    liveChart.addData(theo2, i, datatheo2[i]);
                    liveChart.addData(theo3, i, datatheo3[i]);
                }
                first = false;
            }
            liveChart.repaint();
        } catch (DevFailed e) {
            ErrorPane.showErrorMessage(null, dev.get_name(), e);
            T1.stop();
            this.setVisible(false);
        }
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void scan() {
        T1.start();
        setVisible(true);
    }

    /**
     * Read Values in DSP.
     *
     * @param data : data to read in DPRAM..
     * @return short table of the read data.
     */
    /*-------------------------------------------------------------------------*/
    public short[] readValues2(short[] data) {
        short[] values = new short[data[1]];
        try {
            DeviceData argin = new DeviceData();
            argin.insert(data);
            DeviceData argout = dev.command_inout("ReadValues", argin);
            values = argout.extractShortArray();
        } catch (Exception e) {
            System.out.println("Catch exception in readValues() :" + e
                    + "\n Message :" + e.getMessage());
        }
        return values;
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private boolean readSensAndOffset() {
        short[] dataPh1 = new short[250];
        boolean isPositive;
        try {
            DeviceAttribute devattr1 = dev.read_attribute("AnalogicValuesPhase1");
            dataPh1 = devattr1.extractShortArray();
        } catch (DevFailed e) {
            ErrorPane.showErrorMessage(null, dev.get_name(), e);
            T1.stop();
            this.setVisible(false);
        }
        if (dataPh1[0]>0 & dataPh1[1]>dataPh1[0])
            isPositive = true;
        else
            isPositive = false;
        return isPositive;
    }
    //final JCheckBox displayTheoChB = new JCheckBox("Display theorical curve");
} // End of Class
