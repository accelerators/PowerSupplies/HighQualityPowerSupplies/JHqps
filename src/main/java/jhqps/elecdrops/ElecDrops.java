//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/ElecDrops.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: ElecDrops.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:02  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================

package jhqps.elecdrops;

/**
 * @author goudard
 * @version 1.0
 */

//	Import Standard Java Classes

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;

import javax.swing.*;
import java.util.Date;

//	Import Tango Classes
/*-------------------------------------------------------------------------*/

/**
 * This class defines the elecdrops object to access the tango device server.
 * This Device server has been written in C++ by Ph. Falaise
 * ElecDrops object allows to use Tango API Commands (like readTime method) or
 * Attributes.
 */
/*-------------------------------------------------------------------------*/

public class ElecDrops implements ElecDropDefs {

    //	Link to the Tango device server
//=================================
    public static DeviceProxy dev;

    //	Display debugging tips
//========================
    public static boolean trace = false;

    /*-------------------------------------------------------------------------*/

    /**
     * Default constructor :
     * Creates a connection for Tango Elecdrop Device Server using Tango API.
     */
    /*-------------------------------------------------------------------------*/
    public ElecDrops(String devname) {
        try {
            dev = new DeviceProxy(devname);
        } catch (DevFailed e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            dev = null;
            if (trace)
                Except.print_exception(e);
        }
    }
    /*-------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------*/
/* Main Program
/*-------------------------------------------------------------------------*/
    public static void main(String[] args) {


	/* Search for -trace argument
	-----------------------------*/
        for (int i = 1 ; i<=args.length ; i++) {
            if (args[i - 1].compareTo("-trace") == 0)
                trace = true;
            else
                System.out.println("Bad argument in the command line.");
        }
        ElecDrops gt = new ElecDrops("infra/d-drops/hqps");
        gt.DspTimeAttr(true);
        System.out.println("Time attribute = " + new Date(gt.DspTimeAttr(false)));
    }

    /**
     * returns DSP time.
     */
    /*-------------------------------------------------------------------------*/
    public long readTime() {
        long time = 0;
        try {
            DeviceData argout = dev.command_inout("GetDSPTime");
            time = argout.extractLong(); // previously cast to (long)
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
        }
        System.out.println("readTime->long time =" + time);
        return time;
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public String devStatus() {
        String status = "unknown";

        try {
            DeviceData argout = dev.command_inout("DevStatus");
            status = argout.extractString();
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
        }
        return status;
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public DevState devState() {
        DevState status = DevState.OFF;

        try {
            DeviceData argout = dev.command_inout("DevState");
            status = argout.extractDevState();
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
        }
        return status;
    }
    /*-------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public short[] readDrop(short Nb) {
        short[] drop = new short[14];
        if (dev != null)
            try {
                DeviceData argin = new DeviceData();
                argin.insert(Nb);
                DeviceData argout = dev.command_inout("ReadDrops", argin);
                drop = argout.extractShortArray();
            } catch (DevFailed e) {
                JOptionPane.showMessageDialog(null,
                        e.errors[0].desc,
                        e.errors[0].reason,
                        JOptionPane.ERROR_MESSAGE);
                if (trace)
                    Except.print_exception(e);
            }

        return drop;
    }

    /**
     * @param time : Time to set in the DSP.
     */
    /*-------------------------------------------------------------------------*/
    public void setTime(long time) {
        try {
            DeviceData argin = new DeviceData();
            argin.insert(time / 1000); // Convert to ms since 01/01/70
            dev.command_inout("SetTime", argin);
            String s = new Date(time).toString();
            JOptionPane.showMessageDialog(null,
                    "DSP Time has been set at " + s,
                    "Information",
                    JOptionPane.INFORMATION_MESSAGE);

        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
        }
    }

    /*-------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public short[] readValues(short[] data) {
        short[] values = new short[data[1]];
        try {
            DeviceData argin = new DeviceData();
            argin.insert(data);
            DeviceData argout = dev.command_inout("ReadValues", argin);
            values = argout.extractShortArray();
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
        }
        return values;
    }
    /*-------------------------------------------------------------------------*/

    /**
     * Set the analogic output values mode.
     * 0 means that the analogic values in the DPRAM is the 3 phases live.
     * 1 means that the analogic values in the DPRAM is trig on drops with a long acquisiton time (10 periods).
     * 2 means that the analogic values in the DPRAM is trig on drops with a short acquisiton time (2 periods).
     */
    /*-------------------------------------------------------------------------*/
    public void setAnalogicMode(short mode) {
        try {
            DeviceData argin = new DeviceData();
            argin.insert(mode);
            dev.command_inout("SetAnalogicMode", argin);
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
        }
    }
    /*-------------------------------------------------------------------------*/

    /**
     * Read the analogic output values mode setting by the SetAnalogicMode command.
     * 0 means that the analogic values in the DPRAM is the 3 phases live.
     * 1 means that the analogic values in the DPRAM is trig on drops with a long acquisiton time (10 periods).
     * 2 means that the analogic values in the DPRAM is trig on drops with a short acquisiton time (2 periods).
     */
    /*-------------------------------------------------------------------------*/
    public short readAnalogicMode() {
        short value = 0;
        try {
            DeviceData argout = dev.command_inout("ReadAnalogicMode");
            value = argout.extractShort();
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
        }
        return value;
    }

    /**
     * If write_it = true, it reads the DspTime attribute of pf/elec-drop/1.
     * If write_it = false, it write the DspTime attribute of pf/elec-drop/1.
     */
    /*-------------------------------------------------------------------------*/
    public long DspTimeAttr(boolean write_it) {
        long result;
        String attname = "DspTime";
        DeviceAttribute devattr = new DeviceAttribute(attname);

        try {
            if (write_it) {
                int t = (int) (System.currentTimeMillis() / 1000);
                devattr.insert(t);
                dev.write_attribute(devattr);
            }
            devattr = dev.read_attribute(attname);
            result = ((long) devattr.extractLong()) * 1000;

            //	And display if has been written
            if (write_it)
                JOptionPane.showMessageDialog(null,
                        "DSP Time has been set at " + new Date(result),
                        "Information",
                        JOptionPane.INFORMATION_MESSAGE);
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            if (trace)
                Except.print_exception(e);
            result = NOTOK;
        }
        return result;
    }
}
