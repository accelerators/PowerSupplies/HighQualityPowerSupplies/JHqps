//*********************************************************************
//  $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/DropGraph.java,v $
//
//  Project:   New Drop Appli
//
// Description:
//
//  $Author: goudard $
//
// $Log: DropGraph.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.1.1.1  2009/01/25 10:00:30  goudard
// Import to CVS.
//
// Revision 1.1.1.1  2008/11/06 15:47:31  goudard
// Imported using TkCVS
//
// Revision 1.12  2003/02/21 09:15:02  goudard
// Added compatibility with jserver ElecLogger.Added last drop graphical view.
//
// Revision 1.11  2002/09/19 11:45:52  goudard
// Auto update of the Jlist + time in milliseconds
//
// Revision 1.1  2002/08/12 11:27:21  goudard
// Added new ElecDropLiveMode, help on version, reading DSP time attributte
//
// Revision 1.1  2002/05/23 12:53:25  goudard
// Initial version.
//
//  
// Copyright 1995 by European Synchrotron Radiation Facility, Grenoble, France
//							 All Rights Reversed
// *********************************************************************/
/*
 * DropDisplay.java
 *
 * Created on August 13, 2002, 10:10 AM
 */
/**
 * @author Goudard
 */
package jhqps.elecdrops;

//	Import Standard Java Classes
//==============================

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.INumberSpectrum;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

//	Import ATK Classes
//====================

public class DropGraph extends JDialog implements ElecDropDefs {
    private AttributeList number_spectrum_atts;
    private INumberSpectrum spectrum1;
    private INumberSpectrum spectrum2;
    private INumberSpectrum spectrum3;
    //private JFrame 				trendFrame;
    private JPanel jPanel1;
    private JTabbedPane jTabbedPane1;
    private JButton exitBtn;

    /*-------------------------------------------------------------------------*/

    /**
     * Creates new form AtkPanel
     */
    /*-------------------------------------------------------------------------*/
    public DropGraph(JFrame parent, boolean modal, String devname) {
        super(parent, modal);
        try {
            number_spectrum_atts = new fr.esrf.tangoatk.core.AttributeList();
            initComponents();
            String attname1;
            attname1 = devname + "/AnalogicValuesPhase1";
            String attname2;
            attname2 = devname + "/AnalogicValuesPhase2";
            String attname3;
            attname3 = devname + "/AnalogicValuesPhase3";

            spectrum1 = (INumberSpectrum) number_spectrum_atts.add(attname1);
            spectrum2 = (INumberSpectrum) number_spectrum_atts.add(attname2);
            spectrum3 = (INumberSpectrum) number_spectrum_atts.add(attname3);
            INumberSpectrum[] spectrum = new INumberSpectrum[NB_PHASES];
            spectrum[0] = spectrum1;
            spectrum[1] = spectrum2;
            spectrum[2] = spectrum3;
            showNumberSpectrumAtts(spectrum);

        } catch (ConnectionException e) {
            System.out.println("Connection Exception :\n\t" + e.toString());
        }
        number_spectrum_atts.startRefresher();
        pack();
    }

    /**
     * Main method to perform tests...
     */
    /*-------------------------------------------------------------------------*/
    public static void main(String[] args) {
        new DropGraph(null, true, "infra/d-drops/hqps").setVisible(true);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void showNumberSpectrumAtts(INumberSpectrum[] data) {
        int nb_atts;
        int idx;

        nb_atts = number_spectrum_atts.getSize();

        SpectrumPanel[] att_tab = new SpectrumPanel[nb_atts];

        for (idx = 0; idx<nb_atts ; idx++) {
            data[idx] = (INumberSpectrum) number_spectrum_atts.getElementAt(idx);
            data[idx].setName("Phase " + (idx + 1) + " analogic values (V)");
            att_tab[idx] = new SpectrumPanel(data[idx], idx + 1);

            // Exctract the name of the Tab
            String name = (data[idx].getName()).substring(0, 7);
            jTabbedPane1.addTab(name, att_tab[idx]);
        }

    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void initComponents() {
        jTabbedPane1 = new JTabbedPane();
        jPanel1 = new JPanel();
        exitBtn = new JButton("Close");

        jPanel1.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints1;
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints1.insets = new Insets(4, 5, 5, 5);

        exitBtn.setFocusPainted(false);
        exitBtn.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                exitBtnClicked(e);
            }
        });

        jTabbedPane1.setTabPlacement(JTabbedPane.TOP);
        jTabbedPane1.setPreferredSize(new Dimension(1000, 500));
        jPanel1.add(jTabbedPane1, gridBagConstraints1);
        getContentPane().add(exitBtn, BorderLayout.SOUTH);
        getContentPane().add(jPanel1, BorderLayout.CENTER);
        this.setTitle("RMS Analogic values of the drop");
    }
    /*-------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void exitBtnClicked(MouseEvent e) {
        number_spectrum_atts.stopRefresher();
        this.setVisible(false);
    }
}    // End of class
