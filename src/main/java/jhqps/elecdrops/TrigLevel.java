//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/TrigLevel.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: TrigLevel.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:19  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:51  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:02  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================

/*
 * TrigLevel.java
 *
 * Created on 27 mai 2002, 09:15
 */

/**
 * @author goudard
 */
package jhqps.elecdrops;

//	Import graphic classes
//=========================

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.JSmoothLabel;
import fr.esrf.tangoatk.widget.util.WheelSwitch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//	Import tango classes
//======================

public class TrigLevel extends JFrame implements ElecDropDefs {

    public static short[] Thersholds = new short[7];
    public boolean change = false;
    /*-------------------------------------------------------------------------*/
    fr.esrf.TangoApi.DeviceProxy dev;

    /*-------------------------------------------------------------------------*/
    private int NB_WHEELS = 4;
    /*-------------------------------------------------------------------------*/
    // Variables declaration - do not modify
    //private JPanel jPanel1;
    private JSmoothLabel jLabel1;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel4;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel5;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel6;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel7;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel8;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel9;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel10;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel11;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel jLabel12;
    private JTextField maxRMS;
    private JTextField minfreq;
    private JTextField maxfreq;
    private JSmoothLabel minbackRMS;
    private JSmoothLabel maxbackRMS;
    private JSmoothLabel minbackfreq;
    private JSmoothLabel maxbackfreq;
    private JTextField minRMS;
    private JButton applyBtn;
    private JButton dismissBtn;
    private WheelSwitch[] Wheels = new WheelSwitch[4];
    private short[] thersholdsToChange = new short[4];

    /**
     * Creates new form TrigLevel
     */
    /*-------------------------------------------------------------------------*/
    public TrigLevel(fr.esrf.TangoApi.DeviceProxy device, String devname) {
        short[] data = new short[2];
        data[0] = TRIG_LEVELS;
        data[1] = NB_TRIG_LEVELS;

        device = dev;
        initComponents();
        System.out.println("devname = " + devname);
        try {
            dev = new DeviceProxy(devname);
        } catch (DevFailed e) {
            System.out.println("DevFailed Error");
            Except.print_exception(e);
            // TODO : Display DeviceProxy Creation failed!
        }
        long t1 = System.currentTimeMillis();
        readThersholds(data);
        System.out.println("read time=" + (System.currentTimeMillis() - t1));
    }

    /**
     * @param args the command line arguments
     */
    /*-------------------------------------------------------------------------*/
    public static void main(String args[]) {
        //TrigLevel T1 = new TrigLevel();
        //T1.setVisible(true);
        //short[] data = new short[2];
        //data[0] = 48;
        //data[1] = 8;
        //T1.readThersholds(data);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    /*-------------------------------------------------------------------------*/
    private void initComponents() {
        jLabel1 = new JSmoothLabel();
        jLabel5 = new JSmoothLabel();
        jLabel7 = new JSmoothLabel();
        jLabel10 = new JSmoothLabel();
        jLabel6 = new JSmoothLabel();
        jLabel4 = new JSmoothLabel();
        jLabel8 = new JSmoothLabel();
        jLabel9 = new JSmoothLabel();
        applyBtn = new JButton("Apply");
        dismissBtn = new JButton("Dismiss");
        maxRMS = new JTextField();
        minfreq = new JTextField();
        maxfreq = new JTextField();
        minRMS = new JTextField();
        minbackRMS = new JSmoothLabel();
        maxbackRMS = new JSmoothLabel();
        minbackfreq = new JSmoothLabel();
        maxbackfreq = new JSmoothLabel();
        jLabel11 = new JSmoothLabel();
        jLabel12 = new JSmoothLabel();

        jLabel1.setText("Max RMS (V)");
        jLabel5.setText("Min RMS (V)");
        jLabel7.setText("Max period (us)");
        jLabel10.setText("Min period (us)");
        jLabel6.setText("Max back RMS (V)");
        jLabel4.setText("Min back RMS(V)");
        jLabel8.setText("Min back period (us)");
        jLabel9.setText("Max back period (us)");
        minbackRMS.setText("--------.-");
        maxbackRMS.setText("--------.-");
        minbackfreq.setText("-----.--");
        maxbackfreq.setText("-----.--");

        for (int i = 0 ; i<NB_WHEELS ; i++) {
            Wheels[i] = new WheelSwitch();
            if (i<(NB_WHEELS / 2))
                Wheels[i].setFormat("%6d");    //	RMS format
            else
                Wheels[i].setFormat("%6d");    // period format
            Wheels[i].setBackground(Color.white);
        }

        setLocation(250, 250);
        setSize(600, 300);
        getContentPane().setBackground(Color.white);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        GridBagConstraints gridBagConstraints1;
        Font font = new Font("Dialog", 0, 14);
        setTitle("DROPS TRIGGER LEVELS");
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                exitForm(evt);
            }
        });

        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 2;
        gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
        maxRMS.setHorizontalAlignment(JTextField.RIGHT);
        maxRMS.setToolTipText("Enter a new value and press Enter to modify");
        maxRMS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                maxRMSActionPerformed(evt);
            }
        });
        getContentPane().add(Wheels[1], gridBagConstraints1);

        //gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 3;
        //gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
        minRMS.setHorizontalAlignment(JTextField.RIGHT);
        minRMS.setToolTipText("Enter a new value and press Enter to modify");
        minRMS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                minRMSActionPerformed(evt);
            }
        });
        getContentPane().add(Wheels[0], gridBagConstraints1);

        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 4;
        gridBagConstraints1.gridy = 2;
        gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
        maxfreq.setHorizontalAlignment(JTextField.RIGHT);
        maxfreq.setToolTipText("Enter a new value and press Enter to modify");
        maxfreq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                maxfreqActionPerformed(evt);
            }
        });
        getContentPane().add(Wheels[3], gridBagConstraints1);

        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 4;
        gridBagConstraints1.gridy = 3;
        gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
        minfreq.setHorizontalAlignment(JTextField.RIGHT);
        minfreq.setToolTipText("Enter a new value and press Enter to modify");
        minfreq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                minfreqActionPerformed(evt);
            }
        });
        getContentPane().add(Wheels[2], gridBagConstraints1);

        minbackRMS.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 5;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(minbackRMS, gridBagConstraints1);

        maxbackRMS.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 4;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(maxbackRMS, gridBagConstraints1);

        minbackfreq.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 4;
        gridBagConstraints1.gridy = 5;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(minbackfreq, gridBagConstraints1);

        maxbackfreq.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 4;
        gridBagConstraints1.gridy = 4;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(maxbackfreq, gridBagConstraints1);

        jLabel1.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 2;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel1, gridBagConstraints1);

        jLabel4.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 5;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel4, gridBagConstraints1);

        jLabel5.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 3;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel5, gridBagConstraints1);

        jLabel6.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 4;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel6, gridBagConstraints1);

        jLabel7.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 2;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel7, gridBagConstraints1);

        jLabel8.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 4;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel8, gridBagConstraints1);

        jLabel9.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 5;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel9, gridBagConstraints1);

        jLabel10.setFont(font);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 3;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel10, gridBagConstraints1);

        applyBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                applyBtnActionPerformed(evt);
            }
        });
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 6;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(applyBtn, gridBagConstraints1);

        dismissBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dismissBtnActionPerformed(evt);
            }
        });
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 6;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(dismissBtn, gridBagConstraints1);

        jLabel11.setFont(new Font("Dialog", 3, 12));
        jLabel11.setText("FREQUENCY TRIGGERS");
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel11, gridBagConstraints1);

        jLabel12.setFont(new Font("Dialog", 3, 12));
        jLabel12.setText("RMS TRIGGERS");
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(jLabel12, gridBagConstraints1);
        //pack();

    }

    /**
     * Apply button
     */
    /*-------------------------------------------------------------------------*/
    private void applyBtnActionPerformed(ActionEvent evt) {
        short[] converted_values = new short[NB_TRIG_LEVELS];

        for (int i = 0 ; i<NB_WHEELS ; i++) {
            converted_values[i] = (short) Wheels[i].getValue();
            System.out.println("new_val=" + converted_values[i]);
        }
        updateThersholds(converted_values);
        short[] data = new short[2];
        data[0] = TRIG_LEVELS;
        data[1] = NB_TRIG_LEVELS;
        readThersholds(data);
        pack();
    }

    /**
     * Dismiss button
     */
    /*-------------------------------------------------------------------------*/
    private void dismissBtnActionPerformed(ActionEvent evt) {
        setVisible(false);
    }

    /**
     * Action Performed in maxRMS TextField
     */
    /*-------------------------------------------------------------------------*/
    private void maxRMSActionPerformed(ActionEvent evt) {
        String newmaxRMS = maxRMS.getText();
        try {
            Thersholds[1] = (new Short(newmaxRMS)).shortValue();
            if (ElecDropsGUI.trace)
                System.out.println("new thershold : " + Thersholds[1]);
            applyBtn.setText("Set new Thersholds");
            change = true;
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "Please enter a number, and not a string.",
                    "Number Format Exception",
                    JOptionPane.ERROR_MESSAGE);
            maxRMS.setText("");
            applyBtn.setText("Dismiss");
        }
        pack();
    }

    /**
     * Action Performed in minRMS TextField
     */
    /*-------------------------------------------------------------------------*/
    private void minRMSActionPerformed(ActionEvent evt) {
        String newminRMS = minRMS.getText();
        try {
            Thersholds[0] = new Short(newminRMS).shortValue();
            if (ElecDropsGUI.trace)
                System.out.println("new thershold : " + Thersholds[0]);
            applyBtn.setText("Set new Thersholds");
            change = true;
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "Please enter a number, and not a string.",
                    "Number Format Exception",
                    JOptionPane.ERROR_MESSAGE);
            minRMS.setText("");
            applyBtn.setText("Dismiss");
        }
        pack();
    }

    /**
     * Action Performed in maxfreq TextField
     */
    /*-------------------------------------------------------------------------*/
    private void maxfreqActionPerformed(ActionEvent evt) {
        String newmaxfreq = maxfreq.getText();
        try {
            Thersholds[5] = new Short(newmaxfreq).shortValue();
            if (ElecDropsGUI.trace)
                System.out.println("new thershold : " + Thersholds[5]);
            applyBtn.setText("Set new Thersholds");
            change = true;
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "Please enter a number, and not a string.",
                    "Number Format Exception",
                    JOptionPane.ERROR_MESSAGE);
            maxfreq.setText("");
            applyBtn.setText("Dismiss");
        }
    }

    /**
     * Action Performed in minfreq TextField
     */
    /*-------------------------------------------------------------------------*/
    private void minfreqActionPerformed(ActionEvent evt) {
        String newminfreq = minfreq.getText();
        try {
            Thersholds[4] = new Short(newminfreq).shortValue();
            if (ElecDropsGUI.trace)
                System.out.println("new thershold : " + Thersholds[4]);
            applyBtn.setText("Set new Thersholds");
            change = true;
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "Please enter a number, and not a string.",
                    "Number Format Exception",
                    JOptionPane.ERROR_MESSAGE);
            minfreq.setText("");
            applyBtn.setText("Dismiss");
        }
    }

    /**
     * Setting new thersholds
     */
    /*-------------------------------------------------------------------------*/
    private void updateThersholds(short[] Thersholds) {

        thersholdsToChange[0] = Thersholds[0];
        thersholdsToChange[1] = Thersholds[1];
        thersholdsToChange[2] = Thersholds[2];
        thersholdsToChange[3] = Thersholds[3];
        //if(ElecDropsGUI.trace)
        System.out.println("Updating thersholds :\nMin RMS = " + thersholdsToChange[0] + "\nMaxRMS = "
                + thersholdsToChange[1] + "\nMinperiod = "
                + thersholdsToChange[2] + "\nMaxperiod = "
                + thersholdsToChange[3] + "\n");
        try {
            DeviceData argin = new DeviceData();
            argin.insert(thersholdsToChange);
            dev.command_inout("SetThresholds", argin);
            JOptionPane.showMessageDialog(this,
                    "New settings have been sent to DSP.",
                    "Thersholds settings",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (DevFailed e) {
            Except.print_exception(e);
            JOptionPane.showMessageDialog(this,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /*-------------------------------------------------------------------------*/
    public void readThersholds(short[] data) {
        short[] values = new short[data[1]];
        try {
            DeviceData argin = new DeviceData();
            argin.insert(data);
            DeviceData argout = dev.command_inout("ReadValues", argin);
            values = argout.extractShortArray();
            this.setVisible(true);
        } catch (DevFailed e) {
            Except.print_exception(e);
            JOptionPane.showMessageDialog(this,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    JOptionPane.ERROR_MESSAGE);
            this.setVisible(false);
        }
        String[] convert_val = new String[NB_TRIG_LEVELS];
        for (int i = 0 ; i<NB_TRIG_LEVELS ; i++) {
            if (i<4)
                convert_val[i] = new String(("" + values[i]));
            if (i >= 4)
                convert_val[i] = new String(("" + values[i]));
        }
        Wheels[0].setValue(values[0]);
        Wheels[1].setValue(values[1]);
        minbackRMS.setText(convert_val[2]);
        maxbackRMS.setText(convert_val[3]);
        Wheels[2].setValue(values[4]);
        Wheels[3].setValue(values[5]);
        minbackfreq.setText(convert_val[6]);
        maxbackfreq.setText(convert_val[7]);

        for (int i = 0 ; i<7 ; i++) {
            Thersholds[i] = values[i];
            //if(ElecDropsGUI.trace)
            System.out.println("Current Thersholds " + i + " " + Thersholds[i]);
        }
        thersholdsToChange[0] = Thersholds[0];
        thersholdsToChange[1] = Thersholds[1];
        thersholdsToChange[2] = Thersholds[4];
        thersholdsToChange[3] = Thersholds[5];
        pack();
    }

    /**
     * Exit the Application
     */
    /*-------------------------------------------------------------------------*/
    private void exitForm(WindowEvent evt) {
        setVisible(false);
    }
    // End of variables declaration

}
