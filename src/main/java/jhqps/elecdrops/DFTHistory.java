//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/DFTHistory.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: DFTHistory.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.4  2009/01/30 15:26:49  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
// Revision 1.3  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================

package jhqps.elecdrops;
/**
 * @author : O. Goudard
 */

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.*;
import fr.esrf.TangoDs.Except;
import fr.esrf.TangoDs.TangoConst;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.JSmoothLabel;
import fr.esrf.tangoatk.widget.util.Splash;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//import tool_panels.spectrum_histo.Histo_3D._3D_Record;
//===============================================================

/**
 * JDialog Class to display info
 *
 * @author Olivier Goudard
 */
//===============================================================


public class DFTHistory extends JDialog {
    private static final String[] menu_labels = {
            "Set refresher period",
    };
    private static final int REFRESHER_PERIOD = 0;
    public static Splash splash;
    static String[] labels = null;
    public String attr_THD;
    private Component parent;
    private String attname;
    private JSmoothLabel titleLabel;
    private long period = 3000;
    private SpectrumViewer viewer;
    private boolean running = true;

    //===============================================================
    //=======================================================
    //=======================================================
    private int splash_progress = 5;
    //===============================================================
    //===============================================================
    private JButton exceptionBtn;

    /**
     * Creates new form SpectrumHisto
     */
    //===============================================================
    public DFTHistory(JFrame parent, String attname, String attnameTHD, String device, boolean modal) throws DevFailed {
        super(parent, attname, modal);
        createDialog(parent, attname, attnameTHD, device);
        pack();
    }

    //===============================================================
    //===============================================================
    public DFTHistory(JDialog parent, String attname, String attnameTHD, String device) throws DevFailed {
        this(parent, attname, attnameTHD, device, true);
    }

    //===============================================================
    //===============================================================
    public DFTHistory(JDialog parent, String attname, String attnameTHD, String device, boolean modal) throws DevFailed {
        super(parent, attname, modal);
        createDialog(parent, attname, attnameTHD, device);
        System.out.println(attname.toString() + " imported.");
        pack();
    }

    //===============================================================
    //===============================================================
    static public boolean supportedType(int type) {
        switch (type) {
            case TangoConst.Tango_DEV_DOUBLE:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_FLOAT:
                return true;
        }
        return false;
    }

    //===============================================================
    //===============================================================
    private void createDialog(Component parent, String attname, String attname_THD, String device) throws DevFailed {
        //	Build title and start splash screen
        String title = "20kV DFT history on " + device.substring(12).toUpperCase() + ". ";

        splash = new Splash();
        splash.setTitle(title);
        increaseSplashProgress(5, "Building UI");

        //	Build object
        this.parent = parent;
        this.attname = attname;
        this.attr_THD = attname_THD;
        initComponents();

        viewer = new SpectrumViewer(this);
        getContentPane().add(viewer, BorderLayout.CENTER);
        ATKGraphicsUtils.centerDialog(this);
        setTitle(title);
        splash.progress(100);
        splash.setVisible(false);
        //Sleeping 1 sec because the title is not correctly set otherwise.
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        pack();
    }

    private void increaseSplashProgress(int i, String message) {
        splash_progress += i;
        if (splash_progress>99)
            splash_progress = 99;
        splash.progress(splash_progress);
        splash.setMessage(message);
    }

    private void initComponents() {
        JPanel jPanel1 = new JPanel();
        JButton cancelBtn = new JButton();
        JPanel jPanel2 = new JPanel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        cancelBtn.setText("Dismiss");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });
        jPanel1.add(cancelBtn);

        exceptionBtn = new JButton();
        exceptionBtn.setText("Error");
        exceptionBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exceptionBtnActionPerformed(evt);
            }
        });
        exceptionBtn.setVisible(false);
        jPanel1.add(exceptionBtn);
        jPanel1.setBackground(java.awt.Color.white);
        getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

        titleLabel = new JSmoothLabel();
        titleLabel.setFont(new java.awt.Font("Dialog", 1, 18));
        titleLabel.setSize(200, 10);
        jPanel2.add(titleLabel);
        jPanel2.setBackground(java.awt.Color.white);
        getContentPane().add(jPanel2, java.awt.BorderLayout.NORTH);
    }

    //===============================================================
    //===============================================================
    private void exceptionBtnActionPerformed(java.awt.event.ActionEvent evt) {
        DevFailed e = viewer.getException();
        if (e != null)
            ErrorPane.showErrorMessage(this, null, e);
    }

    //===============================================================

    //===============================================================
    //===============================================================
    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {
        doClose();
    }

    //===============================================================

    /**
     * Closes the dialog
     */
    //===============================================================
    private void closeDialog(java.awt.event.WindowEvent evt) {
        doClose();
    }

    /**
     * Closes the dialog
     */
    //===============================================================
    private void doClose() {
        if (parent.getWidth()>0) {
            setVisible(false);
            dispose();
        } else {
            running = false;
            System.exit(0);
        }
    }

    //===============================================================
    //===============================================================
    class SpectrumViewer extends jhqps.elecdrops.ElecDropDFTHisto_3D {
        private AttributeProxy attribute;
        private AttributeProxy attributeTHD;
        private Refresher refresher;
        private JDialog parent;

        //===========================================================
        SpectrumViewer(JDialog parent) throws DevFailed {
            super(parent);
            //super(parent, 600);
            this.parent = parent;
            setSearchBestFitVisible(true);
            attribute = new AttributeProxy(attname);
            attributeTHD = new AttributeProxy(attr_THD);
            readHistory();

            setPollingPeriod();
            refresher = new Refresher(this, attribute, attributeTHD);
            refresher.start();

            //  Add buttons to default popup menu
            JMenuItem btn;
            for (String label : menu_labels) {
                btn = new JMenuItem(label);
                btn.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        histoMenuActionPerformed(evt);
                    }
                });
                addToMenu(btn);
            }
        }
        //======================================================

        /**
         * Get the attribute polling
         * to set the period for refresher.
         */

        //======================================================
        private void setPollingPeriod() {
            try {
                period = attribute.get_polling_period();
            } catch (NoSuchMethodError nsme) {
                try {
                    //	The method does not already exist, do it myself
                    //System.err.println("TangORB is NOT up to date !!!");
                    DeviceProxy dev = attribute.getDeviceProxy();

                    DbDatum datum = dev.get_property("polled_attr");
                    String[] str = datum.extractStringArray();
                    String attname = attribute.name().toLowerCase();
                    if (str != null) {
                        for (int i = 0 ; i<str.length ; i += 2) {
                            //	Check for name
                            if (str[i].toLowerCase().equals(attname)) {
                                //	not last index.
                                if (i<str.length - 1) {
                                    period = Integer.parseInt(str[i + 1]);
                                }
                            }
                        }
                    } else
                        Except.throw_exception("NOT_POLLED",
                                "Attribute " + attribute.name() + " not polled",
                                "SpectrumHisto.setPollingPeriod()");
                } catch (NumberFormatException ex) {
                    System.err.println(ex);
                } catch (DevFailed e) {
                    if (e.errors[0].reason.equals("NOT_POLLED")) {
                        e.errors[0].desc += "\n\n - The refresher period is set 3 seconds.\n" +
                                " - There is no history to be read.\n" +
                                " - The fit range cannot be done.";
                    }
                    splash.setVisible(false);
                    ErrorPane.showErrorMessage(new JFrame(), null, e);
                }
            } catch (DevFailed e) {
                //	If not polled -> does not change period.
                if (e.errors[0].reason.equals("NOT_POLLED")) {
                    e.errors[0].desc += "\n\n - The refresher period is set 3 seconds.\n" +
                            " - There is no history to be read.\n" +
                            " - The fit range cannot be done.";
                }
                splash.setVisible(false);
                ErrorPane.showErrorMessage(new JFrame(), null, e);
            }
        }

        //======================================================
        private void histoMenuActionPerformed(ActionEvent evt) {
            String cmd = evt.getActionCommand();
            if (cmd.equals(menu_labels[REFRESHER_PERIOD])) {
                int nb_sec = (int) period / 1000;
                String strval = "" + nb_sec;
                boolean ok = false;
                while (!ok) {
                    //	Get delay as string
                    if ((strval = (String)
                            JOptionPane.showInputDialog(parent,
                                    "Refresher Period in seconds : ",
                                    "",
                                    JOptionPane.QUESTION_MESSAGE,
                                    null, null, strval)) == null)
                        return;

                    //	Convert to int
                    try {
                        nb_sec = Integer.parseInt(strval);
                        //	Check value
                        if (nb_sec<1)
                            //app_util.PopupError.show(this,
                            System.out.println("The period must be higher than 1 second");
                        else
                            ok = true;
                    } catch (NumberFormatException e) {
                        //app_util.PopupError.show(this, e.toString());
                        System.out.println(e.toString());
                    }
                }
                period = 1000 * nb_sec;
            }
        }

        //===========================================================
        private void readHistory() {
            try {
                System.out.println("Reading History");
                increaseSplashProgress(50, "Reading History");
                DeviceDataHistory[] histo = attribute.history();
                System.out.println(histo.length + " data read");
                increaseSplashProgress(20, "Updating image");
                addHistoData(histo);
                setPreferredSize(new Dimension(histo.length, getYSize() + 50));
            } catch (DevFailed e) {
                //	if cannot read history --> continue
                Except.print_exception(e);
            }
        }

        //===========================================================
        void addData(double[] data) {
            addHistoData(data);
            updateImage();
        }

        //===========================================================
        DevFailed getException() {
            return refresher.getException();
        }

    }

    //===============================================================
    //===============================================================
    class Refresher extends Thread {
        private AttributeProxy att;
        private AttributeProxy att_THD;
        private SpectrumViewer viewer;
        private DevFailed dev_failed;

        //===========================================================
        Refresher(SpectrumViewer viewer, AttributeProxy att, AttributeProxy att_THD) {
            this.viewer = viewer;
            this.att = att;
            this.att_THD = att_THD;
        }

        //===========================================================
        DevFailed getException() {
            return dev_failed;
        }

        //===========================================================
        public void run() {
            while (running) {
                double d_period = (double) period / 1000;
                try {
                    DeviceAttribute res = att_THD.read();
                    Double result = res.extractDouble();
                    titleLabel.setText("DFT History  (" + d_period + " s) | THD = " + result.toString().substring(0, 4) + " %");
                } catch (DevFailed e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                try {
                    DeviceAttribute da = att.read();
                    switch (da.getType()) {
                        case TangoConst.Tango_DEV_DOUBLE:
                            viewer.addData(da.extractDoubleArray());
                            break;
                        case TangoConst.Tango_DEV_LONG: {
                            int[] values = da.extractLongArray();
                            double[] d_values = new double[values.length];
                            for (int i = 0 ; i<values.length ; i++)
                                d_values[i] = (double) values[i];
                            viewer.addData(d_values);
                        }
                        break;
                        case TangoConst.Tango_DEV_USHORT: {
                            int[] values = da.extractUShortArray();
                            double[] d_values = new double[values.length];
                            for (int i = 0 ; i<values.length ; i++)
                                d_values[i] = (double) values[i];
                            viewer.addData(d_values);
                        }
                        break;
                        case TangoConst.Tango_DEV_SHORT: {
                            short[] values = da.extractShortArray();
                            double[] d_values = new double[values.length];
                            for (int i = 0 ; i<values.length ; i++)
                                d_values[i] = (double) values[i];
                            viewer.addData(d_values);
                        }
                        break;
                        case TangoConst.Tango_DEV_FLOAT: {
                            float[] values = da.extractFloatArray();
                            double[] d_values = new double[values.length];
                            for (int i = 0 ; i<values.length ; i++)
                                d_values[i] = (double) values[i];
                            viewer.addData(d_values);
                        }
                        break;
                        default:
                            Except.throw_exception("TYPE_NOT_SUPPORTED",
                                    TangoConst.Tango_CmdArgTypeName[da.getType()] +
                                            "\n   is a non supported type !",
                                    "SpectrumHisto.Refresher.run()");

                    }
                    exceptionBtn.setVisible(false);
                } catch (DevFailed e) {
                    if (e.errors[0].reason.equals("TYPE_NOT_SUPPORTED")) {
                        splash.setVisible(false);
                        ErrorPane.showErrorMessage(new JFrame(), null, e);
                        return;
                    }
                    //	else
                    Except.print_exception(e);
                    dev_failed = e;
                    exceptionBtn.setVisible(true);
                    viewer.addData(null);
                }
                try {
                    sleep(period);
                } catch (Exception e) {
                }

            }
        }
    }
}
