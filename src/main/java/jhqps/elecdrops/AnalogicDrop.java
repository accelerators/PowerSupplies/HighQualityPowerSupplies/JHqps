/**
 * //  $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/AnalogicDrop.java,v $
 * //
 * //  Project:   New Drop Appli
 * //
 * // Description:
 * //
 * //  $Author: goudard $
 * //
 * // $Log: AnalogicDrop.java,v $
 * // Revision 1.55  2009/08/25 13:53:22  goudard
 * // Modifications of HqpsBinIn and HqpsBinOut reading.
 * // Added :
 * //  - Incoherent state
 * //  - wago uninitialized state
 * //  - conditions to normal and economy mode detection.
 * //
 * // Modification of ElecDropStat : isolation curve added.
 * //
 * // Revision 1.54  2009/08/25 11:39:18  goudard
 * // Change behaviour when wago plc is unreachable (network problem)
 * //
 * // Revision 1.53  2009/08/18 11:33:50  goudard
 * // File JHqpsParameters.java added to CVS
 * // elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
 * //
 * // Revision 1.51  2009/06/15 13:43:01  goudard
 * // Modifications in
 * // 	- state management of HQPSGlobal state button.
 * // 	- FFT for jhqps.elecdrops.
 * // 	- Main Parameters window.
 * //
 * // Revision 1.1.1.1  2009/01/25 10:00:30  goudard
 * // Import to CVS.
 * //
 * // Revision 1.1.1.1  2008/11/06 15:47:31  goudard
 * // Imported using TkCVS
 * //
 * // Revision 1.12  2003/02/21 09:15:02  goudard
 * // Added compatibility with jserver ElecLogger.Added last drop graphical view.
 * //
 * // Revision 1.11  2002/09/19 11:45:52  goudard
 * // Auto update of the Jlist + time in milliseconds
 * //
 * // Revision 1.1  2002/08/12 11:27:21  goudard
 * // Added new ElecDropLiveMode, help on version, reading DSP time attributte
 * //
 * // Revision 1.1  2002/05/23 12:53:25  goudard
 * // Initial version.
 * //
 * //
 * // Copyright 1995 by European Synchrotron Radiation Facility, Grenoble, France
 * //							 All Rights Reversed
 * //
 * AnalogicDrop.java
 * <p>
 * Created on June 11, 2002, 9:42 AM
 * <p>
 * AnalogicDrop.java
 * <p>
 * Created on June 11, 2002, 9:42 AM
 */
/**
 * AnalogicDrop.java
 *
 * Created on June 11, 2002, 9:42 AM
 */

package jhqps.elecdrops;

/**
 * @author goudard
 */

//	Import Standart Classes
//=========================

import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLChart;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//	Import ATK Classes
//====================

public class AnalogicDrop extends JDialog implements ElecDropDefs, IJLChartListener {

    //private int 						phase =0;
    public int maxPh1 = 0;
    public int maxPh2 = 0;
    public int maxPh3 = 0;
    private JPanel CenterPl;
    private JPanel SouthPl;
    //private JTable 						jTable1;
//private JCheckBox 					displayTheoChB;
//private JCheckBox 					displayErrorChB;
    private JButton CloseBtn;
    private JTabbedPane jTabbedPane1;
    private JLDataView theoDataPh1;
    private JLDataView theoDataPh2;
    private JLDataView theoDataPh3;
    private JLDataView errorCurvePh1;
    private JLDataView errorCurvePh2;
    private JLDataView errorCurvePh3;
    private JLDataView AnalogicValuesPh1 = new JLDataView();
    private JLDataView AnalogicValuesPh2 = new JLDataView();
    private JLDataView AnalogicValuesPh3 = new JLDataView();
    //private JLabel                      deltav;
    private JLChart AnalogicValuesChartPh1;
    private JLChart AnalogicValuesChartPh2;
    private JLChart AnalogicValuesChartPh3;
    //  Phase calculation variables
    private double fitted_phasePh1;
    private double fitted_phasePh2;
    private double fitted_phasePh3;
    //private double[]                    d;
    private double[] errResultPh1;
    private double[] errResultPh2;
    private double[] errResultPh3;
    private double[][] theo;
    private double[][] fitTablePh1;
    private double[][] fitTablePh2;
    private double[][] fitTablePh3;
    private int best_phiPh1;
    private int best_phiPh2;
    private int best_phiPh3;

    /*-------------------------------------------------------------------------*/

    /**
     *	Creates new form AnalogicDrop .
     */
    /*-------------------------------------------------------------------------*/
    public AnalogicDrop(JFrame parent, boolean modal) {
        super(parent, modal);
        maxPh1 = maxPh2 = maxPh3 = 0;
        for (int i = 1 ; i<250 ; i++) {
            AnalogicValuesPh1.add(i * 0.8, jhqps.elecdrops.DropDisplay.thisDrop.analogicValuesPh1[i] * coeff);
            AnalogicValuesPh2.add(i * 0.8, DropDisplay.thisDrop.analogicValuesPh2[i] * coeff);
            AnalogicValuesPh3.add(i * 0.8, DropDisplay.thisDrop.analogicValuesPh3[i] * coeff);
            if (DropDisplay.thisDrop.analogicValuesPh1[i]>maxPh1)
                maxPh1 = DropDisplay.thisDrop.analogicValuesPh1[i];
            if (DropDisplay.thisDrop.analogicValuesPh2[i]>maxPh2)
                maxPh2 = DropDisplay.thisDrop.analogicValuesPh2[i];
            if (DropDisplay.thisDrop.analogicValuesPh3[i]>maxPh3)
                maxPh3 = DropDisplay.thisDrop.analogicValuesPh3[i];
        }

        DropDisplay.thisDrop.toString();
        initComponents();
        initTheoricalPhase();
        initTheoricalCurve();
        pack();
    }
    /*-------------------------------------------------------------------------*/

    /**
     *
     */
    /*-------------------------------------------------------------------------*/
    private void initComponents() {
        AnalogicValuesChartPh1 = new JLChart();
        AnalogicValuesChartPh2 = new JLChart();
        AnalogicValuesChartPh3 = new JLChart();
        CenterPl = new JPanel();
        SouthPl = new JPanel();
  /*  jTable1 					= new JTable();
    displayTheoChB 				= new JCheckBox("Display theorical curve",true);
    displayErrorChB             = new JCheckBox("Display error",true);
    deltav                      = new JLabel("\u0394U/U = -.- volts");
*/
        jTabbedPane1 = new JTabbedPane();
        CloseBtn = new JButton("Close");


        getContentPane().setLayout(new BorderLayout());
        setSize(200, 200);
        setTitle("Drop Statistics");
        setBackground(Color.white);
        setLocation(200, 200);

        jTabbedPane1.addTab("Phase 1", AnalogicValuesChartPh1);
        jTabbedPane1.addTab("Phase 2", AnalogicValuesChartPh2);
        jTabbedPane1.addTab("Phase 3", AnalogicValuesChartPh3);
        jTabbedPane1.setTabPlacement(JTabbedPane.TOP);
        jTabbedPane1.setPreferredSize(new Dimension(1000, 500));

        initAnalogicValuesCharts();

        AnalogicValuesPh1.setColor(Color.red);
        AnalogicValuesPh2.setColor(Color.red);
        AnalogicValuesPh3.setColor(Color.red);
        AnalogicValuesPh1.setName("Phase1");
        AnalogicValuesPh2.setName("Phase2");
        AnalogicValuesPh3.setName("Phase3");

        CenterPl.setLayout(new GridBagLayout());
        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.gridx = 0;
        gridBC.gridy = 0;
        gridBC.fill = GridBagConstraints.BOTH;
        gridBC.weightx = 1.0;
        gridBC.weighty = 1.0;
        CenterPl.add(jTabbedPane1, gridBC);

        CloseBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                CloseBtnActionPerformed(evt);
            }
        });

        //  Placing close button
        SouthPl.setLayout(new BorderLayout());
        SouthPl.add(CloseBtn);

        getContentPane().add(CenterPl, BorderLayout.CENTER);
        getContentPane().add(SouthPl, BorderLayout.SOUTH);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void CloseBtnActionPerformed(ActionEvent evt) {
        this.setVisible(false);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void initAnalogicValuesCharts() {
        // AnalogicValuesCharts chart setup
        AnalogicValuesChartPh1.setHeader("Analogic values.");
        AnalogicValuesChartPh1.setHeaderVisible(true);
        AnalogicValuesChartPh1.setBackground(Color.white);
        AnalogicValuesChartPh1.setChartBackground(new Color(238, 232, 170));
        AnalogicValuesChartPh1.setHeaderFont(new Font("Arial", Font.BOLD, 18));
        AnalogicValuesChartPh1.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        AnalogicValuesChartPh1.getY1Axis().setName("Tension (V)");
        AnalogicValuesChartPh1.getY1Axis().setAutoScale(false);
        AnalogicValuesChartPh1.getY1Axis().setGridVisible(true);
        AnalogicValuesChartPh1.getY1Axis().setSubGridVisible(true);
        AnalogicValuesChartPh1.getXAxis().setName("Time (ms)");
        AnalogicValuesChartPh1.getXAxis().setGridVisible(true);
        AnalogicValuesChartPh1.getXAxis().setAutoScale(false);
        AnalogicValuesChartPh1.getXAxis().setMinimum(0);
        AnalogicValuesChartPh1.getXAxis().setMaximum(200);
        AnalogicValuesChartPh1.setPreferredSize(new Dimension(480, 360));
        AnalogicValuesChartPh1.getY1Axis().setMaximum((maxPh1 * coeff) + 250);
        AnalogicValuesChartPh1.getY1Axis().setMinimum(-(maxPh1 * coeff) - 250);

        AnalogicValuesChartPh2.setHeader("Analogic values.");
        AnalogicValuesChartPh2.setHeaderVisible(true);
        AnalogicValuesChartPh2.setBackground(Color.white);
        AnalogicValuesChartPh2.setChartBackground(new Color(238, 232, 170));
        AnalogicValuesChartPh2.setHeaderFont(new Font("Arial", Font.BOLD, 18));
        AnalogicValuesChartPh2.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        AnalogicValuesChartPh2.getY1Axis().setName("Tension (V)");
        AnalogicValuesChartPh2.getY1Axis().setAutoScale(false);
        AnalogicValuesChartPh2.getY1Axis().setGridVisible(true);
        AnalogicValuesChartPh2.getY1Axis().setSubGridVisible(true);
        AnalogicValuesChartPh2.getXAxis().setName("Time (ms)");
        AnalogicValuesChartPh2.getXAxis().setGridVisible(true);
        AnalogicValuesChartPh2.getXAxis().setAutoScale(false);
        AnalogicValuesChartPh2.getXAxis().setMinimum(0);
        AnalogicValuesChartPh2.getXAxis().setMaximum(200);
        AnalogicValuesChartPh2.setPreferredSize(new Dimension(480, 360));
        AnalogicValuesChartPh2.getY1Axis().setMaximum((maxPh2 * coeff) + 250);
        AnalogicValuesChartPh2.getY1Axis().setMinimum(-(maxPh2 * coeff) - 250);

        AnalogicValuesChartPh3.setHeader("Tension vs time recorded during the drop.");
        AnalogicValuesChartPh3.setHeaderVisible(true);
        AnalogicValuesChartPh3.setBackground(Color.white);
        AnalogicValuesChartPh3.setChartBackground(new Color(238, 232, 170));
        AnalogicValuesChartPh3.setHeaderFont(new Font("Arial", Font.BOLD, 18));
        AnalogicValuesChartPh3.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        AnalogicValuesChartPh3.getY1Axis().setName("Tension (V)");
        AnalogicValuesChartPh3.getY1Axis().setAutoScale(false);
        AnalogicValuesChartPh3.getY1Axis().setGridVisible(true);
        AnalogicValuesChartPh3.getY1Axis().setSubGridVisible(true);
        AnalogicValuesChartPh3.getXAxis().setName("Time (ms)");
        AnalogicValuesChartPh3.getXAxis().setGridVisible(true);
        AnalogicValuesChartPh3.getXAxis().setAutoScale(false);
        AnalogicValuesChartPh3.getXAxis().setMinimum(0);
        AnalogicValuesChartPh3.getXAxis().setMaximum(200);
        AnalogicValuesChartPh3.setPreferredSize(new Dimension(480, 360));
        AnalogicValuesChartPh3.getY1Axis().setMaximum((maxPh3 * coeff) + 250);
        AnalogicValuesChartPh3.getY1Axis().setMinimum(-(maxPh3 * coeff) - 250);

        AnalogicValuesChartPh1.getY1Axis().addDataView(AnalogicValuesPh1);
        AnalogicValuesChartPh2.getY1Axis().addDataView(AnalogicValuesPh2);
        AnalogicValuesChartPh3.getY1Axis().addDataView(AnalogicValuesPh3);

    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void initTheoricalPhase() {
        fitTablePh1 = new double[250][1000];
        fitTablePh2 = new double[250][1000];
        fitTablePh3 = new double[250][1000];

        errorCurvePh1 = new JLDataView();
        errorCurvePh2 = new JLDataView();
        errorCurvePh3 = new JLDataView();

        errorCurvePh1.setColor(Color.magenta);
        errorCurvePh2.setColor(Color.magenta);
        errorCurvePh3.setColor(Color.magenta);

        errorCurvePh1.setName("Error curve");
        errorCurvePh2.setName("Error curve");
        errorCurvePh3.setName("Error curve");


        //  Caluclating phase 1 of recorded data
        double addPh1 = 0;
        double addPh2 = 0;
        double addPh3 = 0;
        errResultPh1 = new double[1000];
        errResultPh2 = new double[1000];
        errResultPh3 = new double[1000];
        for (int phi = 0 ; phi<1000 ; phi++) {
            for (int i = 0 ; i<250 ; i++) {
                fitTablePh1[i][phi] = Math.abs((DropDisplay.thisDrop.analogicValuesPh1[i] * Math.sqrt(2)) -
                        20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * i * 50 / 1000 * 0.8 + (2 * Math.PI * phi / 1000)));
                fitTablePh2[i][phi] = Math.abs((DropDisplay.thisDrop.analogicValuesPh2[i] * Math.sqrt(2)) -
                        20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * i * 50 / 1000 * 0.8 + (2 * Math.PI * phi / 1000)));
                fitTablePh3[i][phi] = Math.abs((DropDisplay.thisDrop.analogicValuesPh3[i] * Math.sqrt(2)) -
                        20000 * Math.sqrt(2) * Math.sin(2 * Math.PI * i * 50 / 1000 * 0.8 + (2 * Math.PI * phi / 1000)));
                addPh1 += fitTablePh1[i][phi];
                addPh2 += fitTablePh2[i][phi];
                addPh3 += fitTablePh3[i][phi];
            }
            errResultPh1[phi] = addPh1;
            errResultPh2[phi] = addPh2;
            errResultPh3[phi] = addPh3;
            addPh1 = 0;
            addPh2 = 0;
            addPh3 = 0;
        }

        // Search phase
        double minPh1 = errResultPh1[0];
        double minPh2 = errResultPh2[0];
        double minPh3 = errResultPh3[0];
        best_phiPh1 = 0;
        best_phiPh2 = 0;
        best_phiPh3 = 0;
        for (int phi = 0 ; phi<1000 ; phi++) {
            if (errResultPh1[phi]<minPh1) {
                minPh1 = errResultPh1[phi];
                best_phiPh1 = phi;
            }
            if (errResultPh2[phi]<minPh2) {
                minPh2 = errResultPh2[phi];
                best_phiPh2 = phi;
            }
            if (errResultPh3[phi]<minPh3) {
                minPh3 = errResultPh3[phi];
                best_phiPh3 = phi;
            }
        }
        fitted_phasePh1 = 2 * Math.PI * best_phiPh1 / 1000;
        fitted_phasePh2 = 2 * Math.PI * best_phiPh2 / 1000;
        fitted_phasePh3 = 2 * Math.PI * best_phiPh3 / 1000;
        System.out.println("Fitted phase 1= " + fitted_phasePh1 + " radians.");
        System.out.println("Fitted phase 2= " + fitted_phasePh2 + " radians.");
        System.out.println("Fitted phase 3= " + fitted_phasePh3 + " radians.");
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void initTheoricalCurve() {
        theoDataPh1 = new JLDataView();
        theoDataPh2 = new JLDataView();
        theoDataPh3 = new JLDataView();

        theoDataPh1.setColor(Color.black);
        theoDataPh2.setColor(Color.black);
        theoDataPh3.setColor(Color.black);
        theoDataPh1.setName("Theorical analogic values");
        theoDataPh2.setName("Theorical analogic values");
        theoDataPh3.setName("Theorical analogic values");

        //  Fill a table to calculate error
        theo = new double[3][250];

        //  Filling theorical curve
        for (int i = 1 ; i<250 ; i++) {
            double yPh1 = 20000 * Math.sqrt(2) * Math.sin((2 * Math.PI * 50 * i / 1000 * 0.8) + fitted_phasePh1);
            double yPh2 = 20000 * Math.sqrt(2) * Math.sin((2 * Math.PI * 50 * i / 1000 * 0.8) + fitted_phasePh2);
            double yPh3 = 20000 * Math.sqrt(2) * Math.sin((2 * Math.PI * 50 * i / 1000 * 0.8) + fitted_phasePh3);
            // /1000->0.8ms because, in this mode, the dsp take 1 point  every 160 micro
            // and 10 period and then read 1 point on 5 160*5=800 micro
            theo[0][i] = yPh1;
            theo[1][i] = yPh2;
            theo[2][i] = yPh3;
            theoDataPh1.add((double) i * 0.8, yPh1);
            theoDataPh2.add((double) i * 0.8, yPh2);
            theoDataPh3.add((double) i * 0.8, yPh3);

            //  Trace error
            errorCurvePh1.add((double) i * 0.8, fitTablePh1[i][best_phiPh1]);
            errorCurvePh2.add((double) i * 0.8, fitTablePh2[i][best_phiPh2]);
            errorCurvePh3.add((double) i * 0.8, fitTablePh3[i][best_phiPh3]);
        }
        AnalogicValuesChartPh1.getY1Axis().addDataView(errorCurvePh1);
        AnalogicValuesChartPh2.getY1Axis().addDataView(errorCurvePh2);
        AnalogicValuesChartPh3.getY1Axis().addDataView(errorCurvePh3);
        AnalogicValuesChartPh1.getY1Axis().addDataView(theoDataPh1);
        AnalogicValuesChartPh2.getY1Axis().addDataView(theoDataPh2);
        AnalogicValuesChartPh3.getY1Axis().addDataView(theoDataPh3);
        AnalogicValuesChartPh1.getY1Axis().setAutoScale(true);
        AnalogicValuesChartPh2.getY1Axis().setAutoScale(true);
        AnalogicValuesChartPh3.getY1Axis().setAutoScale(true);
    }
    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/

    public String[] clickOnChart(JLChartEvent evt) {
        String ret[] = new String[1];
        //ret[0] = evt.searchResult.dataView.getName();
        ret[0] = (int) evt.getTransformedYValue() + " events";
		/*try {
			Applet.newAudioClip(new File("/segfs/tango/jclient/elecdrops/src.wav")).play();
        }
		catch (Exception e){System.out.println("Exxception while playing sound...");}
		System.out.println(" sound played");
		*/
        return ret;
    }

}
