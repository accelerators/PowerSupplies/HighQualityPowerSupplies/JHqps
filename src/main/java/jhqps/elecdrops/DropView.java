//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/DropView.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: DropView.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
/**
 * @author goudard
 * @Revision 1.1
 */
package jhqps.elecdrops;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.JSmoothLabel;

import javax.swing.*;
import java.io.IOException;

public class DropView extends JFrame implements ElecDropDefs {

    static DeviceProxy dev;
    private JList list;
    private JSmoothLabel nb_unread_drop;

    /*-------------------------------------------------------------------------*/
    public DropView() {
        initComponents();
        try {
            dev = new DeviceProxy("pf/elec-drop/1");
        } catch (DevFailed e) {
            System.out.println("DevFailed Error");
            Except.print_exception(e);
            // TODO : Display DeviceProxy Creation failed!
        }
    }

    public static void main(String args[]) throws IOException {
        new DropView();
    }

    /*-------------------------------------------------------------------------*/
    private void initComponents() {

        list = new JList();
        nb_unread_drop = new JSmoothLabel();
        nb_unread_drop.setText("Number of unread drops : ");
        setTitle("DROP DETAILS");


        list.setPreferredSize(new java.awt.Dimension(200, 1000));

        //pane.setSize(1000,600);
        //pane.setVisible(true);
        // getContentPane().add(list, java.awt.BorderLayout.CENTER);
        getContentPane().add(nb_unread_drop, java.awt.BorderLayout.SOUTH);

        pack();
    }
}
