//+======================================================================
//$Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/ElecDropsGUI.java,v $

//Project:   JHqps

//Description:  HQPS java application for CTRM. .

//$Author: goudard $

//$Revision: 1.55 $

//$Log: ElecDropsGUI.java,v $
//Revision 1.55  2009/08/25 13:53:22  goudard
//Modifications of HqpsBinIn and HqpsBinOut reading.
//Added :
// - Incoherent state
// - wago uninitialized state
// - conditions to normal and economy mode detection.
//
//Modification of ElecDropStat : isolation curve added.
//
//Revision 1.54  2009/08/25 11:39:18  goudard
//Change behaviour when wago plc is unreachable (network problem)
//
//Revision 1.53  2009/08/18 11:33:50  goudard
//File JHqpsParameters.java added to CVS
//elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
//Revision 1.51  2009/06/15 13:43:02  goudard
//Modifications in
//	- state management of HQPSGlobal state button.
//	- FFT for jhqps.elecdrops.
//	- Main Parameters window.
//
//Revision 1.2  2009/01/28 13:11:14  goudard
//Added cvs header.


//Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//All Rights Reversed
//-======================================================================
/**
 * @author goudard
 */
package jhqps.elecdrops;

//TODO : Start the dev of the History file for DFT.

//Import Standard Java Classes
//==============================

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.TangoApi.ConnectionFailed;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.widget.device.SimpleStateViewer;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.JSmoothLabel;
import fr.esrf.tangoatk.widget.util.JSmoothProgressBar;
import fr.esrf.tangoatk.widget.util.Splash;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

//Import Tango Classes
//======================
//Import ATK Classes
//====================

/**
 * This class defines the elecdrops Graphical User Interface for the appli.
 * It allows users to read and configure the main parameters of the Drop
 * Detection system.
 */
public class ElecDropsGUI extends javax.swing.JFrame implements ElecDropDefs {

    //	Display debugging tips in shell
//	=================================
    public static boolean trace = false;
    public long previous_length;
    public DeviceProxy dev;
    public File drop_file;
    public int nb_drop;
    public String actual_filename;
    public String first_drop_date;
    public String inst_name;
    public String device;
    public ElecDrops DSP_device;
    public DbDatum file;
    //	Shared Memory datas
//	=====================
    public String dropdate;
    public int new_nb_drops;
    public jhqps.elecdrops.Drop[] the_drops;
    public DropDisplay toDisplay;
    public javax.swing.JList list;
    public String devname = "";
    public Dimension jHqps_dimension = new Dimension(200, 700);
    //	ATK Components to display devices states
//	==========================================
    Splash splash;
    javax.swing.Timer new_drop_scanner;
    int index = 0;
    jhqps.elecdrops.DFTHistory historic_fft = null;
    int prg = 0;
    //	Graphical components
//	======================
    private javax.swing.JMenuBar menu_bar;
    private javax.swing.JMenu file_menu;
    private javax.swing.JMenu help_menu;
    private javax.swing.JMenu option_item;
    private javax.swing.JMenu view_menu;
    private javax.swing.JMenu edit_menu;
    private javax.swing.JMenu live_mode_menu;
    //private javax.swing.JMenuItem 	    lastDropview;
    private javax.swing.JMenuItem refresh;
    private javax.swing.JMenuItem exit_item;
    private javax.swing.JMenuItem set_trig_levels;
    private javax.swing.JMenuItem set_DSP_time;
    private javax.swing.JMenuItem set_live_mode;
    private javax.swing.JMenuItem about;
    private javax.swing.JMenuItem help_on_version;
    private javax.swing.JMenuItem fsb_item;
    private javax.swing.JMenuItem print;
    private javax.swing.JMenuItem live_item;
    //private javax.swing.JMenuItem 	    	rms_chart;
    //private javax.swing.JMenuItem       	freq_chart;
    //private javax.swing.JMenuItem 	    	dft_chart;
    //private javax.swing.JMenuItem 	   	 	mean_chart;
    private javax.swing.JMenuItem stats;
    private javax.swing.JMenuItem dft_histo;
    private JSmoothLabel nb_unread_drop;
    private javax.swing.JScrollPane pane;
    private javax.swing.DefaultListModel drops = new javax.swing.DefaultListModel();
    private fr.esrf.tangoatk.core.Device dropPanel;
    private SimpleStateViewer dropDS;
    //	Delay to scan if a new drop appeared in logger
//	================================================
    private int delay = 3000;

    /**
     * ElecDropsGUI constructor
     */
    public ElecDropsGUI(String param) {
        try {
            devname = param;
            DSP_device = new ElecDrops(devname);
            dev = new DeviceProxy(devname);
            System.out.println(devname + " : import ok.");

            JSmoothProgressBar myBar = new JSmoothProgressBar();
            myBar.setStringPainted(true);
            myBar.setProgressBarColors(Color.GRAY, Color.LIGHT_GRAY, Color.DARK_GRAY);

            splash = new Splash(new javax.swing.ImageIcon(getClass().getResource(
                    "/jhqps/SinusThreePhase.gif")), new Color(20, 0, 0), myBar);
            splash.setCursor(new Cursor(Cursor.WAIT_CURSOR));
            splash.setTitle("Electric Drops Application");
            splash.setMessage("Graphical components creation.");
            splash.setCopyright("ESRF, Grenoble, France");
            splash.setMaxProgress(100);
            splash.initProgress();
            splash.progress(prg);

            initComponents();

            splash.setMessage("Retrieving drops from HDB.");
            double range = 100 - prg;

            //HDB access
            //=================================================
            try {
                DeviceProxy hdbDev = new DeviceProxy("infra/hdb-push/sys");
                hdbDev.set_timeout_millis(600000);

                System.out.println("Import DB server infra/hdb-push/sys : OK !");
                String[] s = new String[1];

                if (param.equals("infra/d-drops/process"))
                    s[0] = "infra/d-drops/process/drops";
                if (param.equals("infra/d-drops/ss1"))
                    s[0] = "infra/d-drops/ss1/drops";
                if (param.equals("infra/d-drops/facility"))
                    s[0] = "infra/d-drops/facility/drops";

                int[] tbl = new int[2];
                long now = System.currentTimeMillis();
                long now_sec = now / 1000; //in second
                tbl[0] = (int) now_sec - 15184000; // DB read start date = now - 2 months
                //tbl[1]=1516122333;
                tbl[1] = (int) now_sec; // DB read ending date

                DevVarLongStringArray lsa = new DevVarLongStringArray();
                lsa.svalue = s;
                lsa.lvalue = tbl;
                DeviceData argin = new DeviceData();
                argin.insert(lsa);

                System.out.println("creating argin = " + argin);

                DeviceData hdbdrops = hdbDev.command_inout("ReadString", argin);
                System.out.println("Read string ok ");

                DevVarLongStringArray hdb_drops = hdbdrops.extractLongStringArray();
                System.out.println("extract string ok ");
                //Vector<String> drop_list = new Vector<String>();
                nb_drop = hdb_drops.svalue.length;

                System.out.println("Number of recorded drops in HDB = " + nb_drop);

                // Print drops recorded in HDB
                //for(int i=0 ; i < nb_drop ; i++)
                //System.out.println("Drop " + i + " = "+ hdb_drops.svalue[i].toString());

                //for(int i =0 ; i < drop_list.size() ;i++)
                //System.out.println("Element " + i + " = "+ drop_list.elementAt(i));

                the_drops = new jhqps.elecdrops.Drop[hdb_drops.svalue.length];

                // In case of 0 drop in signal.
                //System.out.println("the_drops.length = "+ the_drops.length);
                if (the_drops.length == 0)
                    splash.setVisible(false);

                // Filling Drop table
                for (int i = 0 ; i<nb_drop ; i++) {
                    //System.out.println("filling "+ i + " drop");
                    the_drops[nb_drop - 1 - i] = jhqps.elecdrops.Drop.fill_data(hdb_drops.svalue[i].toString(), i);

                    double p = (double) i / (double) nb_drop;
                    splash.progress(prg + (int) (p * range));
                }

            } catch (ConnectionFailed e) {
                e.toString();
            } catch (Exception e) {
                e.toString();
            }

            // Droplist generation
            for (int i = 0 ; i<the_drops.length ; i++)
                drops.addElement(the_drops[i].dsp_time);

            System.out.println("Droplist lentgh = " + the_drops.length);
        } catch (DevFailed e) {
            splash.setVisible(false);
            dev = null;
            javax.swing.JOptionPane.showMessageDialog(null,
                    e.errors[0].desc,
                    e.errors[0].reason,
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            if (trace) {
                System.out.println("DevFailed Error : ");
                Except.print_exception(e);
            }
        } catch (Exception e) {
            System.out.println("Exception : " + e);
            Except.print_exception(e);
        }

        splash.progress(100);
        splash.setVisible(false);

        nb_unread_drop.setText(nb_drop + " drops recorded in HDB.");
        setSize(jHqps_dimension);
        ATKGraphicsUtils.centerFrameOnScreen(this);
        setVisible(true);
        pack();
    }

    /**
     * Formating date in milliseconds with com.braju.format jar file.
     */
    public static String date_in_ms1(long logged_time) {
        long nb_sec = logged_time / 1000;
        long ms = logged_time - (nb_sec * 1000);
        Date date_in_milli = new Date(logged_time);
        String time = DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT,
                java.text.DateFormat.MEDIUM, Locale.FRANCE).format(date_in_milli);
        String result = time + "." + String.format("%003d", (int) ms);

        return result;
    }

    /**
     * Formatting date in milliseconds with printf.jar file.
     *
     * @param logged_time : time to convert to the right format.
     * @return a string with date in the correct format.
     */
    /*-------------------------------------------------------------------------*/
    public static String date_in_ms(long logged_time) {
        long nb_sec = logged_time / 1000;
        long ms = logged_time - (nb_sec * 1000);
        Date date_in_milli = new Date(logged_time);
        //Object[] millis	= {new Integer((int) ms)};
        String time = DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT,
                java.text.DateFormat.MEDIUM, Locale.FRANCE).format(date_in_milli);
        String result = time + "." + String.format("%02d", (int) (ms / 10)); // to have only 2 digit

        return result;
    }

    /**
     * Launch a unix script to know if logger is running.
     */
    public static boolean loggerStatus() {
        boolean running = false;
        try {
            String cmd = new String("/segfs/tango/jserver/ElecLogger/is_logger_running");
            Process proc = Runtime.getRuntime().exec(cmd);
            InputStream inStr = proc.getInputStream();
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(inStr));
            String str;
            while ((str = br.readLine()) != null) {
                if (str.indexOf("ElecLogger")>0)
                    running = true;
                else
                    running = false;
            }

            // wait for end of command
            //========================
            proc.waitFor();
        } catch (Exception e) {
            System.out.println(e);
            running = false;
        }
        return running;
    }

    /**
     * @param argv the command line arguments
     */
    public static void main(String argv[]) throws IOException {
        //	Launching application
        new ElecDropsGUI(argv[0]);
    }

    /**
     * Counts tokens in the logging file.
     *
     * @return number of token in the logging file.
     * @throws IOException : If an input or output error occurs.
     */
    /*-------------------------------------------------------------------------*/
    public static short countTokens(String filename) throws IOException {
        InputStream inStream = new FileInputStream(filename);
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(inStream));
        StreamTokenizer st = new StreamTokenizer(bufRead);
        int type = 0;
        short count = 0;

        while (type != StreamTokenizer.TT_EOF) {
            type = st.nextToken();
            switch (type) {
                case StreamTokenizer.TT_WORD:
                    count++;
                    break;
                case StreamTokenizer.TT_NUMBER:
                    count++;
                    break;
            }
        }
        return ((short) (count / NB_PARAM)); // NB_PARAM= 11 -> nb tokens per line in logger file.
    }

    /**
     * Reads the logger file until EOF is reached
     *
     * @param nb_drops : number of drops to be read.
     * @param Filename : logger file name.
     * @return Drop table of all the drop recorded in file.
     * @throws FileNotFoundException : Logging file not found.
     * @throws IOException           : If an input or output error occurs.
     */
    /*-------------------------------------------------------------------------*/
    public static jhqps.elecdrops.Drop[] readLogger(int nb_drops, String Filename)
            throws FileNotFoundException, IOException {
        InputStream inStream = new FileInputStream(Filename);
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(inStream));
        Vector<Float> drop_list = new Vector<Float>();
        int exit = 0;
        int i = 0;
        StreamTokenizer st = new StreamTokenizer(bufRead);

        st.ordinaryChars(47, 47);
        while (exit == 0) {
            int type = st.nextToken();
            switch (type) {
                case StreamTokenizer.TT_NUMBER:
//				 System.out.println(""+i+" : "+st.nval);
                    drop_list.addElement(new Float(st.nval));
                    i++;
                    break;
                case StreamTokenizer.TT_EOF:
                    exit = 1;
                    break;
            }
        }

        //	Convert Float data to a float array
        //=====================================
        float[] convertedVal = new float[drop_list.size()];
        for (i = 0; i<drop_list.size() ; i++) {
            Float d = (Float) drop_list.elementAt(i);
            convertedVal[i] = d.floatValue();
        }

        //	Copy float array to a Drop array to be returned
        //=================================================
        int j = 0;
        jhqps.elecdrops.Drop[] result = new jhqps.elecdrops.Drop[nb_drops];
        System.out.println("nb_drops==" + nb_drops);
        for (j = 0; j<nb_drops ; j++) {
            result[j] = new jhqps.elecdrops.Drop(convertedVal, j * (FILE_NB_DATA + 4));
            //if(trace)
            //	System.out.println(result[j]);
        }
        return result;
    }

    /**
     *	rms_chartActionPerformed : Display Live RMS Chart.
     */
/*	 private void rms_chartActionPerformed(ActionEvent evt)
	 {
		 ElecDropLiveMode live_frame = new ElecDropLiveMode(this, false, dev);
		 live_frame.scan();
		 live_frame.rmsHistoBtnActionPerformed(evt);
	 }
*/
    /**
     *	freq_chartActionPerformed : Display live frequency chart.
     */
/*	 private void freq_chartActionPerformed(ActionEvent evt)
	 {
		 ElecDropLiveMode live_frame = new ElecDropLiveMode(this, false, dev);
		 live_frame.scan();
		 live_frame.freqHistoBtnActionPerformed(evt);
	 }
*/
    /**
     *	dft_chartActionPerformed : Display Live DFT chart.
     */
/*	 private void dft_chartActionPerformed(ActionEvent evt)
	 {
		 ElecDropLiveMode live_frame = new ElecDropLiveMode(this, false, dev);
		 live_frame.scan();
		 live_frame.dftLiveBtnActionPerformed(evt);
	 }
*/
    /**
     *	mean_chartActionPerformed : Display mean Live values in a chart .
     */
/*	 private void mean_chartActionPerformed(ActionEvent evt)
	 {
		 ElecDropLiveMode live_frame = new ElecDropLiveMode(this, false, dev);
		 live_frame.scan();
		 live_frame.meanLiveBtnActionPerformed(evt);
	 }
*/

    /**
     * Reads the logger file until EOF is reached
     *
     * @param Filename : logger file name
     * @return string table of all the dates in the logging file.
     * @throws FileNotFoundException : Logging file not found.
     * @throws IOException           : If an input or output error occurs.
     */
    /*-------------------------------------------------------------------------*/
    public static String[] readDropDate(String Filename) throws FileNotFoundException,
            IOException {
        trace = true;
        InputStream inStream = new FileInputStream(Filename);
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(inStream));
        Vector<Double> drop_list = new Vector<Double>();
        int exit = 0;
        int i = 0;
        StreamTokenizer st = new StreamTokenizer(bufRead);

//		 st.resetSyntax();
        st.ordinaryChars(47, 47);
        while (exit == 0) {
            int type = st.nextToken();
            switch (type) {
                case StreamTokenizer.TT_NUMBER:
                    //System.out.println(""+i+" : "+st.nval);
                    drop_list.addElement(new Double(st.nval));
                    i++;
                    break;
                case StreamTokenizer.TT_EOF:
                    exit = 1;
                    break;
            }
        }

        //	Copy Vector data to a String array to be returned
        //===================================================
        String[] dates = new String[drop_list.size() / (FILE_NB_DATA + 4)];
        double[] array1 = new double[drop_list.size()];
        long[] array2 = new long[drop_list.size()];
        int j;

        for (i = UNIX_TIME_OFFEST + 4, j = 0; i<drop_list.size() ; i = i + (FILE_NB_DATA + 4), j++) {
            Double d = (Double) drop_list.elementAt(i);
            array1[i] = d.doubleValue() * 1000;
            array2[i] = (long) array1[i];
            dates[j] = date_in_ms(array2[i]);
            System.out.println("listdate: " + d);
            System.out.println("Drop " + j + " date: " + date_in_ms(array2[i]));

        }
        return dates;
    }

    /**
     *	lastDropviewActionPerformed : Display a graphical view of the last drop stored in the DSP.
     */
	 /*private void lastDropviewActionPerformed(ActionEvent evt)
{
	new DropGraph(this,true,this.devname).setVisible(true);
}*/

    /**
     * Graphics initialisation.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void initComponents() throws IOException {
        menu_bar = new javax.swing.JMenuBar();
        file_menu = new javax.swing.JMenu("File ");
        help_menu = new javax.swing.JMenu("Help");
        option_item = new javax.swing.JMenu("Options");
        view_menu = new javax.swing.JMenu("View");
        live_mode_menu = new javax.swing.JMenu("20kV Live");
        edit_menu = new javax.swing.JMenu("Edit");
        refresh = new javax.swing.JMenuItem("Refresh");
//		 lastDropview		= new javax.swing.JMenuItem("Analogic Values of last drop");
        // rms_chart			= new javax.swing.JMenuItem("20kV RMS Chart");
        // freq_chart			= new javax.swing.JMenuItem("20kV Frequency Chart");
        //dft_chart			= new javax.swing.JMenuItem("20kV DFT Chart");
        //mean_chart			= new javax.swing.JMenuItem("20kV HO Chart");
        live_item = new javax.swing.JMenuItem("Live mode");
        exit_item = new javax.swing.JMenuItem("Exit ...");
        set_trig_levels = new javax.swing.JMenuItem("Trigger Levels");
        set_DSP_time = new javax.swing.JMenuItem("DSP Time");
        set_live_mode = new javax.swing.JMenuItem("Live Mode");
        help_on_version = new javax.swing.JMenuItem("Help on versions");
        about = new javax.swing.JMenuItem("About ...");
        stats = new javax.swing.JMenuItem("Statistics");
        dft_histo = new javax.swing.JMenuItem("DFT History");
        list = new javax.swing.JList(drops);
        nb_unread_drop = new JSmoothLabel();
//		 nb_drop_in_fileLbl	= new JSmoothLabel();
        pane = new javax.swing.JScrollPane(list);
        fsb_item = new javax.swing.JMenuItem("Open drop file");
        dropDS = new SimpleStateViewer();
        print = new javax.swing.JMenuItem("Print ...");

        String str = "";
        if (devname.equals("infra/d-drops/ss1"))
            str = "SS1";
        if (devname.equals("infra/d-drops/process"))
            str = "PROCESS";
        if (devname.equals("infra/d-drops/facility"))
            str = "FACILITY";
        System.out.println("str = " + str);
        String title = str + " DROPS";

        setTitle(title);

        //setLocation(jHqps_dimension.width ,0);
        //setSize(230,jHqps_dimension.height);
        splash.setMessage("Creating GUI Objects.");
        pane.setWheelScrollingEnabled(true);
        help_menu.setBackground(new java.awt.Color(0, 153, 102));
        help_menu.setForeground(java.awt.Color.white);
        help_menu.add(help_on_version);
        help_menu.addSeparator();
        help_menu.add(about);
        file_menu.setActionCommand("Menu");
        file_menu.setBackground(new java.awt.Color(0, 153, 102));
        file_menu.setForeground(java.awt.Color.white);
        file_menu.add(fsb_item);
        file_menu.add(option_item);
//		 file_menu.add(dserver_state);
//		 file_menu.add(logger_status);
//		 file_menu.add(read_log_item);
        file_menu.addSeparator();
        file_menu.add(print);
        file_menu.addSeparator();
        file_menu.add(exit_item);
        view_menu.setBackground(new java.awt.Color(0, 153, 102));
        view_menu.setForeground(java.awt.Color.white);
        //view_menu.add(lastDropview);
        edit_menu.setBackground(new Color(0, 153, 102));
        edit_menu.setForeground(Color.white);
        edit_menu.add(refresh);
        live_mode_menu.add(live_item);
        // live_mode_menu.add(rms_chart);
        // live_mode_menu.add(freq_chart);
        // live_mode_menu.add(dft_chart);
        // live_mode_menu.add(mean_chart);
        live_mode_menu.setBackground(new java.awt.Color(0, 153, 102));
        live_mode_menu.setForeground(java.awt.Color.white);
        view_menu.add(stats);
        view_menu.add(dft_histo);
        for (int i = 29 ; i<=39 ; i++)
            splash.progress(i);
        splash.setMessage("Creating Menus.");

        dft_histo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                dft_histoActionPerformed(evt);
            }
        });


        print.setAccelerator(KeyStroke.getKeyStroke('P', Event.CTRL_MASK));
        print.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                printActionPerformed(evt);
            }
        });

        about.setAccelerator(KeyStroke.getKeyStroke('A', Event.CTRL_MASK));
        about.setMnemonic('a');
        about.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                aboutActionPerformed(evt);
            }
        });

        exit_item.setAccelerator(KeyStroke.getKeyStroke('X', Event.CTRL_MASK));
        exit_item.setMnemonic('x');
        exit_item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                exit_itemActionPerformed(evt);
            }
        });

        set_DSP_time.setAccelerator(KeyStroke.getKeyStroke('S', Event.CTRL_MASK));
        set_DSP_time.setMnemonic('s');
        set_DSP_time.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                set_DSP_timeActionPerformed(evt);
            }
        });

        set_live_mode.setAccelerator(KeyStroke.getKeyStroke(',', Event.CTRL_MASK));
        set_live_mode.setMnemonic(',');
        set_live_mode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                set_live_modeActionPerformed(evt);
            }
        });

        set_trig_levels.setAccelerator(KeyStroke.getKeyStroke('T', Event.CTRL_MASK));
        set_trig_levels.setMnemonic('t');
        set_trig_levels.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                set_trig_levelsActionPerformed(evt);
            }
        });

        fsb_item.setAccelerator(KeyStroke.getKeyStroke('O', Event.CTRL_MASK));
        fsb_item.setMnemonic('o');
        fsb_item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                fsb_itemActionPerformed(evt);
            }
        });

        live_item.setAccelerator(KeyStroke.getKeyStroke('L', Event.CTRL_MASK));
        live_item.setMnemonic('l');
        live_item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                live_itemActionPerformed(evt);
            }
        });

/*		 rms_chart.setAccelerator(KeyStroke.getKeyStroke('R', Event.CTRL_MASK));
		 rms_chart.setMnemonic('r');
		 rms_chart.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent evt) {
				 rms_chartActionPerformed(evt);
			 }
		 });

		 freq_chart.setAccelerator(KeyStroke.getKeyStroke('F', Event.CTRL_MASK));
		 freq_chart.setMnemonic('f');
		 freq_chart.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent evt) {
				 freq_chartActionPerformed(evt);
			 }
		 });

		 dft_chart.setAccelerator(KeyStroke.getKeyStroke('D', Event.CTRL_MASK));
		 dft_chart.setMnemonic('d');
		 dft_chart.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent evt) {
				 dft_chartActionPerformed(evt);
			 }
		 });

		 mean_chart.setAccelerator(KeyStroke.getKeyStroke('H', Event.CTRL_MASK));
		 mean_chart.setMnemonic('h');
		 mean_chart.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent evt) {
				 mean_chartActionPerformed(evt);
			 }
		 });
*/

        help_menu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        help_on_version.setAccelerator(KeyStroke.getKeyStroke('V', Event.CTRL_MASK));
        help_on_version.setMnemonic('v');
        help_on_version.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                help_on_versionActionPerformed(evt);
            }
        });


        refresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                refreshActionPerformed(evt);
            }
        });

//		 lastDropview.addActionListener(new ActionListener() {
//		 public void actionPerformed(ActionEvent evt) {
//		 lastDropviewActionPerformed(evt);
//		 }
//		 });

        stats.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                statsActionPerformed(evt);
            }
        });

        menu_bar.setBackground(new java.awt.Color(0, 153, 102));
        menu_bar.setForeground(java.awt.Color.white);
        menu_bar.add(file_menu);
        menu_bar.add(edit_menu);
        menu_bar.add(view_menu);
        menu_bar.add(live_mode_menu);
        menu_bar.add(help_menu);

        option_item.add(set_trig_levels);
        option_item.add(set_live_mode);
        option_item.add(set_DSP_time);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                exitForm(evt);
            }
        });

        splash.progress(++prg);

        //	Set Jlist params
        //==================
        list.setFont(new Font("Tahoma", Font.BOLD, 14));
        list.setAlignmentY(CENTER_ALIGNMENT);

        //	Displays a Tooltip text in Jlist
        //==================================
        list.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent e) {
                list.setToolTipText("Click a date to display drop details.");
            }
        });

        //	Detects double-click in JList
        //===============================
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                int index;
                if (evt.getClickCount() == 1) {
                    list.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    int selected = list.locationToIndex(evt.getPoint());
                    System.out.println("selected = " + selected);
                    int tmp = (new_nb_drops - nb_drop);
                    if (tmp>0)
                        index = selected + tmp;
                    else
                        index = selected;
                    try {
                        listSelectionPerformed(evt, index);
                        index = toDisplay.getNewIndex();
                        //System.out.println("new index is now :  = "+index);
                    } catch (FileNotFoundException e) {
                        System.out.println(e);
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
            }
        };
        list.addMouseListener(mouseListener);

        splash.progress(++prg);

        splash.setMessage("Creating Listeners.");

        //	Scanning to display new drops
        //===============================
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (ElecDropsGUI.trace)
                    System.out.println("Timer is scanning for new drops");

//				 try
                //		{
                long length = drop_file.length();
                if (previous_length != length) {
                    readNbDrop();
                    //	Display new drop in JList
                    drops.clear();
                    //new_nb_drops = countDrops(actual_filename);
                    //droplist = readLogger((int) nb_drops, actual_filename);
                    //dropdate = readDropDate(actual_filename);

					 /*				for(int i=0 ; i<dropdate.length ; i++)
  						dropdate[i] = dropdate[i].substring(0,9) +"  -   " + dropdate[i].substring(9);
					for(int i=0 ; i<dropdate.length ; i++)
						drops.insertElementAt(dropdate[i], FISRT_ELEMENT);
					  */
                    index = index + nb_drop;
                    nb_drop = (short) new_nb_drops;
                    previous_length = length;
                    javax.swing.JOptionPane.showMessageDialog(null,
                            "New drops detected on 20 kV!",
                            "Drop Detection",
                            javax.swing.JOptionPane.WARNING_MESSAGE);
                }
                //}
				 /*			catch (FileNotFoundException e)
			{
            	JOptionPane.showMessageDialog( 	null,
 										e.getMessage(),
							            "Logger File Error",
										JOptionPane.ERROR_MESSAGE);
            	splash.setVisible(false);
        	}
			catch (IOException e)
			{
            	JOptionPane.showMessageDialog( 	null,
 										e.getMessage(),
							            "I/O Exception",
										JOptionPane.ERROR_MESSAGE);
        	}
				  */
            }
        };
        new_drop_scanner = new javax.swing.Timer(delay, taskPerformer);

        splash.progress(++prg);
        splash.setMessage("Creating Drop Scanner.");

        /**
         *	Java logger and c++ server state display using ATK tools.
         */
        try {
            dropPanel = DeviceFactory.getInstance().getDevice(devname);

            dropDS.setModel(dropPanel);
            dropDS.setText("DSP");
            dropDS.setPreferredSize(new Dimension(75, 25));
            dropDS.setBorder(new javax.swing.border.EtchedBorder(new java.awt.Color(204, 204, 204), java.awt.Color.gray));
            dropDS.setFont(new Font("Tahoma", Font.BOLD, 12));
            dropDS.setHorizontalAlignment(JSmoothLabel.CENTER_ALIGNMENT);
            dropDS.setSizingBehavior(JSmoothLabel.MATRIX_BEHAVIOR);
            inst_name = "Elecdrops/20kv";
            devPopup popup = new devPopup(inst_name, devname);
            popup.setModel(dropPanel);
            dropDS.setPopUp(popup);
        } catch (ConnectionException e) {
            if (trace)
                System.out.println(e);
            javax.swing.JOptionPane.showMessageDialog(null,
                    e.getMessage(),
                    "Connection Exception",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }

        //dserver_state.add(dropDS);
        //dserver_state.add(new JSeparator());
        //dserver_state.add(logDS);
        splash.progress(++prg);
        splash.setMessage("Creating server state viewers.");

        javax.swing.JPanel top = new javax.swing.JPanel();
        top.setLayout(new FlowLayout());
        top.add(dropDS);

        javax.swing.JPanel down = new javax.swing.JPanel();
        down.setLayout(new BorderLayout());
        down.add(nb_unread_drop, BorderLayout.NORTH);
        //down.add(nb_drop_in_fileLbl, BorderLayout.SOUTH);

        getContentPane().add(down, BorderLayout.SOUTH);
        getContentPane().add(top, BorderLayout.NORTH);
        getContentPane().add(pane, BorderLayout.CENTER);
        setJMenuBar(menu_bar);
        splash.progress(++prg);
    }

    /**
     * statsActionPerformed : Launching statistics on drops
     *
     * @param evt
     */
    private void statsActionPerformed(ActionEvent evt) {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        new ElecDropStats(the_drops).setVisible(true);
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    /**
     * fsb_itemActionPerformed :
     */
    private void fsb_itemActionPerformed(ActionEvent evt) {
        if (file == null) {
            JOptionPane.showMessageDialog(this, "Please select a file", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            File currentPath = new File(file.extractString());
            javax.swing.JFileChooser fsb = new javax.swing.JFileChooser(currentPath.getAbsolutePath());
            fsb.setApproveButtonText("Open");
            fsb.setDialogTitle("Choose a Drop File");
            fsb.showOpenDialog(null);
            File chosen_drop_file = fsb.getSelectedFile();
            if (trace)
                System.out.println("Opening file" + fsb.getSelectedFile());
            String[] droplist1 = readDropDate(chosen_drop_file.getAbsolutePath());
            drops.clear();
            actual_filename = chosen_drop_file.getAbsolutePath();
            for (int i = 0 ; i<droplist1.length ; i++) {
                long logged_time = new Long(droplist1[i]);
                String time = date_in_ms1(logged_time);
                drops.addElement(time);
                System.out.println("newdroplist : " + droplist1[i]);
            }
        } catch (FileNotFoundException e) {
            javax.swing.JOptionPane.showMessageDialog(null,
                    e.getMessage(),
                    "Logger File Error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            javax.swing.JOptionPane.showMessageDialog(null,
                    e.getMessage(),
                    "I/O Error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * live_itemActionPerformed : Display LiveMode frame.
     */
    private void dft_histoActionPerformed(ActionEvent evt) {
        try {
            System.out.println("Opening " + devname + "/DftAveraging");
            historic_fft = new jhqps.elecdrops.DFTHistory(this, devname + "/DftAveraging", devname + "/THD", devname, false);
            System.out.println(devname + "/DftAveraging Opened. ");
            historic_fft.setSize(1200, 666);
            historic_fft.setLocation(this.getLocation());
            historic_fft.setVisible(true);
        } catch (DevFailed e) {
            e.printStackTrace();
        }
    }

    /**
     * live_itemActionPerformed : Display LiveMode frame.
     */
    private void live_itemActionPerformed(ActionEvent evt) {
        ElecDropLiveMode live_frame = new ElecDropLiveMode(this, false, dev);
        live_frame.scan();
    }

    /**
     * refreshActionPerformed : Display LiveMode frame.
     */
    private void refreshActionPerformed(ActionEvent evt) {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        pack();
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    /**
     * set_trig_levelsActionPerformed : Display Trigger Level frame.
     */
    private void set_trig_levelsActionPerformed(ActionEvent evt) {
        new TrigLevel(dev, this.devname);
        short[] data = new short[2];
        data[0] = TRIG_LEVELS;
        data[1] = NB_TRIG_LEVELS;

    }

    /**
     * set_DSP_timeActionPerformed : retrieving Unix time and setting VDSP time.
     */
    private void set_DSP_timeActionPerformed(ActionEvent evt) {
        DSP_device.DspTimeAttr(WriteDsp);
    }

    /**
     * set_live_modeActionPerformed
     */
    private void set_live_modeActionPerformed(ActionEvent evt) {
        System.out.println("Openning live mode options");
    }

    private void help_on_versionActionPerformed(ActionEvent evt) {
        new HelpOnVersion(new javax.swing.JFrame(), true).setVisible(true);
    }

    private void aboutActionPerformed(ActionEvent evt) {
        new About(new javax.swing.JFrame(), true).setVisible(true);
    }

    private void printActionPerformed(ActionEvent evt) {
        //	Printing stuff
        //================
        java.awt.PageAttributes pa = new java.awt.PageAttributes();
        java.awt.JobAttributes ja = new java.awt.JobAttributes();
        pa.setPrintQuality(5);
        pa.setPrinterResolution(200);
        pa.setColor(java.awt.PageAttributes.ColorType.MONOCHROME);
        pa.setMedia(java.awt.PageAttributes.MediaType.A4);
        ja.setPrinter("ctb312b1u");

        java.awt.PrintJob printJob = java.awt.Toolkit.getDefaultToolkit().getPrintJob(
                this, "elecdrops Application", ja, pa);

        if (printJob != null) {
            java.awt.Graphics g = printJob.getGraphics();
            g.translate(200, 300);
            g.setClip(1, 1, getSize().width, getSize().height);
            g.setFont(new Font("Arial", 1, 12));
            printAll(g);
            g.dispose();
            printJob.end();
        }
    }

    /*-------------------------------------------------------------------------*/

    /**
     * exit_itemActionPerformed : Exiting elecdrops appli.
     */
    private void exit_itemActionPerformed(ActionEvent evt) {
        new_drop_scanner.stop();
        setVisible(false);
        dispose();
    }

    /**
     * Exit elecdrops Application
     */
    private void exitForm(WindowEvent evt) {
        new_drop_scanner.stop();
        setVisible(false);
        dispose();

    }

    /**
     * Reading of a selected drop to display it in a new window.
     */
    private void listSelectionPerformed(MouseEvent evt, int index)
            throws FileNotFoundException, IOException {
        displayRecordedDrop(index);
    }
    /*-------------------------------------------------------------------------*/

    /**
     * Reading number of drops in DSP DPRAM.
     */
    private int readNbDrop() {
        short[] data = {NUMBER_OF_DROPS, 1};
        short[] values = new short[data[1]];
        if (dev != null) {
            try {
                DeviceData argin = new DeviceData();
                argin.insert(data);
                DeviceData argout = dev.command_inout("ReadValues", argin);
                values = argout.extractShortArray();
                //nb_drops_in_file = countDrops(actual_filename);
                //System.out.println("values[0] = "+values[0]);
            } catch (DevFailed e) {
                if (trace)
                    Except.print_exception(e);
                javax.swing.JOptionPane.showMessageDialog(null,
                        e.errors[0].desc,
                        e.errors[0].reason,
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            }
			 /*catch(FileNotFoundException e)
		{
			Except.print_exception(e);
		}
		catch(IOException e)
		{
			Except.print_exception(e);
		}*/
        }
        if (first_drop_date != null) {
            nb_unread_drop.setText(values[0] + " drops recorded since DSP restart.");
        }
        nb_unread_drop.setText(values[0] + " drops recorded since DSP restart.");
        return (values[0]);
    }
    /*-------------------------------------------------------------------------*/

    /**
     * scan : This method start the scanning of drops in each ElecdDropsGUI
     * open in the control system.
     */
    public void scan() {
        new_drop_scanner.start();
    }
    /*-------------------------------------------------------------------------*/

    /**
     * displayRecordedDrop : This method displays recorded drops in a
     * new DropDisplay Class.
     */
    public void displayRecordedDrop(int index) throws IOException, FileNotFoundException {
        the_drops[index].drop_number = index;
        toDisplay = new jhqps.elecdrops.DropDisplay(this, false, the_drops[index], the_drops[index].dsp_time, the_drops, index, list);
        list.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        toDisplay.setVisible(true);
    }

    /*-------------------------------------------------------------------------*/

    /**
     * Counting the number of drops recorded in the Logger File.
     *
     * @param Filename :  logger file name.
     * @return a short value equal to the number opf drop recorded in the logging file..
     * @throws FileNotFoundException : Logging file not found.
     * @throws IOException           : If an input or output error occurs.
     */
    /*-------------------------------------------------------------------------*/
    public short countDrops(String Filename) throws FileNotFoundException,
            IOException {

        InputStream inStream = new FileInputStream(Filename);
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(inStream));
        short lines = 0;
        int exit = 0;
        StreamTokenizer st = new StreamTokenizer(bufRead);

        st.eolIsSignificant(true);
        st.ordinaryChars(47, 47);
        while (exit == 0) {
            int type = st.nextToken();
            switch (type) {
                case StreamTokenizer.TT_EOL:
                    lines++;
                    break;
                case StreamTokenizer.TT_EOF:
                    exit = 1;
                    break;
            }
        }
        if (trace)
            System.out.println(lines + " drops recorded in " + file.extractString());
        return lines;
    }
} // End
