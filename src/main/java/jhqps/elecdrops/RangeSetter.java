//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/RangeSetter.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: RangeSetter.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:19  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:02  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.elecdrops;

import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * RGA range setter
 */
public class RangeSetter extends JDialog implements ActionListener {

    public static Insets noMargin = new Insets(0, 0, 0, 0);

    private JTextField minText;
    private JTextField maxText;
    private JButton dismissButton;
    private JButton applyButton;
    private boolean okFlag;
    private double min;
    private double max;

    public RangeSetter(JFrame parent) {

        super(parent, true);
        JPanel innerPanel = new JPanel(null);
        innerPanel.setPreferredSize(new Dimension(195, 90));
        setContentPane(innerPanel);

        JLabel maxLabel = new JLabel("Max");
        maxLabel.setFont(ATKConstant.labelFont);
        maxLabel.setBounds(5, 5, 60, 25);
        innerPanel.add(maxLabel);
        maxText = new JTextField();
        maxText.setMargin(noMargin);
        maxText.setBounds(70, 5, 120, 25);
        innerPanel.add(maxText);

        JLabel minLabel = new JLabel("Min");
        minLabel.setFont(ATKConstant.labelFont);
        minLabel.setBounds(5, 30, 60, 25);
        innerPanel.add(minLabel);
        minText = new JTextField();
        minText.setMargin(noMargin);
        minText.setBounds(70, 30, 120, 25);
        innerPanel.add(minText);

        applyButton = new JButton("Apply");
        applyButton.setFont(ATKConstant.labelFont);
        applyButton.addActionListener(this);
        applyButton.setBounds(5, 60, 90, 25);
        innerPanel.add(applyButton);

        dismissButton = new JButton("Dismiss");
        dismissButton.setFont(ATKConstant.labelFont);
        dismissButton.addActionListener(this);
        dismissButton.setBounds(100, 60, 90, 25);
        innerPanel.add(dismissButton);

        setTitle("Range");

    }

    public void actionPerformed(ActionEvent e) {

        Object src = e.getSource();
        okFlag = false;

        if (src == dismissButton) {
            setVisible(false);
        } else if (src == applyButton) {
            setVisible(false);
            try {
                min = Double.parseDouble(minText.getText());
                max = Double.parseDouble(maxText.getText());
                if (min<max) {
                    okFlag = true;
                } else {
                    JOptionPane.showMessageDialog(this, "Min must be lower than max", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(this, "Invalid number syntax\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    public boolean editRange(double min, double max) {

        String minValue = toScientific(min);
        minText.setText(minValue);
        String maxValue = toScientific(max);
        maxText.setText(maxValue);
        ATKGraphicsUtils.centerDialog(this);
        setVisible(true);
        return okFlag;

    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    private String toScientific(double d) {

        double a = Math.abs(d);
        int e = 0;
        String f = "%.2fe%d";

        if (a != 0) {
            if (a<1) {
                while (a<1) {
                    a = a * 10;
                    e--;
                }
            } else {
                while (a >= 10) {
                    a = a / 10;
                    e++;
                }
            }
        }

        if (a >= 9.999999999) {
            a = a / 10;
            e++;
        }

        if (d<0) a = -a;

        return String.format(f, a, e);
    }

}
