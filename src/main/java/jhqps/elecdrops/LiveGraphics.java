//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/LiveGraphics.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: LiveGraphics.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:02  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================

package jhqps.elecdrops;

/*
 * LiveGraphics.java
 *
 * Created on September 25, 2002, 6:01 PM
 */

/**
 * @author Goudard
 */

import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.widget.util.ErrorHistory;

import java.awt.*;

//	Import ATK Classes
//====================

public class LiveGraphics extends javax.swing.JFrame implements ElecDropDefs {

    public ErrorHistory errorHistory;
    //private  JFrame		            trendFrame;
    public INumberSpectrum spec;
    javax.swing.JScrollPane jScrollPane1;
    private fr.esrf.tangoatk.core.AttributeList number_spectrum_atts; /* used in spectrum tabs */
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;

    /**
     * Creates new form AtkPanel
     */

    public LiveGraphics(String devname) {
        try {
            number_spectrum_atts = new fr.esrf.tangoatk.core.AttributeList();
            initComponents();
            String attname = devname + "/RMSLive";
            String attname1 = devname + "/FREQLive";
            String attname2 = devname + "/DFTLive";
            // Needs an atribute name.
            spec = (INumberSpectrum) number_spectrum_atts.add(attname);
            spec = (INumberSpectrum) number_spectrum_atts.add(attname1);
            spec = (INumberSpectrum) number_spectrum_atts.add(attname2);
            showNumberSpectrumAtts();
        } catch (ConnectionException e) {
            System.out.println("Connection Exception :" + e.toString());
        }
        //this.setVisible(true);
        number_spectrum_atts.startRefresher();
        pack();
    }

    private void showNumberSpectrumAtts() {
        int nb_atts, idx;
        SpectrumPanel att_tab;
        INumberSpectrum spectrum_att = null;

        nb_atts = number_spectrum_atts.getSize();
        System.out.println("nb_atts = " + nb_atts);
        for (idx = 0; idx<nb_atts ; idx++) {
            spectrum_att = (INumberSpectrum) number_spectrum_atts.getElementAt(idx);
            // Create one Spectrum Panel per Number Spectrum Attribute
            att_tab = new SpectrumPanel(spectrum_att, idx + 1);
            // Add the spectrum panel as a tab into the tabbed panel of the main frame
            jTabbedPane1.addTab(spectrum_att.getNameSansDevice(), att_tab);
            pack();
        }
    }

    private void initComponents() {//GEN-BEGIN:initComponents
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();

        jPanel1.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints1;
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints1.insets = new java.awt.Insets(5, 5, 5, 5);
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        jTabbedPane1.setPreferredSize(new Dimension(500, 250));
//      jTabbedPane1.addTab("Scalar", jScrollPane1);
        jPanel1.add(jTabbedPane1, gridBagConstraints1);
        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

    }
}
