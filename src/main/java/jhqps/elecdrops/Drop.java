//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/Drop.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: Drop.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.2  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================

/**
 * Drop.java
 * <p>
 * Created on 3 startunec 2002, 08:41
 *
 * @author goudard
 * @rev 1.1
 */
package jhqps.elecdrops;

//	Import Standard Java Classes
//==============================

import java.text.DateFormat;
import java.util.Date;
import java.util.StringTokenizer;

/*-------------------------------------------------------------------------*/

/**
 *
 */
/*-------------------------------------------------------------------------*/
public class Drop implements ElecDropDefs {

    public int rmsPh1;
    public int rmsPh2;
    public int rmsPh3;
    public int meanPh1;
    public int meanPh2;
    public int meanPh3;
    public float freqPh1;
    public float freqPh2;
    public float freqPh3;
    public int duration;
    public int durationPh1;
    public int durationPh2;
    public int durationPh3;
    public int OVdurationPh1;
    public int OVdurationPh2;
    public int OVdurationPh3;
    public int coding;
    public int drop_number;
    public int newdrops;
    public float freqmin = 49;
    public float freqmax = 51;
    public float rmsmin = 12500;
    public float rmsmax = 14500;
    public short[] analogicValuesPh1 = new short[250];
    public short[] analogicValuesPh2 = new short[250];
    public short[] analogicValuesPh3 = new short[250];
    public boolean hasAnalogicValues;
    public String dsp_time;
    public String log_filename;
    /*-------------------------------------------------------------------------*/

    /**
     * Constructor for the Drop Class.
     */
    /*-------------------------------------------------------------------------*/
    public Drop() {
    }

    /*-------------------------------------------------------------------------*/

    /**
     * Constructor for the Drop Class.
     */
    /*-------------------------------------------------------------------------*/
    public Drop(int nb_new_drop) {
        this.newdrops = nb_new_drop;
    }

    /*-------------------------------------------------------------------------*/

    /**
     * Constructor for the Drop Class.
     */
    /*-------------------------------------------------------------------------*/
    public Drop(int rmsPh1,
                int rmsPh2,
                int rmsPh3,
                int meanPh1,
                int meanPh2,
                int meanPh3,
                float freqPh1,
                float freqPh2,
                float freqPh3,
                int duration,
                int coding,
//				Date	dsp_time,
                int drop_number) {
        this.rmsPh1 = rmsPh1;
        this.rmsPh2 = rmsPh2;
        this.rmsPh3 = rmsPh3;
        this.meanPh1 = meanPh1;
        this.meanPh2 = meanPh2;
        this.meanPh3 = meanPh3;
        this.freqPh1 = freqPh1;
        this.freqPh2 = freqPh2;
        this.freqPh3 = freqPh3;
        this.duration = duration * 20;
        this.coding = coding;
//		this.dsp_time	=	dsp_time;
        this.drop_number = drop_number;
    }

    /*-------------------------------------------------------------------------*/

    /**
     * Constructor for the Drop Class.
     */
    /*-------------------------------------------------------------------------*/
    public Drop(float rmsPh1,
                float rmsPh2,
                float rmsPh3,
                float freqPh1,
                float freqPh2,
                float freqPh3) {
        this.rmsPh1 = (int) rmsPh1;
        this.rmsPh2 = (int) rmsPh2;
        this.rmsPh3 = (int) rmsPh3;
        this.meanPh1 = 0;
        this.meanPh2 = 0;
        this.meanPh3 = 0;
        this.freqPh1 = freqPh1;
        this.freqPh2 = freqPh2;
        this.freqPh3 = freqPh3;
        this.duration = 0;
        this.coding = 0;
//		this.dsp_time	=	new Date();
        this.drop_number = 0;
    }

    /*-------------------------------------------------------------------------*/

    /**
     * Constructor for the Drop Class.
     */
    /*-------------------------------------------------------------------------*/
    public Drop(float[] convertedVal, int start) {
        this.rmsPh1 = (int) convertedVal[(start + 6)];
        this.rmsPh2 = (int) convertedVal[(start + 7)];
        this.rmsPh3 = (int) convertedVal[(start + 8)];
        this.freqPh1 = convertedVal[(start + 12)];
        this.freqPh2 = convertedVal[(start + 13)];
        this.freqPh3 = convertedVal[(start + 14)];
        this.meanPh1 = (int) convertedVal[(start + 9)];
        this.meanPh2 = (int) convertedVal[(start + 10)];
        this.meanPh3 = (int) convertedVal[(start + 11)];
        this.durationPh1 = (int) (convertedVal[(start + 16)]);
        this.durationPh2 = (int) (convertedVal[(start + 17)]);
        this.durationPh3 = (int) (convertedVal[(start + 18)]);
        this.OVdurationPh1 = (int) (convertedVal[(start + 19)]);
        this.OVdurationPh2 = (int) (convertedVal[(start + 20)]);
        this.OVdurationPh3 = (int) (convertedVal[(start + 21)]);
        this.coding = (int) convertedVal[(start + 15)];
        this.drop_number = start / 19;//18; // 18 : NB_LOGGED_DATAS
        //	for(int i=0 ; i < 250 ; i++){
        //		this.analogicValuesPh1[i]=(short) convertedVal[i+(start+22)];
        //		this.analogicValuesPh2[i]=(short) convertedVal[i+(start+272)];
        //		this.analogicValuesPh3[i]=(short) convertedVal[i+(start+522)];
        //	}
    }

    /*-------------------------------------------------------------------------*/

    /**
     * Constructor for the Drop Class.
     */
    /*-------------------------------------------------------------------------*/
    public Drop(long[] convertedVal, int start) {
        this.rmsPh1 = (int) convertedVal[(start + 6)];
        this.rmsPh2 = (int) convertedVal[(start + 7)];
        this.rmsPh3 = (int) convertedVal[(start + 8)];
        this.meanPh1 = (int) convertedVal[(start + 14)];
        this.meanPh2 = (int) convertedVal[(start + 15)];
        this.meanPh3 = (int) convertedVal[(start + 16)];
        this.freqPh1 = convertedVal[(start + 9)];
        this.freqPh2 = convertedVal[(start + 10)];
        this.freqPh3 = convertedVal[(start + 11)];
        this.duration = (int) (convertedVal[(start + 13)] * 20);
        this.coding = 0;
        this.dsp_time = date_in_ms(convertedVal[(start + 0)]);
        //this.dsp_time	=	new Date((long) convertedVal[(start+0)]);
        this.drop_number = start;


    }
    /*-------------------------------------------------------------------------*/

    /**
     * Formating date in milliseconds
     *
     * @param ms number of milliseconds.
     * @retrun a string equivalent of the ms in param.
     */
    /*-------------------------------------------------------------------------*/
    public static String in_ms(long ms) {
        String s = new String();
        if (ms >= 1)
            s = ".00" + ms;
        if (ms >= 10)
            s = ".0" + ms;
        if (ms >= 100)
            s = "." + ms;
        return s;
    }
    /*-------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public static Drop fill_data(String data, int drop_number) {
        Drop filled_drop = new Drop();
        StringTokenizer st = new StringTokenizer(data);
        filled_drop.dsp_time = st.nextToken();
        filled_drop.dsp_time += " - " + st.nextToken();
        filled_drop.rmsPh1 = new Integer(st.nextToken());
        filled_drop.rmsPh2 = new Integer(st.nextToken());
        filled_drop.rmsPh3 = new Integer(st.nextToken());
        filled_drop.meanPh1 = new Integer(st.nextToken());
        filled_drop.meanPh2 = new Integer(st.nextToken());
        filled_drop.meanPh3 = new Integer(st.nextToken());
        filled_drop.freqPh1 = new Integer(st.nextToken());
        filled_drop.freqPh2 = new Integer(st.nextToken());
        filled_drop.freqPh3 = new Integer(st.nextToken());
        filled_drop.coding = new Integer(st.nextToken());
        filled_drop.durationPh1 = new Integer(st.nextToken());
        filled_drop.durationPh2 = new Integer(st.nextToken());
        filled_drop.durationPh3 = new Integer(st.nextToken());
        filled_drop.OVdurationPh1 = new Integer(st.nextToken());
        filled_drop.OVdurationPh2 = new Integer(st.nextToken());
        filled_drop.OVdurationPh3 = new Integer(st.nextToken());
        filled_drop.drop_number = drop_number;
        //filled_drop.toString();
        return filled_drop;
    }

    /*-------------------------------------------------------------------------*/

    /**
     * Formating date in milliseconds
     *
     * @param logged_time
     * @retrun a string equivalent of the date.
     */
    /*-------------------------------------------------------------------------*/
    public String date_in_ms(long logged_time) {
        //System.out.println("converting in ms");
        long nb_sec = logged_time / 1000;
        long ms = logged_time - (nb_sec * 1000);
        Date date_in_milli = new Date(logged_time);
        String time = DateFormat.getDateTimeInstance(java.text.DateFormat.MEDIUM,
                java.text.DateFormat.FULL).format(date_in_milli);
        String result = time.substring(0, time.length() - 8) + "." + String.format("%003d", (int) ms) +
                time.substring(time.length() - 8, time.length());
        return result;
    }
    /*-------------------------------------------------------------------------*/

    /**
     */
    /*-------------------------------------------------------------------------*/
    public jhqps.elecdrops.Drop copyDrop(Drop droptocopy) {

        jhqps.elecdrops.Drop copiedDrop = new jhqps.elecdrops.Drop();

        copiedDrop.drop_number = droptocopy.drop_number;
        copiedDrop.rmsPh1 = droptocopy.rmsPh1;
        copiedDrop.rmsPh2 = droptocopy.rmsPh2;
        copiedDrop.rmsPh3 = droptocopy.rmsPh3;
        copiedDrop.freqPh1 = droptocopy.freqPh1;
        copiedDrop.freqPh2 = droptocopy.freqPh2;
        copiedDrop.freqPh3 = droptocopy.freqPh3;
        copiedDrop.meanPh1 = droptocopy.meanPh1;
        copiedDrop.meanPh2 = droptocopy.meanPh2;
        copiedDrop.meanPh3 = droptocopy.meanPh3;
        copiedDrop.durationPh1 = droptocopy.durationPh1;
        copiedDrop.durationPh2 = droptocopy.durationPh2;
        copiedDrop.durationPh3 = droptocopy.durationPh3;
        copiedDrop.OVdurationPh1 = droptocopy.OVdurationPh1;
        copiedDrop.OVdurationPh2 = droptocopy.OVdurationPh2;
        copiedDrop.OVdurationPh3 = droptocopy.OVdurationPh3;
        copiedDrop.coding = droptocopy.coding;

        for (int i = 0 ; i<250 ; i++) {
            copiedDrop.analogicValuesPh1[i] = droptocopy.analogicValuesPh1[i];
            copiedDrop.analogicValuesPh2[i] = droptocopy.analogicValuesPh2[i];
            copiedDrop.analogicValuesPh3[i] = droptocopy.analogicValuesPh3[i];
        }

        return (copiedDrop);
    }

    /**
     * toString() universal method
     *
     * @retrun prints drop parameters to stdout.
     */
    /*-------------------------------------------------------------------------*/
    public String toString() {
        System.out.println("_________________________________");
        System.out.println("Drop number : " + (drop_number + 1));
        System.out.println("Drop date : " + dsp_time);
        System.out.println("Phase 1 RMS : " + rmsPh1 + " V");
        System.out.println("Phase 2 RMS : " + rmsPh2 + " V");
        System.out.println("Phase 3 RMS : " + rmsPh3 + " V");
        System.out.println("Phase 1 mean : " + meanPh1 + " V");
        System.out.println("Phase 2 mean : " + meanPh2 + " V");
        System.out.println("Phase 3 mean : " + meanPh3 + " V");
        System.out.println("Phase 1 freq : " + freqPh1 + " Hz");
        System.out.println("Phase 2 freq : " + freqPh2 + " Hz");
        System.out.println("Phase 3 freq : " + freqPh3 + " Hz");
        System.out.println("Phase 1 Drop duration : " + durationPh1 + " ms");
        System.out.println("Phase 2 Drop duration : " + durationPh2 + " ms");
        System.out.println("Phase 3 Drop duration : " + durationPh3 + " ms");
        System.out.println("Phase 1 Overflow duration : " + OVdurationPh1);
        System.out.println("Phase 2 Overflow duration : " + OVdurationPh2);
        System.out.println("Phase 3 Overflow duration : " + OVdurationPh3);
        System.out.println("Drop coding : " + coding);
        //for(int i=0 ; i < 250 ; i++){
        //	System.out.println("analogicValuesPh1["+i+"]"+analogicValuesPh1[i]);
        //	System.out.println("analogicValuesPh2["+i+"]"+analogicValuesPh2[i]);
        //	System.out.println("analogicValuesPh3["+i+"]"+analogicValuesPh3[i]);
        //}
        return ("");

    }
}
