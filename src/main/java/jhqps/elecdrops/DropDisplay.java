///+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/elecdrops/DropDisplay.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: DropDisplay.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.3  2009/01/30 15:27:10  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
// Revision 1.2  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
/*
 * DropDisplay.java
 *
 * Created on August 13, 2002, 10:10 AM
 */
/**
 * @author Goudard
 */
package jhqps.elecdrops;

//Import ATK Classes
//====================

import fr.esrf.tangoatk.widget.util.JSmoothLabel;
import fr.esrf.tangoatk.widget.util.chart.JLChart;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

//Import Standard Java Classes
//==============================

public class DropDisplay extends JDialog implements ElecDropDefs {

    public static jhqps.elecdrops.Drop thisDrop;
    final JLDataView AnalogicValues1 = new JLDataView();
    final JLDataView AnalogicValues2 = new JLDataView();
    final JLDataView AnalogicValues3 = new JLDataView();
    public int local_index;
    public JList local_list;
    public Drop[] local_drop;
    private JLChart AnalogicValuesChart = new JLChart();
    /*-------------------------------------------------------------------------*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton okBtn;
    /*-------------------------------------------------------------------------*/
    //private javax.swing.JButton graphBtn;
    private javax.swing.JButton nextBtn;
    /*-------------------------------------------------------------------------*/
    private javax.swing.JButton previousBtn;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel ph1Lbl;
    private JSmoothLabel ph2Lbl;
    private JSmoothLabel ph3Lbl;
    private JSmoothLabel rmsPh1Lbl;
    private JSmoothLabel rmsPh2Lbl;
    /*-------------------------------------------------------------------------*/
    private JSmoothLabel rmsPh3Lbl;
    private JSmoothLabel freqPh1Lbl;
    /// PROBLEM WITH OPENNG 2 drop appli !
    private JSmoothLabel freqPh2Lbl;
    private JSmoothLabel freqPh3Lbl;
    private JSmoothLabel rmsValueLbl;
    private JSmoothLabel freqLbl;
    private JSmoothLabel dateLbl;
    private JSmoothLabel durationLbl;
    private JSmoothLabel durationPh1Lbl;
    private JSmoothLabel durationPh2Lbl;
    private JSmoothLabel durationPh3Lbl;
    private JSmoothLabel h0Lbl;
    private JSmoothLabel meanPh1Lbl;
    private JSmoothLabel meanPh2Lbl;
    private JSmoothLabel meanPh3Lbl;
    private JSmoothLabel dropnumberLbl;

    /**
     * Creates new form DropDisplay
     */
    /*-------------------------------------------------------------------------*/
    public DropDisplay(javax.swing.JFrame parent, boolean modal, jhqps.elecdrops.Drop[] the_drops, int index, JList list) {
        super(parent, modal);
        local_list = list;
        local_drop = the_drops;
        local_index = index;
        try {
            initComponents();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Passing Drop Values in the constructor
     */
    /*-------------------------------------------------------------------------*/
    public DropDisplay(javax.swing.JFrame parent, boolean modal, jhqps.elecdrops.Drop dropToDisplay, String dropdate, jhqps.elecdrops.Drop[] the_drops, int index, JList list) {
        super(parent, modal);
        local_list = list;
        local_drop = the_drops;
        local_index = index;
        try {
            initComponents();
        } catch (IOException e) {
            System.out.println(e);
        }
        updateLabels(dropToDisplay, dropdate);
        thisDrop = dropToDisplay.copyDrop(dropToDisplay);
    }

    /**
     * @param args the command line arguments
     */
    /*-------------------------------------------------------------------------*/
    public static void main(String args[]) {
        //new DropDisplay(null, true,the_drops).setVisible(true);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    /*-------------------------------------------------------------------------*/
    void initComponents() throws IOException, FileNotFoundException {//GEN-BEGIN:initComponents
        okBtn = new javax.swing.JButton("OK");
        //graphBtn 		= new javax.swing.JButton("Graph");
        nextBtn = new javax.swing.JButton();
        previousBtn = new javax.swing.JButton();
        ph1Lbl = new JSmoothLabel();
        ph1Lbl.setText("Phase 1");
        ph2Lbl = new JSmoothLabel();
        ph2Lbl.setText("Phase 2");
        ph3Lbl = new JSmoothLabel();
        ph3Lbl.setText("Phase 3");
        rmsPh1Lbl = new JSmoothLabel();
        rmsPh2Lbl = new JSmoothLabel();
        rmsPh3Lbl = new JSmoothLabel();
        freqPh1Lbl = new JSmoothLabel();
        freqPh2Lbl = new JSmoothLabel();
        freqPh3Lbl = new JSmoothLabel();
        rmsValueLbl = new JSmoothLabel();
        rmsValueLbl.setText("RMS Value ");
        freqLbl = new JSmoothLabel();
        freqLbl.setText("Period ");
        dateLbl = new JSmoothLabel();
        dateLbl.setText("Date : ");
        durationPh1Lbl = new JSmoothLabel();
        durationPh2Lbl = new JSmoothLabel();
        durationPh3Lbl = new JSmoothLabel();
        durationLbl = new JSmoothLabel();
        durationLbl.setText("Drop duration");
        h0Lbl = new JSmoothLabel();
        h0Lbl.setText("Mean Value");
        meanPh1Lbl = new JSmoothLabel();
        meanPh2Lbl = new JSmoothLabel();
        meanPh3Lbl = new JSmoothLabel();
        dropnumberLbl = new JSmoothLabel();
        AnalogicValuesChart = new JLChart();

        // AnalogicValuesChart chart setup
        AnalogicValuesChart.setHeader("Analogic values.");
        AnalogicValuesChart.setHeaderVisible(true);
        AnalogicValuesChart.setBackground(Color.white);
        AnalogicValuesChart.setChartBackground(new Color(238, 232, 170));
        AnalogicValuesChart.setHeaderFont(new Font("Arial", Font.BOLD, 18));
        AnalogicValuesChart.setLabelFont(new Font("Dialog", Font.BOLD, 12));
        AnalogicValuesChart.getY1Axis().setName("Tension (V)");
        AnalogicValuesChart.getY1Axis().setAutoScale(false);
        AnalogicValuesChart.getY1Axis().setGridVisible(true);
        AnalogicValuesChart.getY1Axis().setSubGridVisible(true);
        AnalogicValuesChart.getXAxis().setName("Time (ms)");
        AnalogicValuesChart.getXAxis().setGridVisible(true);
        AnalogicValuesChart.getXAxis().setAutoScale(false);
        AnalogicValuesChart.getXAxis().setMinimum(0);
        AnalogicValuesChart.getXAxis().setMaximum(200);
        AnalogicValuesChart.setPreferredSize(new Dimension(480, 360));
        AnalogicValues1.setColor(Color.red);
        AnalogicValues2.setColor(Color.blue);
        AnalogicValues3.setColor(Color.green);
        AnalogicValues1.setName("Phase1");
        AnalogicValues2.setName("Phase2");
        AnalogicValues3.setName("Phase3");

        getContentPane().setLayout(new java.awt.GridBagLayout());
        getContentPane().setBackground(java.awt.Color.white);
        getContentPane().setForeground(java.awt.Color.white);
        this.setLocation(300, 0);

        GridBagConstraints gridBagConstraints1;
        Font font = new Font("Arial", 1, 14);
        Font font1 = new Font("Arial", 1, 20);
        Color darkBlue = new Color(0, 20, 200);

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        okBtn.setFocusPainted(false);
        okBtn.setBackground(java.awt.Color.white);
        okBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                okBtnActionPerformed(evt);
            }
        });

        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 8;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;

        getContentPane().add(okBtn, gridBagConstraints1);

        ph1Lbl.setFont(font);
        ph1Lbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 2;
        getContentPane().add(ph1Lbl, gridBagConstraints1);

        ph2Lbl.setFont(font);
        ph2Lbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 2;
        getContentPane().add(ph2Lbl, gridBagConstraints1);

        ph3Lbl.setFont(font);
        ph3Lbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 2;
        getContentPane().add(ph3Lbl, gridBagConstraints1);

        rmsPh1Lbl.setFont(font);
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 3;
        getContentPane().add(rmsPh1Lbl, gridBagConstraints1);

        rmsPh2Lbl.setFont(font);
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 3;
        getContentPane().add(rmsPh2Lbl, gridBagConstraints1);

        rmsPh3Lbl.setFont(font);
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 3;
        getContentPane().add(rmsPh3Lbl, gridBagConstraints1);

        freqPh1Lbl.setFont(font);
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 4;
        getContentPane().add(freqPh1Lbl, gridBagConstraints1);

        freqPh2Lbl.setFont(font);
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 4;
        getContentPane().add(freqPh2Lbl, gridBagConstraints1);

        freqPh3Lbl.setFont(font);
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 4;
        getContentPane().add(freqPh3Lbl, gridBagConstraints1);

        rmsValueLbl.setFont(font);
        rmsValueLbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 3;
        getContentPane().add(rmsValueLbl, gridBagConstraints1);

        freqLbl.setFont(font);
        freqLbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 4;
        getContentPane().add(freqLbl, gridBagConstraints1);


        durationLbl.setFont(font);
        durationLbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 7;
        getContentPane().add(durationLbl, gridBagConstraints1);

        durationPh1Lbl.setFont(font);
        durationPh1Lbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 7;
        getContentPane().add(durationPh1Lbl, gridBagConstraints1);

        durationPh2Lbl.setFont(font);
        durationPh2Lbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 7;
        getContentPane().add(durationPh2Lbl, gridBagConstraints1);

        durationPh3Lbl.setFont(font);
        durationPh3Lbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 7;
        getContentPane().add(durationPh3Lbl, gridBagConstraints1);

        h0Lbl.setFont(font);
        h0Lbl.setForeground(darkBlue);
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 5;
        getContentPane().add(h0Lbl, gridBagConstraints1);

        meanPh1Lbl.setFont(font);
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.gridy = 5;
        getContentPane().add(meanPh1Lbl, gridBagConstraints1);

        meanPh2Lbl.setFont(font);
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 5;
        getContentPane().add(meanPh2Lbl, gridBagConstraints1);

        meanPh3Lbl.setFont(font);
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 5;
        getContentPane().add(meanPh3Lbl, gridBagConstraints1);

        dropnumberLbl.setFont(font1);
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 0;
        gridBagConstraints1.weightx = 1.0;
        gridBagConstraints1.weighty = 1.0;
        gridBagConstraints1.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        gridBagConstraints1.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        getContentPane().add(dropnumberLbl, gridBagConstraints1);


        nextBtn.setFocusPainted(false);
        nextBtn.setBorder(BorderFactory.createEmptyBorder());
        nextBtn.setBackground(java.awt.Color.white);
        nextBtn.setToolTipText("Click to display next drop");
        nextBtn.setIcon(new ImageIcon(getClass().getResource("/jhqps/arrow2.gif")));
        nextBtn.setPreferredSize(new Dimension(32, 32));
        nextBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                nextBtnActionPerformed(evt);
            }
        });

        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 3;
        gridBagConstraints1.gridy = 8;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(nextBtn, gridBagConstraints1);

        previousBtn.setFocusPainted(false);
        previousBtn.setBorder(BorderFactory.createEmptyBorder());
        previousBtn.setBackground(java.awt.Color.white);
        previousBtn.setToolTipText("Click to display previous drop");
        previousBtn.setIcon(new ImageIcon(getClass().getResource("/jhqps/arrow1.gif")));
        previousBtn.setPreferredSize(new Dimension(32, 32));

        previousBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                previousBtnActionPerformed(evt);
            }
        });
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 8;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(previousBtn, gridBagConstraints1);

	/*	 graphBtn.setFocusPainted(false);
		 graphBtn.setBackground(java.awt.Color.white);
		 graphBtn.addActionListener(new ActionListener() {
			 public void actionPerformed(ActionEvent evt) {
				 graphBtnActionPerformed(evt);
			 }
		 });*/
        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridx = 2;
        gridBagConstraints1.gridy = 8;
        gridBagConstraints1.insets = new java.awt.Insets(10, 10, 10, 10);
        // getContentPane().add(graphBtn, gridBagConstraints1);

        dateLbl.setFont(font);
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        getContentPane().add(dateLbl, gridBagConstraints1);
    }

    /**
     * Exit the Application
     */
    /*-------------------------------------------------------------------------*/
    private void exitForm(java.awt.event.WindowEvent evt) {
        this.setVisible(false);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    private void okBtnActionPerformed(ActionEvent evt) {
        this.setVisible(false);
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
	/*private void graphBtnActionPerformed(ActionEvent evt) {
		//	Display graphical view of the drop
		//====================================
		this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
		new AnalogicDrop(null,true).setVisible(true);
		this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}*/
    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void nextBtnActionPerformed(ActionEvent evt) {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        // View Next Drop
        //===============
        local_index++;
        //if(ElecDropsGUI.trace)
        System.out.println("nextBtnActionPerformed: index = " + local_index);
        try {
            local_drop[local_index].drop_number = local_index;
            updateLabels(local_drop[local_index], local_drop[local_index].dsp_time);
            local_list.setSelectedIndex(local_index);
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(this,
                    "No more Drops to Display !",
                    "Information",
                    JOptionPane.INFORMATION_MESSAGE);
            local_index--;
        }
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void previousBtnActionPerformed(ActionEvent evt) {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        // View Previous Drop
        //===================
        local_index--;
        //if(ElecDropsGUI.trace)
        System.out.println("previousBtnActionPerformed: index = " + local_index);
        try {
            local_drop[local_index].drop_number = local_index;
            updateLabels(local_drop[local_index], local_drop[local_index].dsp_time);
            local_list.setSelectedIndex(local_index);
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(this,
                    "No more Drops to Display !",
                    "Information",
                    JOptionPane.INFORMATION_MESSAGE);
            local_index++;
        }
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    public void updateLabels(jhqps.elecdrops.Drop dropToDisplay, String dropdate) {
        Color darkGreen = new Color(0, 153, 102);
        thisDrop = dropToDisplay.copyDrop(dropToDisplay);

        AnalogicValues1.reset();
        AnalogicValues2.reset();
        AnalogicValues3.reset();
        AnalogicValuesChart.getY1Axis().clearDataView();

        if (ElecDropsGUI.trace)
            dropToDisplay.toString();

        setTitle("DROP " + (dropToDisplay.drop_number + 1));
        rmsPh1Lbl.setText(dropToDisplay.rmsPh1 + " V");
        rmsPh2Lbl.setText(dropToDisplay.rmsPh2 + " V");
        rmsPh3Lbl.setText(dropToDisplay.rmsPh3 + " V");
        if (dropToDisplay.freqPh1 != -1)
            freqPh1Lbl.setText(String.format("%02d", (int) dropToDisplay.freqPh1) + " us");
        else
            freqPh1Lbl.setText("> 22 ms");
        if (dropToDisplay.freqPh2 != -1)
            freqPh2Lbl.setText(String.format("%02d", (int) dropToDisplay.freqPh2) + " us");
        else
            freqPh2Lbl.setText("> 22 ms");
        if (dropToDisplay.freqPh3 != -1)
            freqPh3Lbl.setText(String.format("%02d", (int) dropToDisplay.freqPh3) + " us");
        else
            freqPh3Lbl.setText("> 22 ms");

        meanPh1Lbl.setText(dropToDisplay.meanPh1 + " V");
        meanPh2Lbl.setText(dropToDisplay.meanPh2 + " V");
        meanPh3Lbl.setText(dropToDisplay.meanPh3 + " V");
        dropnumberLbl.setText("Drop number " + (dropToDisplay.drop_number + 1));
        dateLbl.setText("Date : " + dropdate);
        durationLbl.setText("Drop duration ");
        durationPh1Lbl.setText(dropToDisplay.durationPh1 * 20 + "ms");
        durationPh2Lbl.setText(dropToDisplay.durationPh2 * 20 + "ms");
        durationPh3Lbl.setText(dropToDisplay.durationPh3 * 20 + "ms");

        Color darkBlue = new Color(0, 20, 200);
        meanPh1Lbl.setForeground(darkBlue);
        meanPh2Lbl.setForeground(darkBlue);
        meanPh3Lbl.setForeground(darkBlue);


        //	Analogic values filling
        int max = 0;
        for (int i = 0 ; i<250 ; i++) {
            AnalogicValues1.add(i * 0.8, dropToDisplay.analogicValuesPh1[i] * coeff);
            AnalogicValues2.add(i * 0.8, dropToDisplay.analogicValuesPh2[i] * coeff);
            AnalogicValues3.add(i * 0.8, dropToDisplay.analogicValuesPh3[i] * coeff);

            if (dropToDisplay.analogicValuesPh1[i]>max)
                max = dropToDisplay.analogicValuesPh1[i];
            if (dropToDisplay.analogicValuesPh2[i]>max)
                max = dropToDisplay.analogicValuesPh2[i];
            if (dropToDisplay.analogicValuesPh3[i]>max)
                max = dropToDisplay.analogicValuesPh3[i];
        }
        AnalogicValuesChart.getY1Axis().setMaximum((max * coeff) + 250);
        AnalogicValuesChart.getY1Axis().setMinimum(-(max * coeff) - 250);
        AnalogicValuesChart.getY1Axis().addDataView(AnalogicValues1);
        AnalogicValuesChart.getY1Axis().addDataView(AnalogicValues2);
        AnalogicValuesChart.getY1Axis().addDataView(AnalogicValues3);
        AnalogicValuesChart.repaint();

        // Initialize colors with DSP coding
        //===================================
        //	boolean pb_ph1, pb_ph2, pb_ph3;
        //if (dropToDisplay.coding < 10)
        //	pb_ph1 = true;
        int result = dropToDisplay.coding & PH1_RMS;
        System.out.println(result + " = result");
        if (result == PH1_RMS)
            rmsPh1Lbl.setForeground(java.awt.Color.red);
        else
            rmsPh1Lbl.setForeground(darkGreen);
        result = dropToDisplay.coding & PH2_RMS;
        if (result == PH2_RMS)
            rmsPh2Lbl.setForeground(java.awt.Color.red);
        else
            rmsPh2Lbl.setForeground(darkGreen);
        result = dropToDisplay.coding & PH3_RMS;
        if (result == PH3_RMS)
            rmsPh3Lbl.setForeground(java.awt.Color.red);
        else
            rmsPh3Lbl.setForeground(darkGreen);
        result = dropToDisplay.coding & PH1_PERIOD;
        if (result == PH1_PERIOD)
            freqPh1Lbl.setForeground(java.awt.Color.red);
        else
            freqPh1Lbl.setForeground(darkGreen);
        result = dropToDisplay.coding & PH2_PERIOD;
        if (result == PH2_PERIOD)
            freqPh2Lbl.setForeground(java.awt.Color.red);
        else
            freqPh2Lbl.setForeground(darkGreen);
        result = dropToDisplay.coding & PH3_PERIOD;
        if (result == PH3_PERIOD)
            freqPh3Lbl.setForeground(java.awt.Color.red);
        else
            freqPh3Lbl.setForeground(darkGreen);

        System.out.println("coding = " + dropToDisplay.coding);
        pack();
    }

    public int getNewIndex() {
        int new_index = local_index;
        return (new_index);
    }
    // End of variables declaration//GEN-END:variables

}
