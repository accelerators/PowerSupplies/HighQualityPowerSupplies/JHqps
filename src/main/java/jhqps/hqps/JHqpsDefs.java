//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsDefs.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: JHqpsDefs.java,v $
// Revision 1.55  2009/08/25 13:53:21  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:17  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:49  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.4  2009/01/30 15:26:49  goudard
// In drop appli :
//  - Correct -1 in DropDisplay.
// In vibration History :
//  - Gradient correct setup
//  - Correct scale between 9.81mg and maximum
// Check HqpsBinIn to display Hqps mode.
//
// Revision 1.3  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.hqps;
/**
 * @author : O. Goudard
 */ 

public interface JHqpsDefs {

	int NB_MACHINE	= 16;
	int TANGO_HOST_PORT = 10000;
	String HDB_ATTRIBUTE_NAME = "last_event";

	// Tango devices to retrieve data history of HQPS
	String [] virtualPrinterDevices = {
			"infra/hqps-vprinter/1",
			"infra/hqps-vprinter/4",
			"infra/hqps-vprinter/5",
			"infra/hqps-vprinter/6",
			"infra/hqps-vprinter/7",
			"infra/hqps-vprinter/8",
			"infra/hqps-vprinter/9",
			"infra/hqps-vprinter/10",
			"infra/hqps-vprinter/master"
	};

  	String [] virtualPrinterLabels = {
		"Genset 1",
		"RB 4A/4B",
		"RB 5A/5B",
		"RB 6A/6B",
		"RB 7A/7B",
		"RB 8A/8B",
		"RB 9A/9B",
		"RB 10A/10B",
		"Master PLC"
  	};

}
