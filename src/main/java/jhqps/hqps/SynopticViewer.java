package jhqps.hqps;

import fr.esrf.tangoatk.widget.jdraw.SynopticProgressListener;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.Splash;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.MissingResourceException;

/**
 * A simple synoptic viewer
 */
public class SynopticViewer extends JFrame implements ActionListener, SynopticProgressListener {

    private JMenuItem exitMenuItem;
    private JMenuItem viewErrorMenuItem;
    private JMenuItem viewDiagMenuItem;
    private TangoSynopticHandler synoptic;
    private ErrorHistory errorHistory;
    private Splash splash;

    public SynopticViewer(String synopticAndTitle) {

        String[] items = synopticAndTitle.split(",");
        if (items.length != 2) {
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Invalid argument syntax [title,synoptic name] expected",
                    "SynopticViewer error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            return;
        }

        String title = items[0];
        String synopticName = items[1];

        // Create menu
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(this);
        fileMenu.add(exitMenuItem);
        menuBar.add(fileMenu);

        JMenu viewMenu = new JMenu("View");
        viewErrorMenuItem = new JMenuItem("Errors ...");
        viewErrorMenuItem.addActionListener(this);
        viewMenu.add(viewErrorMenuItem);
        viewDiagMenuItem = new JMenuItem("Diagnostic ...");
        viewDiagMenuItem.addActionListener(this);
        viewMenu.add(viewDiagMenuItem);
        menuBar.add(viewMenu);

        setJMenuBar(menuBar);

        // Splash Window
        splash = new Splash();
        splash.setTitle(title);
        splash.setCopyright("(c) ESRF");
        splash.setMessage("Loading synoptic ...");
        splash.initProgress();

        // Load the synoptic from stream
        errorHistory = new ErrorHistory();
        synoptic = new TangoSynopticHandler();
        synoptic.setErrorHistoryWindow(errorHistory);

        InputStream jdFileInStream = this.getClass().getResourceAsStream(synopticName);
        if (jdFileInStream == null) {
            splash.setVisible(false);
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Failed to get the inputStream for the synoptic file resource : /jhqps/jdraw_files/Main_HQPS2.jdw \n\n",
                    "Resource error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            return;
        }
        InputStreamReader inStrReader = new java.io.InputStreamReader(jdFileInStream);
        try {
            synoptic.setProgressListener(this);
            synoptic.loadSynopticFromStream(inStrReader);
            synoptic.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
        } catch (java.io.IOException ioex) {
            splash.setVisible(false);
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Cannot find load the synoptic input stream reader.\n"
                            + " Application will abort ...\n"
                            + ioex,
                    "Resource access failed",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            return;
        } catch (MissingResourceException mrEx) {
            splash.setVisible(false);
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Cannot parse the synoptic file : Main_HQPS2.jdw.\n"
                            + " Application will abort ...\n"
                            + mrEx,
                    "Cannot parse the file",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            return;
        } catch (Exception e) {
            splash.setVisible(false);
            System.out.println("exception : " + e.getMessage() + e.toString());
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Null pointer exception"
                            + e.toString(),
                    e.getMessage(),
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            return;
        }

        synoptic.setAutoZoom(true);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closePanel();
                super.windowClosing(e);
            }
        });

        splash.setVisible(false);
        setContentPane(synoptic);
        setTitle(title + "[" + synopticName + "]");
        ATKGraphicsUtils.centerFrameOnScreen(this);
        setVisible(true);

    }

    public void progress(double p) {

        splash.progress((int) (p * 100.0));

    }

    private void closePanel() {

        synoptic.getAttributeList().stopRefresher();
        dispose();

    }

    public void actionPerformed(ActionEvent e) {

        Object src = e.getSource();
        if (src == exitMenuItem) {
            closePanel();
            setVisible(false);
        } else if (src == viewErrorMenuItem) {
            ATKGraphicsUtils.centerFrameOnScreen(errorHistory);
            errorHistory.setVisible(true);
        } else if (src == viewDiagMenuItem) {
            fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
        }

    }


}
