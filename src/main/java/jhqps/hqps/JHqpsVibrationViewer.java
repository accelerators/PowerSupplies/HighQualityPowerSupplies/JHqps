//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsVibrationViewer.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: JHqpsVibrationViewer.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.3  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.hqps;

/**
 * @author : O. Goudard
 */ 

  import java.util.*;
  import fr.esrf.tangoatk.core.*;
  import fr.esrf.tangoatk.widget.util.chart.*;


  public class JHqpsVibrationViewer 
	extends fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer 
    implements fr.esrf.tangoatk.core.INumberScalarListener
{
  List<INumberScalar>   peak_models = null;
  List<JLDataView>      peak_data_views = null;
  INumberScalar         x_min_model = null;
  INumberScalar         x_max_model = null;
  INumberScalar         ind_track_span_model = null;
  INumberSpectrum	refCurveModel = null;
  INumberScalar			refCurveMinxModel = null, refCurveMaxxModel = null;
  double                ref_min_x=0, ref_max_x=0;
  double                min_x = 0;
  double                max_x = 0;
  double               min_y1 = 0;
  double               max_y1 = 0;
  int                   	index_track_span=0;
  boolean               xAxisAutoScale=true;
  
	  
	  /**
	   * Creates new JtmNumberSpectrumPeakViewer 
	   */
	  public JHqpsVibrationViewer()
	  {
	      // Create the graph
	      super();
	      peak_models = new Vector <INumberScalar> ();
	      peak_data_views = new Vector <JLDataView> ();
	  }

 	  /**
	   * Sets the min and max values for X axis to two scalar attributes
	   *
	   * @param max
	   */
	  public void setXaxisModels(INumberScalar max)
	  {
	     if (x_max_model != null)
	     {
	        x_max_model.removeNumberScalarListener(this);
	        x_max_model = null;
	     }
	     if (max == null)
	       return;
	     x_max_model = max;
	     x_max_model.addNumberScalarListener(this);
	     max.refresh();
 	  }

	  public void numberScalarChange(NumberScalarEvent evt)
	  {
	     
	     if (evt.getSource() == x_min_model)
	     {
	        minXchange(evt);
	        return;
	     }
	     
	     if (evt.getSource() == x_max_model)
	     {
	        maxXchange(evt);
	        return;
	     }
  	  }
	  private void minXchange(NumberScalarEvent evt)
	  {
	       min_x = 0;
		  changeAffineTransform();
	  }


	  private void maxXchange(NumberScalarEvent evt)
	  {
	       max_x = evt.getValue();
	       
	       if (min_x < max_x)
	       {
	    	   if (xAxisAutoScale != true);
	    	   {
	             getXAxis().setMinimum(min_x);
	             getXAxis().setMaximum(max_x);
	    	   }
	    	   changeAffineTransform();
	       }
	  }
	  private void changeAffineTransform()
	  {
	      int      nb_x_values;
	      double   step;
	      
	      if (dvy == null)
	         return;
		 
	      nb_x_values = dvy.getDataLength();
	      if (nb_x_values <= 0)
	         return;
	      
	      step =  1.0 / (max_x * 0.01);
	      
	      setXAxisAffineTransform(min_x, step);
	            
	  }
}
