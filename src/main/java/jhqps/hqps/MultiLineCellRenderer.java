package jhqps.hqps;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * <p>Copyright: Copyright (c) 2002</p>
 *
 * @author Guillaume Barreau (guillaume@runtime-collective.com)
 * @version 1.0
 * <p>
 * Distributable under GPL license.
 * See terms of license at gnu.org.
 */

public class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {

    private static final Color selColor = new Color(200, 200, 255);
    private static final Color firstColumnColor = new Color(0xdd, 0xdd, 0xdd);
    private static final Color errorColor = new Color(0xff, 0xdd, 0xaa);
    private static final Color restartColor = new Color(0xaa, 0xff, 0xaa);

    MultiLineCellRenderer() {
        setEditable(false);
        setLineWrap(false);
        setWrapStyleWord(false);
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        boolean errorLine = false;
        boolean restartLine = false;
        if (value instanceof String) {
            setText((String) value);
            if (((String)value).toLowerCase().contains("error"))
                errorLine = true;
            else
            if (((String)value).toLowerCase().contains("no new event"))
                restartLine = true;
        } else
            setText("");


        int selRow = table.getSelectedRow();
        if (selRow == row)
            setBackground(selColor);
        else {
            if (column == 0)
                setBackground(firstColumnColor);
            else
            if (errorLine)
                setBackground(errorColor);
            else
            if (restartLine)
                setBackground(restartColor);
            else
                setBackground(Color.white);
        }

        return this;
    }
}

