///+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsThread.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: JHqpsThread.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.3  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.hqps;
/**
 * @author : O. Goudard
 */ 

import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;

/*-------------------------------------------------------------------------*/
/**
 *	This class defines the Logger methods to store electrical drop on
 *	HQPS datas in a file. The logger file is named loggerFilename. 	
 * @author  Olivier Goudard
 * @version 1.0
 */
/*-------------------------------------------------------------------------*/
public class JHqpsThread extends Thread //implements StandbyAssistantDefs 
{
/**
 *	Add verbose if true.
 */
public static boolean 		trace = false; 
/**
 *	Add verbose if true.
 */
public static boolean 		first = true; 

/**
 *	Currents data in ESRF.
 */
static float [] 			currentdata;
/**
 *	Magnetic field data on Dipole magnets
 */
static float 	 			fielddata;
/**
 *	Date
 */
static String 				time;
/**
 *	Date Format to display in the application.
 */
public DateFormat df ;
/*-------------------------------------------------------------------------*/
/**
*	Constructor for the StandbyAssistantThread class.
*	This class implments a data collection thread on infra/harmcor/1 and infra/harmcor/2 .
*/
/*-------------------------------------------------------------------------*/ 	
public JHqpsThread () 
{
    // Compute the date/time string to display in the page header
    df = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
    df.setTimeZone(TimeZone.getDefault());
}
/*-------------------------------------------------------------------------*/
/**
*	scanSinewaves() method : retrieving data.
*/
/*-------------------------------------------------------------------------*/
public void scan()
{	
    time = df.format(new Date());
	if(trace){	
		System.out.println("time =" + time.toString());
	}
}

/*-------------------------------------------------------------------------*/
/**
 *	sleep method : waits n milliseconds
 *	@param millis : sleep time in milliseconds.
 */
/*-------------------------------------------------------------------------*/
private synchronized void sleep(int millis){
	try {	
		wait(millis);
	} 
	catch (InterruptedException e) {
	}
}

/*-------------------------------------------------------------------------*/	
/**
*	run() method : starts the thread.
*/
/*-------------------------------------------------------------------------*/	
public void run() 
{
	while(true) 
	{
		scan();
		if(!first)
			sleep(1000);
		JHqpsMainFrame.UpdateFields(time); 
		first=false;
	}
}

} // End of class
