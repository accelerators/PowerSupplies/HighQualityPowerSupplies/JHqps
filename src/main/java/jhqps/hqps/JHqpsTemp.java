//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsTemp.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: JHqpsTemp.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.3  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.hqps;
/**
 * @author : O. Goudard
 */ 

//	Import tango classes
import java.awt.event.ActionEvent;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.widget.attribute.ScalarListViewer;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;

public class JHqpsTemp extends javax.swing.JFrame implements JHqpsDefs {
	// Device to change sampling period and number of point for FFT average
	
	INumberScalar bearing_temp_Att = null;	
	INumberScalar cell_temp_Att = null;
	AttributeList 	attl ;
	AttributeList 	attl2 ;
	ScalarListViewer  bearing_temp_viewer = new ScalarListViewer();
	ScalarListViewer  cell_temp_viewer = new ScalarListViewer();
   
	
	public JHqpsTemp()
	{
		attl = new AttributeList();
		attl2 = new AttributeList();
	   	errorHistory = new fr.esrf.tangoatk.widget.util.ErrorHistory();	
	   	
	   	// addErrorListener must be done before registering to the attributes or commands
		attl.addErrorListener(errorHistory);
		attl.addSetErrorListener(errorHistory);
		attl.addSetErrorListener(fr.esrf.tangoatk.widget.util.ErrorPopup.getInstance());
		

		initComponents();
        ATKGraphicsUtils.centerFrameOnScreen(this);
        
	}

	public void initComponents()
	{				
		try {
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/4a/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/4b/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/5a/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/5b/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/6a/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/6b/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/7a/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/7b/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/8a/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/8b/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/9a/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/9b/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/10a/Temperature");
			bearing_temp_Att = (INumberScalar)attl.add("infra/hqps-bearing-temp/10b/Temperature");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/1/Temperature");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/2/Temperature");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/3/Temperature");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/4/Temperature");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/5/Temperature");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/1/DeltaPressure");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/2/DeltaPressure");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/3/DeltaPressure");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/4/DeltaPressure");
			cell_temp_Att = (INumberScalar)attl2.add("infra/hqps-cell/5/DeltaPressure");
		}
		catch(ConnectionException e){
			javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
	}
	
		
	bearing_temp_viewer.setModel(attl);
	bearing_temp_viewer.setBackground(java.awt.Color.WHITE);
	cell_temp_viewer.setModel(attl2);
	cell_temp_viewer.setBackground(java.awt.Color.WHITE);
	//cell_temp_viewer.setForeground(java.awt.Color.WHITE);
	attl.startRefresher();
	attl2.startRefresher();

	cell_title = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
	cell_title.setText("Cells pressure and temperatures");
	bearing_title = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
	bearing_title.setText("Bearings temperatures");

	menu_bar 			= new javax.swing.JMenuBar();
    error_item			= new javax.swing.JMenuItem("Error History");
    view_menu			= new javax.swing.JMenu("View");
	error_item.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
           error_itemActionPerformed(evt);
        }
		private void error_itemActionPerformed(ActionEvent evt) {
			errorHistory.setVisible(true);
		}
	});
  
    view_menu.add(error_item);
    menu_bar.add(view_menu);
 	this.setJMenuBar(menu_bar);
	setTitle("Temperature window");
	setLayout(new java.awt.GridBagLayout());
	setBackground(java.awt.Color.WHITE);
	java.awt.GridBagConstraints constraints = new java.awt.GridBagConstraints();
	javax.swing.JPanel center = new javax.swing.JPanel();
	center.setLayout(new java.awt.GridBagLayout());
	constraints.fill = java.awt.GridBagConstraints.BOTH;
	constraints.gridx = 1;
	constraints.gridy = 2;
	center.add(bearing_temp_viewer, constraints);
	constraints.gridy = 1;
	center.add(bearing_title, constraints);
	constraints.gridx = 2;
	constraints.gridy = 2;
	center.add(cell_temp_viewer, constraints);
	constraints.gridy = 1;
	center.add(cell_title, constraints);
	getContentPane().add(center);
	pack();
	}	
	
	fr.esrf.tangoatk.widget.util.JSmoothLabel cell_title;
	fr.esrf.tangoatk.widget.util.JSmoothLabel bearing_title;
	
	fr.esrf.tangoatk.widget.util.ErrorHistory errorHistory;
	javax.swing.JMenuBar menu_bar;
	javax.swing.JMenu view_menu;	
	javax.swing.JMenuItem error_item;
}