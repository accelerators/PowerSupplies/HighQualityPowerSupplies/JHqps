//+======================================================================
// $Source:  $
//
// Project:   Tango
//
// Description:  java source code for main swing class.
//
// $Author: verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2009
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Revision:  $
//
// $Log:  $
//
//-======================================================================

package jhqps.hqps;

import fr.esrf.tangoatk.widget.util.JSmoothProgressBar;
import fr.esrf.tangoatk.widget.util.Splash;

import javax.swing.*;
import java.awt.*;


//===============================================================
/**
 * This class is a singleton and manage some splash utilities.
 *
 * @author Pascal Verdier
 */
//===============================================================

@SuppressWarnings("WeakerAccess")
public class SplashUtils {
    private static Splash splash = null;
    private static int splashProgress = 0;
    private static boolean splashActive = false;

    private static SplashUtils instance = new SplashUtils();
    private static final String imageFile = "/jhqps/rotabloc.jpg";
    //=======================================================
    //=======================================================
    public static SplashUtils getInstance() {
        return instance;
    }
    //===============================================================
    //===============================================================
    public String getApplicationRelease() {
        String applicationName = getClass().getPackage().getImplementationTitle();
        if (applicationName == null)
            applicationName = "JHqps";

        String release = getClass().getPackage().getImplementationVersion();
        if (release!=null)
            return applicationName + "-" + release;
        else
            return applicationName + " - not released";
    }
    //=======================================================
    //=======================================================
    public void startSplash() {
        if (splash!=null) {
            splash.setVisible(false);
        }
        //	Create a splash window.
        JSmoothProgressBar myBar = new JSmoothProgressBar();
        myBar.setStringPainted(true);
        myBar.setBackground(Color.lightGray);
        myBar.setProgressBarColors(Color.gray, Color.white, Color.white);

        splash = new Splash(new ImageIcon(getClass().getResource(imageFile)), new Color(20, 0, 0), myBar);
        splash.setTitle(getApplicationRelease());
        splash.setCopyright("ESRF, Grenoble, France");
        splash.setMessage("Tango initialization...");
        splash.setVisible(true);
        splash.setForeground(Color.lightGray);
        splashProgress = 0;
    }
    //=======================================================
    //=======================================================
    public void increaseSplashProgress(int i, String message) {
        if (splash == null)
            return;
        splashProgress += i;
        if (splashProgress> 98)
            splashProgress = 1;
        splash.progress(splashProgress);
        if (message!=null)
            splash.setMessage(message);

        //System.out.println(splashProgress);
    }
    //=======================================================
    //=======================================================
    public void setSplashProgress(int i) {
        setSplashProgress(i, null);
    }
    //=======================================================
    //=======================================================
    public void setSplashProgress(int i, String message) {
        if (splash == null)
            return;
        splashProgress = i;
        if (splashProgress> 98)
            splashProgress = 98;
        splash.progress(splashProgress);
        if (message!=null)
            splash.setMessage(message);

        //System.out.println(splashProgress + " - " + message);
    }
    //=======================================================
    //=======================================================
    public void stopSplash() {
        if (splash != null) {
            splashProgress = 100;
            splash.progress(splashProgress);
            splash.setVisible(false);
        }
    }
    //=======================================================
    //=======================================================
    @SuppressWarnings("unused")
    public boolean getSplashActive() {
        return splashActive;
    }
    //=======================================================
    //=======================================================
    @SuppressWarnings("unused")
    public void setSplashActive(boolean b) {
        splashActive = b;
    }
    //=======================================================
    //=======================================================
    @SuppressWarnings("unused")
    public void startAutoUpdate() {
        new UpdateThread().start();
    }
    //=======================================================
    //=======================================================


    //=======================================================
    //=======================================================
    private class UpdateThread extends Thread {
        public void run() {
            while (splash.isVisible()) {
                increaseSplashProgress(1, null);
                try { Thread.sleep(500); } catch (InterruptedException e) { /* */ }
            }
         }
    }
    //=======================================================
    //=======================================================
}
