//+======================================================================
//$Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsMainFrame.java,v $

//Project:   JHqps

//Description:  HQPS java application for CTRM. .

//$Author: goudard $

//$Revision: 1.55 $

//$Log: JHqpsMainFrame.java,v $
//Revision 1.55  2009/08/25 13:53:22  goudard
//Modifications of HqpsBinIn and HqpsBinOut reading.
//Added :
// - Incoherent state
// - wago uninitialized state
// - conditions to normal and economy mode detection.
//
//Modification of ElecDropStat : isolation curve added.
//
//Revision 1.54  2009/08/25 11:39:18  goudard
//Change behaviour when wago plc is unreachable (network problem)
//
//Revision 1.53  2009/08/18 11:33:50  goudard
//File JHqpsParameters.java added to CVS
//elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
//Revision 1.51  2009/06/15 13:43:01  goudard
//Modifications in
//	- state management of HQPSGlobal state button.
//	- FFT for jhqps.elecdrops.
//	- Main Parameters window.
//
//Revision 1.4  2009/01/30 15:26:49  goudard
//In drop appli :
// - Correct -1 in DropDisplay.
//In vibration History :
// - Gradient correct setup
// - Correct scale between 9.81mg and maximum
//Check HqpsBinIn to display Hqps mode.
//
//Revision 1.3  2009/01/28 13:11:14  goudard
//Added cvs header.


//Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//All Rights Reversed
//-======================================================================
package jhqps.hqps;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer;
import fr.esrf.tangoatk.widget.attribute.StateViewer;
import fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer;
import fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer;
import fr.esrf.tangoatk.widget.jdraw.SynopticProgressListener;
import fr.esrf.tangoatk.widget.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.util.MissingResourceException;
import mas_gui.commons.MasException;
import mas_gui.gui.MainsAnalysersFrame;

/**
 * @author : O. Goudard
 */

public class JHqpsMainFrame extends JFrame implements JHqpsDefs, SynopticProgressListener, ActionListener {

    public java.awt.Dimension dimension;
    //private Splash splash;
    private int prg = 0;


    public JHqpsMainFrame() {
        SplashUtils.getInstance().startSplash();

        AttributeList attributeList = new AttributeList();
        errorHistory = new fr.esrf.tangoatk.widget.util.ErrorHistory();
        CommandList commandList = new CommandList();
        IDevStateScalar stateAtt = null;
        INumberSpectrum specAtt = null;
        ICommand initCmd = null;

        // addErrorListener must be done before registering to the attributes or commands
        attributeList.addErrorListener(errorHistory);
        attributeList.addSetErrorListener(errorHistory);
        attributeList.addSetErrorListener(fr.esrf.tangoatk.widget.util.ErrorPopup.getInstance());
        commandList.addErrorListener(errorHistory);
        commandList.addSetErrorListener(errorHistory);
        commandList.addSetErrorListener(fr.esrf.tangoatk.widget.util.ErrorPopup.getInstance());

        try {
            stateAtt = (IDevStateScalar) attributeList.add("infra/hqps/1/state");
            specAtt = (INumberSpectrum) attributeList.add("infra/hqps-common/master/InputVoltage");
            HqpsGlobStatePanel = DeviceFactory.getInstance().getDevice("infra/hqps/1");
            SplashUtils.getInstance().setSplashProgress(++prg);
            
            initCmd = (ICommand) commandList.add("infra/hqps-common/master/Init");
            SplashUtils.getInstance().setSplashProgress(++prg);
            
        } catch (ConnectionException e) {
            e.printStackTrace();
            SplashUtils.getInstance().stopSplash();
            javax.swing.JOptionPane.showMessageDialog(null, "JHqps : Can't connect the device\n"
                            + e.getDescription()
                            + "\nGUI will abort.\n", "Failed to connect the attributes",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        
        // Connect Functioning mode selection devices
        try {

            // Imports of binary output of WAGO PLC (copied from SAIA PLC)
            economyOutProxy = new DeviceProxy("infra/hqps-economy/ctrm");
            normalOutProxy = new DeviceProxy("infra/hqps-normal/ctrm");
            // Imports binary input of SAIA PLC
            normalInProxy = new DeviceProxy("infra/hqps-normal/master-plc");
            economyInProxy = new DeviceProxy("infra/hqps-economy/master-plc");
            SplashUtils.getInstance().setSplashProgress(++prg);

        } catch (DevFailed e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SplashUtils.getInstance().setSplashProgress(++prg, "Graphical component creation...");
        initComponents();

        SplashUtils.getInstance().setSplashProgress(++prg, "Attribute connection...");

        globState.setModel(HqpsGlobStatePanel);
        stateViewer.setModel(stateAtt);
        SplashUtils.getInstance().setSplashProgress(100);
        numberSpectrumViewer.setModel(specAtt);
        voidVoidCommandViewer.setModel(initCmd);
        attributeList.startRefresher();
        SplashUtils.getInstance().stopSplash();
        fr.esrf.tangoatk.widget.util.ATKGraphicsUtils.centerFrameOnScreen(this);
    }

    @Override
    public void progress(double d) {
        double range = (double) (100 - prg);
        int pg = prg + (int) (d * range);
        SplashUtils.getInstance().setSplashProgress(pg);
    }

    public void initComponents() {
        stateViewer = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        numberSpectrumViewer = new fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer();
        voidVoidCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        SynopticFileViewer mainSynoptic = new SynopticFileViewer();
        menu_bar = new javax.swing.JMenuBar();
        file_menu = new javax.swing.JMenu("File ");
        help_menu = new javax.swing.JMenu("Help");
        changeLog_menu = new javax.swing.JMenuItem("Changelog");
        appli_menu = new javax.swing.JMenu("Applications");
        view_menu = new javax.swing.JMenu("View");
        error_item = new javax.swing.JMenuItem("Error History");
        history_item = new javax.swing.JMenuItem("HQPS History");
        diag_item = new javax.swing.JMenuItem("Diagnostic");
        quit_item = new javax.swing.JMenuItem("Exit");
        drop_menu = new javax.swing.JMenuItem("Mains Analysis");
        expert_menu = new javax.swing.JMenu("Expert Applications");
        expert_item = new javax.swing.JMenuItem("Expert View");
        detail_item = new javax.swing.JMenuItem("Rotabloc Data");
        vibration_item = new javax.swing.JMenu("Vibrations");
        temp_menu = new javax.swing.JMenu("Temperatures");
        temp_item = new javax.swing.JMenuItem("Live Temperatures");
        temp_histo_item = new javax.swing.JMenuItem("Temperatures History");
        ecoBtn = new javax.swing.JButton("Economy");
        normalBtn = new javax.swing.JButton("Normal");
        JButton shavingBtn = new JButton("Shaving");
        globState = new fr.esrf.tangoatk.widget.device.SimpleStateViewer();
        vibration_histo_item = new javax.swing.JMenuItem("Vibrations History");
        vibration_live_item = new javax.swing.JMenuItem("Live Vibrations");
        //GCC_item					= new javax.swing.JMenuItem("Diesel Genset");
        hqps_param_item = new javax.swing.JMenuItem("HQPS Power");


        java.io.InputStream jdFileInStream = this.getClass().getResourceAsStream("/jhqps/jdraw_files/Main_HQPS2.jdw");
        if (jdFileInStream == null) {
            SplashUtils.getInstance().stopSplash();
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Failed to get the inputStream for the synoptic file resource : /jhqps/jdraw_files/Main_HQPS2.jdw \n\n",
                    "Resource error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        java.io.InputStreamReader inStrReader = new java.io.InputStreamReader(jdFileInStream);
        try {
            mainSynoptic.setProgressListener(this);
            mainSynoptic.loadSynopticFromStream(inStrReader);
            mainSynoptic.setToolTipMode(fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler.TOOL_TIP_NAME);
        } catch (java.io.IOException ioex) {
            SplashUtils.getInstance().stopSplash();
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Cannot find load the synoptic input stream reader.\n"
                            + " Application will abort ...\n"
                            + ioex,
                    "Resource access failed",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        } catch (MissingResourceException mrEx) {
            SplashUtils.getInstance().stopSplash();
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Cannot parse the synoptic file : Main_HQPS2.jdw.\n"
                            + " Application will abort ...\n"
                            + mrEx,
                    "Cannot parse the file",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        } catch (Exception e) {
            SplashUtils.getInstance().stopSplash();
            System.out.println("exception : " + e.getMessage() + e.toString());
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Null pointer exception"
                            + e.toString(),
                    e.getMessage(),
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }

        create_menu();

        //Set up South Panel
        javax.swing.JPanel SouthPl = new javax.swing.JPanel();
        SouthPl.setLayout(new java.awt.BorderLayout());
        Date_time = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
        Date_time.setBackground(new java.awt.Color(230, 230, 230));
        Date_time.setText(new java.util.Date().toString());
        SouthPl.add(Date_time, java.awt.BorderLayout.EAST);
        SouthPl.add(globState, java.awt.BorderLayout.CENTER);
        globState.setText("HQPS Global State");
        globState.setBorder(new javax.swing.border.EtchedBorder(new java.awt.Color(204, 204, 204), java.awt.Color.gray));
        globState.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        globState.setHorizontalAlignment(fr.esrf.tangoatk.widget.util.JSmoothLabel.CENTER_ALIGNMENT);
        globState.setSizingBehavior(fr.esrf.tangoatk.widget.util.JSmoothLabel.MATRIX_BEHAVIOR);

        javax.swing.JPanel SouthWestPl = new javax.swing.JPanel();
        SouthWestPl.setLayout(new java.awt.FlowLayout());
        shavingBtn.setEnabled(false);
        SouthWestPl.add(ecoBtn);
        SouthWestPl.add(normalBtn);
        SouthWestPl.add(shavingBtn);
        // By default, set enable to false
        ecoBtn.setEnabled(false);
        normalBtn.setEnabled(false);

        read_mode();

        SouthPl.add(SouthWestPl, java.awt.BorderLayout.WEST);

        // Start Date & Time thread.
        scan();

        // Manage window events.
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        java.awt.GridBagConstraints constraints = new java.awt.GridBagConstraints();
        setTitle(SplashUtils.getInstance().getApplicationRelease());
        setLayout(new java.awt.BorderLayout());
        javax.swing.JPanel center = new javax.swing.JPanel();
        center.setLayout(new java.awt.GridBagLayout());
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.fill = java.awt.GridBagConstraints.BOTH;
        center.add(mainSynoptic, constraints);
        setJMenuBar(menu_bar);

        getContentPane().add(center, java.awt.BorderLayout.CENTER);
        getContentPane().add(SouthPl, java.awt.BorderLayout.SOUTH);
        pack();
        dimension = this.getSize();
    }
    /*-------------------------------------------------------------------------*/
    /**
     * UpdateFields() method : Updates current labels and FFT charts.
     *
     * @param time : Date and time
     */
    /*-------------------------------------------------------------------------*/
    public static void UpdateFields(String time) {
        try {
            Date_time.setText(time);
            read_mode();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*-------------------------------------------------------------------------*/
    /**
     * create_menu() method : create menu bar and associated actions.
     */
    /*-------------------------------------------------------------------------*/
    private void create_menu() {
        // Menu bar setup
        view_menu.add(history_item);
        view_menu.add(new javax.swing.JSeparator());
        view_menu.add(error_item);
        view_menu.add(diag_item);
        menu_bar.add(file_menu);
        menu_bar.add(view_menu);
        menu_bar.add(appli_menu);
        menu_bar.add(help_menu);
        file_menu.add(quit_item);
        expert_menu.add(expert_item);
        expert_menu.add(detail_item);
        //	expert_menu.add(GCC_item);
        appli_menu.add(hqps_param_item);
        appli_menu.add(drop_menu);
        appli_menu.add(expert_menu);
        appli_menu.add(vibration_item);
        appli_menu.add(temp_menu);
        vibration_item.add(vibration_histo_item);
        vibration_item.add(vibration_live_item);
        temp_menu.add(temp_item);
        temp_menu.add(temp_histo_item);
        help_menu.add(changeLog_menu);

        //	Set accelerators and mnemonics in the menu
        history_item.setAccelerator(javax.swing.KeyStroke.getKeyStroke('H', MouseEvent.CTRL_MASK));
        history_item.setMnemonic('H');
        quit_item.setAccelerator(javax.swing.KeyStroke.getKeyStroke('X', MouseEvent.CTRL_MASK));
        quit_item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
        quit_item.setMnemonic('x');
        vibration_live_item.setAccelerator(javax.swing.KeyStroke.getKeyStroke('V', MouseEvent.CTRL_MASK));
        vibration_live_item.setMnemonic('v');
        temp_item.setAccelerator(javax.swing.KeyStroke.getKeyStroke('T', MouseEvent.CTRL_MASK));
        temp_item.setMnemonic('t');

        history_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                history_item_itemActionPerformed();
            }
        });
        hqps_param_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hqps_param_itemActionPerformed();
            }
        });
        temp_histo_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temp_histo_itemActionPerformed();
            }
        });

        quit_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                System.exit(0);
            }
        });

        expert_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expert_itemActionPerformed();

            }
        });
        vibration_histo_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vibration_histo_item_itemActionPerformed();

            }
        });
        detail_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detail_itemActionPerformed();

            }
        });

        error_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                error_itemActionPerformed();
            }
        });
        diag_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diag_itemActionPerformed();
            }
        });
        
        drop_menu.addActionListener(this);
        
        changeLog_menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    Desktop.getDesktop().browse(new URI("https://gitlab.esrf.fr/accelerators/PowerSupplies/HighQualityPowerSupplies/JHqps/-/blob/master/CHANGELOG.md"));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(rootPane, ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        
        vibration_live_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vibration_live_itemActionPerformed();
            }
        });
        temp_item.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temp_itemActionPerformed();
            }
        });
        ecoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ecoBtnActionPerformed();
            }
        });
        normalBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                normalBtnActionPerformed();
            }
        });
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            MainsAnalysersFrame masFrame = new MainsAnalysersFrame(false);
            masFrame.setLocationRelativeTo(this);
            masFrame.setVisible(true);

        } catch (MasException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage());
        }
    }

    protected void temp_itemActionPerformed() {
        new JHqpsTemp().setVisible(true);

    }

    private void error_itemActionPerformed() {
        errorHistory.setVisible(true);
    }

    private void ecoBtnActionPerformed() {
        int n = JOptionPane.showConfirmDialog(null,
                "Do you really want to switch HQPS to economy mode ?\n" +
                        "Accumulated energy will be adapted to the load.",
                "Confirmation dialog",
                JOptionPane.YES_NO_OPTION);

        if (n == JOptionPane.YES_OPTION) {
            try {
                economyOutProxy.command_inout("Close");
                normalOutProxy.command_inout("Open");
                ecoBtn.setEnabled(false);
                normalBtn.setEnabled(true);
                globState.setText("HQPS in economy mode");
            } catch (DevFailed e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (n == JOptionPane.NO_OPTION) {
            System.out.println("Operation aborted.");
        }
    }

    private void normalBtnActionPerformed() {
        int n = JOptionPane.showConfirmDialog(null,
                "Do you really want to switch HQPS to normal mode ?\n" +
                        "Accumulated energy will be about 8 MJ per connected rotabloc",
                "Confirmation dialog",
                JOptionPane.YES_NO_OPTION);
        if (n == JOptionPane.YES_OPTION) {
            try {
                normalOutProxy.command_inout("Close");
                economyOutProxy.command_inout("Open");
                ecoBtn.setEnabled(true);
                normalBtn.setEnabled(false);
                globState.setText("HQPS in normal mode");
            } catch (DevFailed e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (n == JOptionPane.NO_OPTION) {
            System.out.println("Operation aborted.");
        }
    }

    private void expert_itemActionPerformed() {
        new SynopticViewer("Expert HQPS2,/jhqps/jdraw_files/Expert_HQPS2.jdw");
    }

    private void hqps_param_itemActionPerformed() {
        new JHqpsParameters(this, true);
    }

    private void history_item_itemActionPerformed() {
        JHqpsTicket histo = new JHqpsTicket();
        histo.setVisible(true);
    }

    private void temp_histo_itemActionPerformed() {
        javax.swing.JOptionPane.showMessageDialog(null,
                "Unable to open temperature history",
                "Not Yet implemented.",
                javax.swing.JOptionPane.INFORMATION_MESSAGE);

    }

    private void detail_itemActionPerformed() {
        //TODO : detail view.
        javax.swing.JOptionPane.showMessageDialog(null,
                "Unable to open rotabloc details",
                "Not Yet implemented.",
                javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }

    //TODO add a viewer of accu speed history.
    private void diag_itemActionPerformed() {
        new fr.esrf.tangoatk.widget.util.ATKDiagnostic("");
    }

    private void vibration_live_itemActionPerformed() {
        JHqpsVibration Vib = new JHqpsVibration(this, false);
        Vib.setVisible(true);
    }

    private void vibration_histo_item_itemActionPerformed() {
        Object[] possibilities = {
                "infra/hqps-vib/4a/vibrationFFT",
                "infra/hqps-vib/5a/vibrationFFT",
                "infra/hqps-vib/6a/vibrationFFT",
                "infra/hqps-vib/7a/vibrationFFT",
                "infra/hqps-vib/8a/vibrationFFT",
                "infra/hqps-vib/9a/vibrationFFT",
                "infra/hqps-vib/10a/vibrationFFT",
                "infra/hqps-vib/4b/vibrationFFT",
                "infra/hqps-vib/5b/vibrationFFT",
                "infra/hqps-vib/6b/vibrationFFT",
                "infra/hqps-vib/7b/vibrationFFT",
                "infra/hqps-vib/8b/vibrationFFT",
                "infra/hqps-vib/9b/vibrationFFT",
                "infra/hqps-vib/10b/vibrationFFT",
        };
        Object message = "Select device";
        String inputValue = (String) javax.swing.JOptionPane.showInputDialog(this,
                message,
                "Device name ?",
                javax.swing.JOptionPane.QUESTION_MESSAGE,
                null,
                possibilities,
                possibilities[0]);
        try {
            if (inputValue != null) {
                JHqpsFFTDialog historic_fft = new JHqpsFFTDialog(this, inputValue, false);
                historic_fft.setSize(1200, 666);
                historic_fft.setLocation(this.getLocation());
                historic_fft.setVisible(true);
            }
        } catch (DevFailed e) {
            e.printStackTrace();
        }
    }

    private static DevState readModeState(DeviceProxy proxy) throws DevFailed {
        return proxy.read_attribute("State").extractState();
    }
    public static void read_mode() {
        // read actual mode
        try {
            DevState economyOutState = readModeState(economyOutProxy);
            DevState economyInState = readModeState(economyInProxy);
            DevState normalOutState = readModeState(normalOutProxy);
            DevState normalInState = readModeState(normalInProxy);

            // case where one of the tango servers doesn't respond.
            if (economyOutState == DevState.UNKNOWN ||
                    normalOutState == DevState.UNKNOWN ||
                    economyInState == DevState.UNKNOWN ||
                    normalInState == DevState.UNKNOWN) {
                normalBtn.setEnabled(true);
                ecoBtn.setEnabled(true);
                globState.setText(" Tango server problem");
                return;
            }
            //HQPS in economy mode
            if (normalInState == DevState.OPEN &&
                    economyInState == DevState.CLOSE &&
                    normalOutState == DevState.OPEN &&
                    economyOutState == DevState.CLOSE) {
                normalBtn.setEnabled(true);
                ecoBtn.setEnabled(false);
                globState.setFont(new java.awt.Font("courrier", Font.BOLD, 14));
                globState.setText("HQPS : economy mode");
                return;
            }
            // HQPS in normal mode
            if (normalInState == DevState.CLOSE &&
                    economyInState == DevState.OPEN &&
                    normalOutState == DevState.CLOSE &&
                    economyOutState == DevState.OPEN) {
                normalBtn.setEnabled(false);
                ecoBtn.setEnabled(true);
                globState.setFont(new java.awt.Font("courrier", Font.BOLD, 14));
                globState.setText("HQPS : normal mode");
                return;
            }
            // Strange case that should never append.
            if (normalOutState == DevState.OPEN &&
                    economyOutState == DevState.OPEN) {
                normalBtn.setEnabled(true);
                ecoBtn.setEnabled(true);
                globState.setText("WAGO PLC uninitialized");
                return;
            }
            // Strange case that should never append.
            if (normalOutState == DevState.CLOSE &&
                    economyOutState == DevState.CLOSE) {
                normalBtn.setEnabled(true);
                ecoBtn.setEnabled(true);
                globState.setText("WAGO PLC bad state");
                return;
            }
            // Not coherent between inputs and outputs
            if (economyOutState == DevState.OPEN &&
                    normalInState == DevState.OPEN &&
                    economyInState == DevState.CLOSE) {
                normalBtn.setEnabled(false);
                ecoBtn.setEnabled(false);
                globState.setText("WAGO incoherent state 1");
                return;
            }
            // Not coherent between inputs and outputs
            if (economyOutState == DevState.CLOSE &&
                    normalInState == DevState.CLOSE &&
                    economyInState == DevState.OPEN) {
                normalBtn.setEnabled(false);
                ecoBtn.setEnabled(false);
                globState.setText("WAGO incoherent state 2");
            }
        } catch (fr.esrf.TangoApi.CommunicationTimeout e) {
            normalBtn.setEnabled(false);
            ecoBtn.setEnabled(false);
            globState.setText("WAGO PLC unreachable");

        } catch (DevFailed e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            //ErrorPane.showErrorMessage(new JFrame(), null, e);
            normalBtn.setEnabled(false);
            ecoBtn.setEnabled(false);
            globState.setFont(new Font("courrier", Font.PLAIN, 10));
            globState.setText("WAGO PLC unreachable");
        }

    }
    /*-------------------------------------------------------------------------*/
    /**
     * scan() method : start the data collection thread.
     */
    /*-------------------------------------------------------------------------*/
    public void scan() {
        JHqpsThread scanThread = new JHqpsThread();
        scanThread.start();
    }

    public static void main(String args[]) {
        new JHqpsMainFrame().setVisible(true);
    }

    private StateViewer stateViewer;
    private NumberSpectrumViewer numberSpectrumViewer;
    private VoidVoidCommandViewer voidVoidCommandViewer;
    private ErrorHistory errorHistory;
    private static JSmoothLabel Date_time;
    private JMenuBar menu_bar;
    private JMenu file_menu;
    private JMenu help_menu;
    private JMenuItem changeLog_menu;
    private JMenu appli_menu;
    private JMenu view_menu;
    private JMenuItem quit_item;
    private JMenuItem error_item;
    private JMenuItem history_item;
    private JMenuItem diag_item;
    private JMenuItem drop_menu;
    private JMenu expert_menu;
    private JMenuItem expert_item;
    private JMenuItem detail_item;
    private JMenuItem hqps_param_item;
    private JMenu vibration_item;
    private JMenuItem vibration_histo_item;
    private JMenuItem vibration_live_item;
    private JMenuItem temp_item;
    private JMenu temp_menu;
    private JMenuItem temp_histo_item;
    private static javax.swing.JButton ecoBtn;
    private static javax.swing.JButton normalBtn;
    private static fr.esrf.tangoatk.widget.device.SimpleStateViewer globState;
    private fr.esrf.tangoatk.core.Device HqpsGlobStatePanel;
    private static DeviceProxy economyOutProxy;
    private static DeviceProxy normalOutProxy;
    private static DeviceProxy normalInProxy;
    private static DeviceProxy economyInProxy;
}
