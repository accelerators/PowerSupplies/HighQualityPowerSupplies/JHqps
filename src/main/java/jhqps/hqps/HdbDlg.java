package jhqps.hqps;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import org.tango.jhdb.Hdb;
import org.tango.jhdb.HdbFailed;
import org.tango.jhdb.data.HdbData;
import org.tango.jhdb.data.HdbDataSet;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Allow to select what we want to retrieve from HDB
 */
class HdbSelectionDlg extends JDialog implements JHqpsDefs, ActionListener {

    final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private static String lastStartDate = "";
    private static String lastStopDate = "";
    private static boolean[] lastSelection = new boolean[virtualPrinterDevices.length];

    static {
        for (int i = 0 ; i<virtualPrinterDevices.length ; i++)
            lastSelection[i] = false;
    }

    private JComboBox<String> timeCombo;
    private JTextField startDateText;
    private JTextField stopDateText;
    private JButton performSearchBtn;
    private JButton dismissBtn;
    private JButton selectAllBtn;
    private JCheckBox[] selectedPrinters;
    boolean okFlag;

    HdbSelectionDlg(JFrame parent) {

        super(parent, true);
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BorderLayout());


        JPanel vpPanel = new JPanel();
        vpPanel.setLayout(new GridLayout(virtualPrinterDevices.length, 1));
        vpPanel.setBorder(BorderFactory.createEtchedBorder());

        selectedPrinters = new JCheckBox[virtualPrinterDevices.length];
        for (int i = 0 ; i<virtualPrinterDevices.length ; i++) {
            selectedPrinters[i] = new JCheckBox(virtualPrinterLabels[i]);
            selectedPrinters[i].setSelected(lastSelection[i]);
            vpPanel.add(selectedPrinters[i]);
        }

        innerPanel.add(vpPanel, BorderLayout.WEST);

        JPanel configPanel = new JPanel();
        configPanel.setLayout(null);
        configPanel.setPreferredSize(new Dimension(320, 0));
        innerPanel.add(configPanel, BorderLayout.SOUTH);

        timeCombo = new JComboBox<>();
        timeCombo.addItem("Last 4 Hours");
        timeCombo.addItem("Last 8 Hours");
        timeCombo.addItem("Last 24 Hours");
        timeCombo.addItem("Last 3 days");
        timeCombo.addItem("Last week");
        timeCombo.addItem("Last month");
        timeCombo.setBounds(10, 10, 300, 25);
        timeCombo.addActionListener(this);
        configPanel.add(timeCombo);

        JLabel startDateLabel = new JLabel("Start date");
        startDateLabel.setBounds(10, 40, 100, 25);
        configPanel.add(startDateLabel);
        startDateText = new JTextField(lastStartDate);
        startDateText.setEditable(true);
        startDateText.setBounds(110, 40, 200, 25);
        configPanel.add(startDateText);

        JLabel stopDateLabel = new JLabel("Stop date");
        stopDateLabel.setBounds(10, 70, 100, 25);
        configPanel.add(stopDateLabel);
        stopDateText = new JTextField(lastStopDate);
        stopDateText.setEditable(true);
        stopDateText.setBounds(110, 70, 200, 25);
        configPanel.add(stopDateText);

        selectAllBtn = new JButton("Select All");
        selectAllBtn.addActionListener(this);
        selectAllBtn.setBounds(10, 100, 150, 25);
        configPanel.add(selectAllBtn);

        innerPanel.add(configPanel, BorderLayout.CENTER);

        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        performSearchBtn = new JButton("Perform search");
        performSearchBtn.addActionListener(this);
        btnPanel.add(performSearchBtn);

        dismissBtn = new JButton("Dismiss");
        dismissBtn.addActionListener(this);
        btnPanel.add(dismissBtn);

        innerPanel.add(btnPanel, BorderLayout.SOUTH);

        setContentPane(innerPanel);
        setTitle("HDB selection");

    }

    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == timeCombo) {

            long now = System.currentTimeMillis();

            switch (timeCombo.getSelectedIndex()) {
                case 0: //Last 4 Hours
                {
                    Date dstop = new java.util.Date(now);
                    stopDateText.setText(dateFormat.format(dstop));
                    Date dstart = new java.util.Date(now - 14400000L);
                    startDateText.setText(dateFormat.format(dstart));
                }
                break;
                case 1: //Last 8 Hours
                {
                    Date dstop = new java.util.Date(now);
                    stopDateText.setText(dateFormat.format(dstop));
                    Date dstart = new java.util.Date(now - 28800000L);
                    startDateText.setText(dateFormat.format(dstart));
                }
                break;
                case 2: //Last 24 Hours
                {
                    Date dstop = new java.util.Date(now);
                    stopDateText.setText(dateFormat.format(dstop));
                    Date dstart = new java.util.Date(now - 86400000L);
                    startDateText.setText(dateFormat.format(dstart));
                }
                break;
                case 3: //Last 3 days
                {
                    Date dstop = new java.util.Date(now);
                    stopDateText.setText(dateFormat.format(dstop));
                    Date dstart = new java.util.Date(now - 259200000L);
                    startDateText.setText(dateFormat.format(dstart));
                }
                break;
                case 4: // Last week
                {
                    Date dstop = new java.util.Date(now);
                    stopDateText.setText(dateFormat.format(dstop));
                    Date dstart = new java.util.Date(now - 604800000L);
                    startDateText.setText(dateFormat.format(dstart));
                }
                break;
                case 5: // Last month
                {
                    Date dstop = new java.util.Date(now);
                    stopDateText.setText(dateFormat.format(dstop));
                    Date dstart = new java.util.Date(now - 2592000000L);
                    startDateText.setText(dateFormat.format(dstart));
                }
                break;
            }

        } else if (src == dismissBtn) {

            updateLastSelection();
            okFlag = false;
            setVisible(false);

        } else if (src == performSearchBtn) {

            updateLastSelection();
            okFlag = true;
            setVisible(false);

        } else if (src == selectAllBtn) {

            for (int i = 0 ; i<virtualPrinterDevices.length ; i++)
                selectedPrinters[i].setSelected(true);

        }

    }

    private void updateLastSelection() {

        lastStartDate = startDateText.getText();
        lastStopDate = stopDateText.getText();
        for (int i = 0 ; i<virtualPrinterDevices.length ; i++)
            lastSelection[i] = selectedPrinters[i].isSelected();

    }

    String getStartDate() {
        return startDateText.getText();
    }

    String getStopDate() {
        return stopDateText.getText();
    }

    boolean isVpSelected(int idx) {
        return selectedPrinters[idx].isSelected();
    }

}

class HdbItem {
    HdbItem(String value, int idx) {
        this.value = value;
        this.idx = idx;
    }
    String value;
    int idx;
}

class HdbLine {

    HdbLine(long date) {
        this.date = date;
        items = new ArrayList<>();
    }

    long date;
    List<HdbItem> items;

    void addItem(HdbItem item) {

        boolean found = false;
        int i = 0;
        while (!found && i<items.size()) {
            found = item.idx == items.get(i).idx;
            if (!found) i++;
        }

        if (found) {
            // Two event of the same vp at the same date
            items.get(i).value =
                    item.value + "\n---------------------------------\n" + items.get(i).value;
        } else {
            items.add(item);
        }

    }

    int getPosForIdx(int idx) {
        boolean found = false;
        int i = 0;
        while (!found && i<items.size()) {
            found = items.get(i).idx == idx;
            if (!found) i++;
        }

        if (!found)
            return -1;
        else
            return i;
    }
}


class HdbDlg extends JFrame implements JHqpsDefs, ActionListener {

    private static String lastDir = ".";
    private final static SimpleDateFormat datefr = new SimpleDateFormat("dd/MM/yyyy");
    private final static SimpleDateFormat timefr = new SimpleDateFormat("HH:mm:ss");

    private String startDate;
    private String stopDate;
    private List<String> sigNames;
    private List<String> sigLabels;
    private List<HdbLine> hdbData;
    private DefaultTableModel dm;
    private JTable theTable;
    private MultiLineCellEditor editor;
    private JButton dismissBtn;
    private JButton copyBtn;
    private JButton saveBtn;
    private boolean errorFlag;

    private HdbDlg(String startDate, String stopDate, List<String> sigNames, List<String> sigLabels) throws DevFailed {

        errorFlag = false;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.sigNames = sigNames;
        this.sigLabels = sigLabels;

        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new BorderLayout());
        setContentPane(innerPanel);

        // Table model
        dm = new DefaultTableModel() {

            public Class getColumnClass(int columnIndex) {
                return String.class;
            }

            public boolean isCellEditable(int row, int column) {
                return false;
            }

            public void setValueAt(Object aValue, int row, int column) {
            }

        };

        // Table initialisation
        theTable = new JTable(dm);
        theTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        MultiLineCellRenderer renderer = new MultiLineCellRenderer();
        theTable.setDefaultRenderer(String.class, renderer);

        editor = new MultiLineCellEditor(theTable);
        theTable.setDefaultEditor(String.class, editor);

        JScrollPane scrollPane = new JScrollPane(theTable);
        scrollPane.setPreferredSize(new Dimension(500, 640));
        innerPanel.add(scrollPane, BorderLayout.CENTER);

        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        innerPanel.add(btnPanel, BorderLayout.SOUTH);

        saveBtn = new JButton("Save");
        saveBtn.addActionListener(this);
        btnPanel.add(saveBtn);

        copyBtn = new JButton("Copy");
        copyBtn.addActionListener(this);
        btnPanel.add(copyBtn);

        dismissBtn = new JButton("Dismiss");
        dismissBtn.addActionListener(this);
        btnPanel.add(dismissBtn);

        HDBSearchMultiple();

        setTitle("HDB history");
    }

    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if (src == dismissBtn) {
            setVisible(false);
        } else if (src == saveBtn) {
            saveFile();
        } else if (src == copyBtn) {
            copy();
        }
    }

    private String buildSaveString() {

        StringBuilder str = new StringBuilder();

        str.append("HDB Date\tHDB Time\t");
        for (String sigLabel : sigLabels)
            str.append(sigLabel).append("\t");
        str.append("\n");

        for (HdbLine hdbDatum : hdbData) {
            String date = datefr.format(hdbDatum.date);
            String time = timefr.format(hdbDatum.date);
            str.append(date).append("\t").append(time).append("\t");

            for (int j = 0 ; j<sigNames.size() ; j++) {
                int itemPos = hdbDatum.getPosForIdx(j);
                if (itemPos<0) {
                    str.append("\t");
                } else {
                    str.append(hdbDatum.items.get(itemPos).value).append("\t");
                }
            }
            str.append("\n");
        }
        return str.toString();

    }

    private void saveFile() {
        JFileChooser chooser = new JFileChooser(lastDir);
        if (chooser.showSaveDialog(this) != JFileChooser.CANCEL_OPTION) {

            File f = chooser.getSelectedFile();
            lastDir = f.getAbsolutePath();
            try {
                FileWriter fw = new FileWriter(f);
                fw.write(buildSaveString());
                fw.close();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "File Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void copy() {
        StringSelection stringSelection = new StringSelection(buildSaveString());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }

    private void addHDBData(long date, String value, int idx) {
        // Search insertion position
        boolean found = false;
        int i = 0;
        while (i<hdbData.size() && !found) {
            found = date<=hdbData.get(i).date;
            if (!found) i++;
        }

        // Add new item
        HdbItem it = new HdbItem(value, idx);
        HdbLine line;

        if (!found) {
            line = new HdbLine(date);
            hdbData.add(line);
        } else {
            if (date == hdbData.get(i).date) {
                // Same line
                line = hdbData.get(i);
            } else {
                line = new HdbLine(date);
                hdbData.add(i, line);
            }
        }
        line.addItem(it);
    }

    private void HDBSearchMultiple() throws DevFailed {

        // Check parameters syntax
        try {
            HdbSelectionDlg.dateFormat.parse(startDate);
        } catch (java.text.ParseException e1) {
            ErrorPane.showErrorMessage(this, "Error",
                    new Exception("Invalid start date format\nUse dd/mm/yyyy hh:mm:ss"));
            errorFlag = true;
            return;
        }

        try {
            HdbSelectionDlg.dateFormat.parse(stopDate);
        } catch (java.text.ParseException e2) {
            ErrorPane.showErrorMessage(this, "Error",
                    new Exception("Invalid stop date format\nUse dd/mm/yyyy hh:mm:ss"));
            errorFlag = true;
            return;
        }

        //  ToDo replace Taco extraction by HDB++ extraction
        SplashUtils.getInstance().startSplash();
        SplashUtils.getInstance().setSplashProgress(5, "Reading HDB data....");
        SplashUtils.getInstance().startAutoUpdate();

        hdbData = new ArrayList<>();
        int deviceIndex = 0;
        for (String sigName : sigNames) {
            String attributeName = getHdbAttributeHeader() + sigName + "/" + HDB_ATTRIBUTE_NAME;
            System.out.println("Extracting " + attributeName);
            getEventsFromHdb(attributeName, startDate, stopDate, deviceIndex++);

            //  Check if data available
            if (hdbData.isEmpty()) {
                errorFlag = true;
                SplashUtils.getInstance().stopSplash();
                ErrorPane.showErrorMessage(this, "HDB Error", new Exception("No data found"));
                return;
            }
        }

        // Build the table
        String[][] vals = new String[hdbData.size()][sigNames.size() + 1];

        for (int i = 0 ; i<hdbData.size() ; i++) {
            Date dt = new java.util.Date(hdbData.get(i).date);
            vals[i][0] = HdbSelectionDlg.dateFormat.format(dt);
            for (int j = 0 ; j<sigNames.size() ; j++) {

                int itemPos = hdbData.get(i).getPosForIdx(j);
                if (itemPos<0) {
                    vals[i][j + 1] = "";
                } else {
                    vals[i][j + 1] = hdbData.get(i).items.get(itemPos).value;
                }
            }
        }

        String[] colName = new String[sigNames.size() + 1];
        colName[0] = "Date";
        for (int i = 0 ; i<sigLabels.size() ; i++)
            colName[i + 1] = sigLabels.get(i);

        dm.setDataVector(vals, colName);
        editor.updateRows();
        theTable.getColumnModel().getColumn(0).setMaxWidth(140);
        theTable.getColumnModel().getColumn(0).setMinWidth(140);
        theTable.validate();
        SplashUtils.getInstance().stopSplash();
    }

    static void showHdbDlg(JFrame parent) {
        HdbSelectionDlg dlg = new HdbSelectionDlg(parent);
        ATKGraphicsUtils.centerDialog(dlg);
        dlg.setVisible(true);
        if (dlg.okFlag) {

            List<String> names = new ArrayList<>();
            List<String> labels = new ArrayList<>();

            for (int i = 0 ; i<virtualPrinterDevices.length ; i++) {
                if (dlg.isVpSelected(i)) {
                    names.add(virtualPrinterDevices[i]);
                    labels.add(virtualPrinterLabels[i]);
                }
            }

            try {
                HdbDlg f = new HdbDlg(dlg.getStartDate(), dlg.getStopDate(), names, labels);
                if (!f.errorFlag) {
                    ATKGraphicsUtils.centerFrameOnScreen(f);
                    f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    f.setVisible(true);
                }
            } catch (DevFailed e) {
                SplashUtils.getInstance().stopSplash();
                ErrorPane.showErrorMessage(parent, "HDB Error", e);
            }
        }
    }


    //=======================================================
    private static Hdb hdb;
    //=======================================================
    private  void getEventsFromHdb(String fullAttributeName, String  start, String  end, int devIndex) throws DevFailed {
        System.out.println("Reading HDB++ between " + start + "  to  " + end);
        // Use new extractor
        try {
            if (hdb == null) {
                hdb = new Hdb();
                hdb.connect();
            }

            HdbDataSet hdbDataSet = hdb.getReader().getData(fullAttributeName, start, end);
            for (int i = 0; i < hdbDataSet.size(); i++) {
                HdbData hdbData = hdbDataSet.get(i);
                // Do NOT use insertion time
                long time = hdbData.getDataTime() / 1000; //  milliseconds is enough
                //String strTime = Utils.formatDate(time);

                if (hdbData.hasFailed()) {
                    //System.err.println(strTime + ": [ERROR] " + hdbData.getErrorMessage());
                    addHDBData(time, "[ERROR] " + hdbData.getErrorMessage(), devIndex);
                }
                else {
                    String str = hdbData.getValueAsString();
                    if (str.toLowerCase().startsWith("no new event")) {
                        addHDBData(time, str, devIndex);
                    }
                    else {
                        addHDBData(extractDateFromHdbDatum(str), str.substring(16), devIndex);
                    }
                }
            }
        }
        catch (HdbFailed e) {
            if (e.getMessage().contains("Signal not found"))
                Except.throw_exception("HdbFailed", fullAttributeName + "\n" + e.getMessage());
            else {
                //e.printStackTrace();
                Except.throw_exception("HdbFailed", e.getMessage());
            }
        }
    }
    //===============================================================
    /**
     * First part is date and time, the second one is the message
     * @param datum The string extracted from HDB
     * @return the date stored (not the storage date)
     */
    //===============================================================
    private long extractDateFromHdbDatum(String datum) {
        if (datum.length()>=15) {
            String strDate = datum.substring(0, 15);
            try {
                return new SimpleDateFormat("yyMMdd - hhmmss").parse(strDate).getTime();
            }
            catch (ParseException e) {
                System.err.println(e.toString());
            }
        }
        else
            System.err.println("Cannot parse date: " + datum);
        return 0;
    }
    //===============================================================
    private static String hdbAttributeHeader = null;
    //===============================================================
    private static String getHdbAttributeHeader(String tangoHost) {
        return  "tango://" + tangoHost + ':' + TANGO_HOST_PORT + '/';
    }
    //===============================================================
    private static String getHdbAttributeHeader() throws DevFailed {
        if (hdbAttributeHeader ==null) {
            String tangoHost = ApiUtil.getTangoHost();
            //  Check syntax
            int p = tangoHost.indexOf('.');
            if (p<0)
                p = tangoHost.indexOf(':');
            if (p<0)
                Except.throw_exception("SyntaxError", "Bad syntax for TANGO_HOST (host:port)");

            //  Check if it is a TANGO_HOST
            String newTangoHost = ApiUtil.get_db_obj(tangoHost).get_tango_host();

            //  Check if alias
            String hostName = tangoHost.substring(0, p);
            if (newTangoHost.startsWith(hostName + '.')) {
                //  Not an alias
                return getHdbAttributeHeader(newTangoHost.substring(0, newTangoHost.indexOf(':')));
            }
            else {
                //  substitute alias to real name
                p = newTangoHost.indexOf('.');
                if (p>0) {
                    //  With FQDN, can add alias to FQDN
                    hdbAttributeHeader = getHdbAttributeHeader(hostName +
                            newTangoHost.substring(p, newTangoHost.indexOf(':')));
                } else {
                    try {
                        //  Get FQDN and append to alias name
                        String alias = InetAddress.getByName(hostName).getCanonicalHostName();
                        int idx = alias.indexOf('.');
                        String fqdn = "";
                        if (idx>0) {
                            fqdn = alias.substring(idx);
                        }
                        // OK, create the attribute header
                        hdbAttributeHeader = getHdbAttributeHeader(hostName + fqdn);
                    } catch (UnknownHostException e) {
                        Except.throw_exception(e.toString(), e.toString());
                    }
                }
            }
        }
        return hdbAttributeHeader;
    }
    //===============================================================
    //===============================================================


    // Test function
    public static void main(String[] args) {
        HdbDlg.showHdbDlg(null);
    }

}
