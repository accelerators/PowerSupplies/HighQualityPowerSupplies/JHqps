//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsTicket.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: JHqpsTicket.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.3  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.hqps;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.*;
import javax.swing.text.DefaultEditorKit;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.widget.util.*;

/**
 * @author : O. Goudard
 */

public class JHqpsTicket extends javax.swing.JFrame implements JHqpsDefs {

  DeviceProxy devices[] = new DeviceProxy[virtualPrinterDevices.length];
  JTextArea textAreas[] = new JTextArea[virtualPrinterDevices.length];
  JButton updateBtn;
  JButton refreshBtn;
  JButton hdbBtn;

  public JHqpsTicket() {

    try {
      for (int i = 0 ; i < virtualPrinterDevices.length; i++) {
        devices[i] = new DeviceProxy(virtualPrinterDevices[i]);
      }
    } catch (DevFailed e) {
      e.printStackTrace();
    }

    initComponents();
    ATKGraphicsUtils.centerFrameOnScreen(this);
    
  }

  long get_events_nb(DeviceProxy dev) throws DevFailed {

    DeviceAttribute data = dev.read_attribute("EventNb");
    long nb_events = data.extractLong();
    System.out.println(nb_events + " events recorded on " + dev.get_name());
    return nb_events;

  }

  String get_history(DeviceProxy dev) throws DevFailed {

    String history;
    try {
      DeviceData data = dev.command_inout("Read");
      history = data.extractString();
      //System.out.println("History on " + dev.get_name() + history);
    } catch (DevFailed e) {
      return dev.get_name() + ": " + e.errors[0].desc;
    }

    return history;
  }

  void record_history(String filename, DeviceProxy dev) throws DevFailed, IOException {

    String history = this.get_history(dev);
    /**
     * First read file and store result in readLogData
     */
    FileInputStream fidin = new FileInputStream(filename);
    byte[] inStr = new byte[fidin.available()];
    fidin.read(inStr);
    fidin.close();
    /**
     *	Open file and write data
     */
    FileOutputStream fidout = new FileOutputStream(filename);
    byte[] histo_str = history.getBytes();
    fidout.write(histo_str);
    fidout.close();

  }

  void initComponents() {

    String str[] = new String[virtualPrinterDevices.length];

    // Create tabbed pane
    JTabbedPane tabbedPane = new JTabbedPane();

    for (int i = 0; i < devices.length; i++) {

      textAreas[i] = new JTextArea();
      textAreas[i].setToolTipText("To copy data: CTRL-A then right click");
      textAreas[i].addMouseListener(new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
          if (e.getButton() == MouseEvent.BUTTON3) {
            JTextArea src = (JTextArea) e.getSource();
            src.copy();
            JOptionPane.showMessageDialog(src, "Selection copied to clipboard", "Clipboard",
                JOptionPane.INFORMATION_MESSAGE);
          }
          super.mousePressed(e);
        }
      });

      JPanel panel = new JPanel();
      panel.setLayout(new BorderLayout());
      panel.setAutoscrolls(true);

      JScrollPane scroll = new JScrollPane(textAreas[i]);
      panel.add(scroll, BorderLayout.CENTER);


      tabbedPane.addTab(virtualPrinterLabels[i], panel);

    }

    tabbedPane.setPreferredSize(new Dimension(600, 500));
    add(tabbedPane, BorderLayout.CENTER);

    JPanel btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

    hdbBtn = new JButton("HDB");
    hdbBtn.setToolTipText("Get history from HDB");
    hdbBtn.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        hdbActionPerformed(evt);
      }
    });
    btnPanel.add(hdbBtn);

    updateBtn = new JButton("Update event");
    updateBtn.setToolTipText("Send update request to virtual printers");
    updateBtn.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        update_dataActionPerformed(evt);
      }
    });
    btnPanel.add(updateBtn);

    refreshBtn = new JButton("Refresh history");
    refreshBtn.setToolTipText("Reload stored history");
    refreshBtn.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        refresh();
      }
    });
    btnPanel.add(refreshBtn);

    add(btnPanel, BorderLayout.SOUTH);

    refresh();

    setJMenuBar(createMenuBar());
    setTitle("Hqps History");
    setBackground(java.awt.Color.WHITE);
    setPreferredSize(new Dimension(800, 450));
    pack();
    for (int i = 0; i < devices.length; i++) {
        textAreas[i].setCaretPosition(0);
    }
  }

  private void hdbActionPerformed(java.awt.event.ActionEvent evt) {
    HdbDlg.showHdbDlg(this);
  }

  private void refresh() {

    String[] str = new String[devices.length];

    try {
      for (int i = 0; i < devices.length; i++)
        str[i] = get_history(devices[i]);
    } catch (DevFailed e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    updateText(str);

  }

  private void updateText(String[] text) {
    for (int i = 0 ; i < virtualPrinterDevices.length; i++) {
      textAreas[i].setText(text[i]);
      textAreas[i].setCaretPosition(text[i].length());
    }
  }

  private void update_dataActionPerformed(java.awt.event.ActionEvent evt) {

    Splash histo_splash = new Splash();
    histo_splash.setTitle("HQPS History");
    histo_splash.setMessage("Retrieving history");
    int j = 10;
    histo_splash.setVisible(true);
    histo_splash.progress(0);
    String str[] = new String[devices.length];
    for (int i = 0; i < devices.length; i++) str[i] = "";
    String curDevName="";

    try {

      // Set the number of data to retrieve
      for (int i = 0; i < devices.length; i++) {
        curDevName = devices[i].name();
        devices[i].write_attribute(new DeviceAttribute("EventNb", 100));
        System.out.println(devices[i].get_name() + " : set EventsNb to 100.");
        histo_splash.progress(i);
      }

      // Ask for data
      for (int i = 0; i < devices.length; i++) {
        curDevName = devices[i].name();
        devices[i].command_inout("AskEventsData");
        // Wait for data
        histo_splash.setMessage("Waiting for data on " + devices[i].get_name());
        while (devices[i].state() == DevState.MOVING) {
          histo_splash.progress(j);
        }
        j += 10;
      }

      // Read the data
      for (int i = 0; i < devices.length; i++) {
        curDevName = devices[i].name();
        str[i] = get_history(devices[i]);
        histo_splash.setMessage("Reading for data on " + devices[i].get_name());
        histo_splash.progress(9 * (i + 1));
      }

    } catch (DevFailed e) {

      histo_splash.setVisible(false);
      ErrorPane.showErrorMessage(this, "Tango Error on " + curDevName , e);

    }

    //Display the data
    updateText(str);
    histo_splash.setVisible(false);

  }

  /**
   * @param args
   */
  public static void main(String[] args) {

    new JHqpsTicket().setVisible(true);
  }

  /**
   * Create an Edit menu to support cut/copy/paste.
   */
  public JMenuBar createMenuBar() {
    JMenuItem menuItem = null;
    JMenuBar menuBar = new JMenuBar();
    JMenu mainMenu = new JMenu("Edit");
    mainMenu.setMnemonic(KeyEvent.VK_E);

    menuItem = new JMenuItem(new DefaultEditorKit.CutAction());
    menuItem.setText("Cut");
    menuItem.setMnemonic(KeyEvent.VK_T);
    mainMenu.add(menuItem);

    menuItem = new JMenuItem(new DefaultEditorKit.CopyAction());
    menuItem.setText("Copy");
    menuItem.setMnemonic(KeyEvent.VK_C);
    mainMenu.add(menuItem);

    menuItem = new JMenuItem(new DefaultEditorKit.PasteAction());
    menuItem.setText("Paste");
    menuItem.setMnemonic(KeyEvent.VK_P);
    mainMenu.add(menuItem);

    menuBar.add(mainMenu);
    return menuBar;

  }

}
