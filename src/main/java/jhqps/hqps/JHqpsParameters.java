//+======================================================================
//$Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsParameters.java,v $

//Project:   JHqps

//Description:  HQPS java application for CTRM. .

//$Author: goudard $

//$Revision: 1.55 $

//$Log: JHqpsParameters.java,v $
//Revision 1.55  2009/08/25 13:53:22  goudard
//Modifications of HqpsBinIn and HqpsBinOut reading.
//Added :
// - Incoherent state
// - wago uninitialized state
// - conditions to normal and economy mode detection.
//
//Modification of ElecDropStat : isolation curve added.
//
//Revision 1.54  2009/08/25 11:39:18  goudard
//Change behaviour when wago plc is unreachable (network problem)
//
//Revision 1.53  2009/08/18 11:33:50  goudard
//File JHqpsParameters.java added to CVS
//elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//

//Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//All Rights Reversed
//-======================================================================
package jhqps.hqps;

import javax.swing.JFrame;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.widget.attribute.ScalarListViewer;

public class JHqpsParameters {
	ScalarListViewer  viewer = new ScalarListViewer();
	ScalarListViewer viewer2 = new ScalarListViewer();

	public JHqpsParameters(JFrame parent, boolean modal) {
		AttributeList attl1 = new AttributeList();
		AttributeList attl2 = new AttributeList();

		try {
			attl1.add("infra/hqps-common/master/InputActivePower");
			attl1.add("infra/hqps-common/master/OutputActivePower");
			attl1.add("infra/hqps-common/master/HQPSRunningTime");
			attl2.add("infra/power-meter/total/ActivePowerTotal");
			attl2.add("infra/power-meter/GreenAlpConfluentIncoming20kV/FacilityActivePower");
			attl2.add("infra/power-meter/GreenAlpNordOuestIncoming20kV/MachineActivePower");
		}
		catch(ConnectionException e) {
			javax.swing.JOptionPane.showMessageDialog(null, "JHqps : Can't connect the attributes\n"
					+ "GUI will abort.\n", "Failed to connect the attributes",
					javax.swing.JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
		viewer.setModel(attl1);
		viewer2.setModel(attl2);

		attl1.startRefresher();
		attl2.startRefresher();
		initComponents();
	}

	public void initComponents(){
		JFrame param_window = new JFrame();
		javax.swing.JPanel NorthPl = new javax.swing.JPanel();
		NorthPl.setLayout(new java.awt.GridBagLayout());
		java.awt.GridBagConstraints constraints = new java.awt.GridBagConstraints();
		constraints.fill = java.awt.GridBagConstraints.REMAINDER;
		viewer.setTheFont(new java.awt.Font("Tahoma",java.awt.Font.BOLD,15));
		viewer.setPropertyButtonVisible(false);
		constraints.gridx = 1;
		NorthPl.add(viewer, constraints);
		viewer.setToolTipText("HQPS parameters");
		viewer2.setTheFont(new java.awt.Font("Tahoma",java.awt.Font.BOLD,15));
		viewer2.setPropertyButtonVisible(false);
		viewer2.setToolTipText("Active power consumption");
		constraints.gridx = 0;
		NorthPl.add(viewer2, constraints);
		param_window.setTitle("HQPS parameters");
		fr.esrf.tangoatk.widget.util.ATKGraphicsUtils.centerFrameOnScreen(param_window);
		param_window.add(NorthPl);
		param_window.setVisible(true);
		param_window.pack();
	}

}
