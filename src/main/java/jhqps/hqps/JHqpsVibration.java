//+======================================================================
// $Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsVibration.java,v $
//
// Project:   JHqps
//
// Description:  HQPS java application for CTRM. .
//
// $Author: goudard $
//
// $Revision: 1.55 $
//
// $Log: JHqpsVibration.java,v $
// Revision 1.55  2009/08/25 13:53:22  goudard
// Modifications of HqpsBinIn and HqpsBinOut reading.
// Added :
//  - Incoherent state
//  - wago uninitialized state
//  - conditions to normal and economy mode detection.
//
// Modification of ElecDropStat : isolation curve added.
//
// Revision 1.54  2009/08/25 11:39:18  goudard
// Change behaviour when wago plc is unreachable (network problem)
//
// Revision 1.53  2009/08/18 11:33:50  goudard
// File JHqpsParameters.java added to CVS
// elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
// Revision 1.51  2009/06/15 13:43:01  goudard
// Modifications in
// 	- state management of HQPSGlobal state button.
// 	- FFT for jhqps.elecdrops.
// 	- Main Parameters window.
//
// Revision 1.3  2009/01/28 13:11:14  goudard
// Added cvs header.
//
//
// Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//               All Rights Reversed
//-======================================================================
package jhqps.hqps;
/**
 * @author : O. Goudard
 */ 

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.event.ListDataListener;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.IBooleanScalar;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;


public class JHqpsVibration extends JFrame implements JHqpsDefs {
	// Names of the CheckBoxes
	private static final String[] mach_names  = {
			"Genset 1","Genset 2","Machine 4a","Machine 4b","Machine 5a","Machine 5b","Machine 6a",
			"Machine 6b","Machine 7a","Machine 7b","Machine 8a","Machine 8b","Machine 9a","Machine 9b","Machine 10a","Machine 10b"
	};  

	// Device to change sampling period and number of point for FFT average
	private static final String[] sampling_dev = {
			"infra/hqps-vdsp-vib/1","infra/hqps-vdsp-vib/2"
	};
	fr.esrf.TangoApi.DeviceProxy dev;

	private INumberScalar sampling_period_Att = null;
	private INumberScalar FFT_average_Att = null;
	private INumberScalar RMS_Att = null;
	private INumberSpectrum fft_Att = null;
	private AttributeList 	attl ;
	private SimpleScalarViewer sampling_viewer = new SimpleScalarViewer();
	private SimpleScalarViewer average_viewer = new SimpleScalarViewer();
	private SimpleScalarViewer RMS_viewer = new SimpleScalarViewer();
	private JHqpsVibrationViewer viewer = new JHqpsVibrationViewer();

	private JFrame parent;
	public JHqpsVibration(JFrame parent, boolean modal)
	{
		//super(parent, modal);
		this.parent = parent;
		initComponents();
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				closeDialog(evt);
			}
		});
		ATKGraphicsUtils.centerFrameOnScreen(this);
		//ATKGraphicsUtils.centerDialog(this);
	}
	private void closeDialog(WindowEvent event) {
		if (parent==null)
			System.exit(0);
		else {
			setVisible(false);
			dispose();
		}
	}


	public void initComponents()
	{
		attl = new AttributeList();
		errorHistory = new fr.esrf.tangoatk.widget.util.ErrorHistory();	

		// addErrorListener must be done before registering to the attributes or commands
		attl.addErrorListener(errorHistory);
		attl.addSetErrorListener(errorHistory);
		attl.addSetErrorListener(fr.esrf.tangoatk.widget.util.ErrorPopup.getInstance());

		//average_viewer.setFormat("%3d");
		//sampling_viewer.setFormat("%3d");
		average_viewer.setBackground(java.awt.Color.WHITE);
		RMS_viewer.setBackground(java.awt.Color.WHITE);
		sampling_viewer.setBackground(java.awt.Color.WHITE);
		samp_period = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
		samp_period.setText("Sampling period");
		fft_average = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
		fft_average.setText("Averaging on");
		vibrationRMS = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
		vibrationRMS.setText("RMS vibration");
		vibrationRMSHistory = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
		vibrationRMSHistory.setText("RMS History");

		menu_bar 			= new javax.swing.JMenuBar();
		error_item			= new javax.swing.JMenuItem("Error History");
		view_menu			= new javax.swing.JMenu("View");
		history_menu		= new javax.swing.JMenu("FFT History");
		history_view		= new javax.swing.JMenuItem("History viewer");
		history_menu.add(history_view);
		view_menu.add(error_item);
		menu_bar.add(view_menu);
		menu_bar.add(history_menu);


		error_item.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				error_itemActionPerformed(evt);
			}
			private void error_itemActionPerformed(ActionEvent evt) {
				errorHistory.setVisible(true);
			}
		});

		history_view.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				history_menuActionPerformed(evt);
			}
		});

		//Set up WEST Panel
		javax.swing.JPanel WestPl = new javax.swing.JPanel();
		java.awt.GridLayout layout = new java.awt.GridLayout();
		layout.setColumns(1);layout.setRows(14);
		WestPl.setLayout(layout);
		WestPl.setBackground(java.awt.Color.WHITE);
		mach_number = new fr.esrf.tangoatk.widget.attribute.BooleanScalarCheckBoxViewer[NB_MACHINE];

		for(int i=0 ; i < NB_MACHINE ; i++)
		{
			mach_number[i] = new fr.esrf.tangoatk.widget.attribute.BooleanScalarCheckBoxViewer(mach_names[i]);
			WestPl.add(mach_number[i], java.awt.BorderLayout.WEST);
		}

		mach_number[0].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genset1ActionPerformed(evt);
			}
			private void genset1ActionPerformed(ActionEvent evt) {
				System.out.println("Action performed in genset1.");
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/1a/VibrationFFT");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/4a/VibrationRMS");

				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				//sampling_viewer.setFormat("%3d");

				sampling_viewer.setModel(sampling_period_Att);
				average_viewer.setModel(FFT_average_Att);
				//sampling_viewer.
				viewer.setModel(fft_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on GENSET 1");
				for(int i=1 ; i< NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});

		mach_number[1].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genset2ActionPerformed(evt);
			}

			private void genset2ActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/1a/VibrationFFT");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib-test/4a/VibrationRMS");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				viewer.setModel(fft_Att);
				RMS_viewer.setModel(RMS_Att);
				average_viewer.setModel(FFT_average_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on GENSET 1");
				for(int i=2 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);

			}
		});
		mach_number[2].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quatreaActionPerformed(evt);
			}
			private void quatreaActionPerformed(ActionEvent evt) {

				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/4a/VibrationFFT");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/4a/VibrationRMS");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				//sampling_viewer.setFormat("%3d");
				RMS_viewer.setModel(RMS_Att);
				sampling_viewer.setModel(sampling_period_Att);

				viewer.setModel(fft_Att);
				average_viewer.setModel(FFT_average_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 4a");

				for(int i=3 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);

				viewer.setXaxisModels(sampling_period_Att);


			}
		});

		mach_number[3].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				quatrebActionPerformed(evt);
			}
			private void quatrebActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/4b/VibrationFFT");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/FFTAverage");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/4b/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/SamplingPeriod");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				viewer.setModel(fft_Att);
				average_viewer.setModel(FFT_average_Att);
				RMS_viewer.setModel(RMS_Att);
				sampling_viewer.setModel(sampling_period_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 4b");
				for(int i=4 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);
				mach_number[2].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);

			}
		});


		mach_number[4].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cinqaActionPerformed(evt);
			}
			private void cinqaActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/5a/VibrationFFT");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/5a/VibrationRMS");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				RMS_viewer.setModel(RMS_Att);
				average_viewer.setModel(FFT_average_Att);
				viewer.setModel(fft_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 5a");
				for(int i=5 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);
				mach_number[2].setSelected(false);
				mach_number[3].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);

			}
		});

		mach_number[5].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cinqbActionPerformed(evt);
			}
			private void cinqbActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/5b/VibrationFFT");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/FFTAverage");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/5b/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/SamplingPeriod");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				RMS_viewer.setModel(RMS_Att);
				viewer.setModel(fft_Att);
				average_viewer.setModel(FFT_average_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 5b");
				for(int i=6 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);
				mach_number[2].setSelected(false);
				mach_number[3].setSelected(false);
				mach_number[4].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[6].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sixaActionPerformed(evt);
			}
			private void sixaActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/6a/VibrationFFT");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/6a/VibrationRMS");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				average_viewer.setModel(FFT_average_Att);
				RMS_viewer.setModel(RMS_Att);
				viewer.setModel(fft_Att);
				sampling_viewer.setModel(sampling_period_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 6a");
				for(int i=7 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);
				mach_number[2].setSelected(false);
				mach_number[3].setSelected(false);
				mach_number[4].setSelected(false);
				mach_number[5].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[7].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sixbActionPerformed(evt);
			}
			private void sixbActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				ListDataListener [] l = attl.getListDataListeners();
				for(int i=0 ; i < l.length ; i++)
					attl.removeListDataListener(l[i]);
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/6b/VibrationFFT");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/6b/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/SamplingPeriod");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/FFTAverage");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				average_viewer.setModel(FFT_average_Att);
				RMS_viewer.setModel(RMS_Att);
				viewer.setModel(fft_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 6b");
				for(int i=8 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);
				mach_number[2].setSelected(false);
				mach_number[3].setSelected(false);
				mach_number[4].setSelected(false);
				mach_number[5].setSelected(false);
				mach_number[6].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[8].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				septaActionPerformed(evt);
			}
			private void septaActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/7a/VibrationFFT");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/7a/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				average_viewer.setModel(FFT_average_Att);
				viewer.setModel(fft_Att);
				RMS_viewer.setModel(RMS_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 7a");
				for(int i=9 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);
				mach_number[2].setSelected(false);
				mach_number[3].setSelected(false);
				mach_number[4].setSelected(false);
				mach_number[5].setSelected(false);
				mach_number[6].setSelected(false);
				mach_number[7].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[9].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				septbActionPerformed(evt);
			}
			private void septbActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/FFTAverage");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/7b/VibrationRMS");
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/7b/VibrationFFT");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/SamplingPeriod");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				RMS_viewer.setModel(RMS_Att);
				average_viewer.setModel(FFT_average_Att);
				viewer.setModel(fft_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 7b");
				for(int i=10 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				mach_number[0].setSelected(false);
				mach_number[1].setSelected(false);
				mach_number[2].setSelected(false);
				mach_number[3].setSelected(false);
				mach_number[4].setSelected(false);
				mach_number[5].setSelected(false);
				mach_number[6].setSelected(false);
				mach_number[7].setSelected(false);
				mach_number[8].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[10].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				huitaActionPerformed(evt);
			}
			private void huitaActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/8a/VibrationFFT");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/8a/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				RMS_viewer.setModel(RMS_Att);
				viewer.setModel(fft_Att);
				average_viewer.setModel(FFT_average_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 8a");
				for(int i=11 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				for(int i = 0 ; i < 10 ; i++)
					mach_number[i].setSelected(false);
			}
		});
		mach_number[11].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				huitbActionPerformed(evt);
			}
			private void huitbActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/FFTAverage");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/SamplingPeriod");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/8b/VibrationRMS");
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/8b/VibrationFFT");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				RMS_viewer.setModel(RMS_Att);
				average_viewer.setModel(FFT_average_Att);
				viewer.setModel(fft_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 8b");
				for(int i=12 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				for(int i = 0 ; i < 11 ; i++)
					mach_number[i].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[12].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				neufaActionPerformed(evt);
			}
			private void neufaActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/9a/VibrationFFT");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/9a/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				viewer.setModel(fft_Att);
				RMS_viewer.setModel(RMS_Att);
				average_viewer.setModel(FFT_average_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 9a");
				for(int i=13 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				for(int i = 0 ; i < 12 ; i++)
					mach_number[i].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[13].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				neufbActionPerformed(evt);
			}
			private void neufbActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/9b/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/SamplingPeriod");
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/9b/VibrationFFT");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/FFTAverage");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				average_viewer.setModel(FFT_average_Att);
				viewer.setModel(fft_Att);
				sampling_viewer.setModel(sampling_period_Att);
				RMS_viewer.setModel(RMS_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 9b");
				for(int i=14 ; i<NB_MACHINE ; i++)
					mach_number[i].setSelected(false);
				for(int i = 0 ; i < 13 ; i++)
					mach_number[i].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[14].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dixaActionPerformed(evt);
			}
			private void dixaActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/10a/VibrationFFT");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/10a/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/SamplingPeriod");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/1/FFTAverage");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				sampling_viewer.setModel(sampling_period_Att);
				average_viewer.setModel(FFT_average_Att);
				viewer.setModel(fft_Att);
				RMS_viewer.setModel(RMS_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 10a");
				mach_number[NB_MACHINE-1].setSelected(false);
				for(int i = 0 ; i < 13 ; i++)
					mach_number[i].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});
		mach_number[15].addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dixbActionPerformed(evt);
			}
			private void dixbActionPerformed(ActionEvent evt) {
				attl.stopRefresher();
				attl.removeAllElements();
				try {
					fft_Att = (INumberSpectrum)attl.add("infra/hqps-vib/10b/VibrationFFT");
					RMS_Att = (INumberScalar)attl.add("infra/hqps-vib/10b/VibrationRMS");
					sampling_period_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/SamplingPeriod");
					FFT_average_Att = (INumberScalar)attl.add("infra/hqps-vdsp-vib/2/FFTAverage");
				}
				catch(ConnectionException e){
					javax.swing.JOptionPane.showMessageDialog(null, "Can't connect the attributes\n"
							+ "GUI will abort.\n", "Failed to connect the attributes",
							javax.swing.JOptionPane.ERROR_MESSAGE);
				}
				average_viewer.setModel(FFT_average_Att);
				RMS_viewer.setModel(RMS_Att);
				viewer.setModel(fft_Att);
				sampling_viewer.setModel(sampling_period_Att);
				attl.startRefresher();
				viewer.setHeader("Vibration spectrum on ROTABLOC 10b");
				mach_number[14].setSelected(false);
				for(int i = 0 ; i < NB_MACHINE-1 ; i++)
					mach_number[i].setSelected(false);
				viewer.setXaxisModels(sampling_period_Att);
			}
		});

		for(int i=0 ; i < NB_MACHINE; i++)
			mach_number[i].setBackground(java.awt.Color.WHITE);

		//Set up North Panel
		javax.swing.JPanel NorthPl = new javax.swing.JPanel();
		java.awt.GridLayout northlayout = new java.awt.GridLayout();
		northlayout.setRows(1);
		NorthPl.setLayout(northlayout);
		this.setJMenuBar(menu_bar);

		NorthPl.add(samp_period);
		NorthPl.add(sampling_viewer);
		NorthPl.add(fft_average);
		NorthPl.add(average_viewer);
		NorthPl.add(vibrationRMS);
		RMS_viewer.setBackground(java.awt.Color.WHITE);

		NorthPl.add(RMS_viewer);

		//Set up South Panel
		viewer.setPreferredSize(new java.awt.Dimension(200,200));
		viewer.setBackground(java.awt.Color.WHITE);
		viewer.setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
		viewer.setPreferredSize(new java.awt.Dimension(750,300));
		viewer.setXAxisUnit("Hertz");
		viewer.getXAxis().setGridVisible(true);
		viewer.getXAxis().setSubGridVisible(true);
		viewer.setUnitVisible(true);
		viewer.setPaintAxisFirst(true);

		average_viewer.setUnitVisible(true);
		RMS_viewer.setUnitVisible(true);
		sampling_viewer.setUnitVisible(true);
		average_viewer.setBackgroundColor(java.awt.Color.WHITE);
		RMS_viewer.setBackgroundColor(java.awt.Color.WHITE);
		sampling_viewer.setBackgroundColor(java.awt.Color.WHITE);

		setTitle("Mechanical vibration analysis window");
		setLayout(new java.awt.BorderLayout());
		getContentPane().add(WestPl, java.awt.BorderLayout.WEST);
		getContentPane().add(NorthPl, java.awt.BorderLayout.NORTH);
		getContentPane().add(viewer, java.awt.BorderLayout.CENTER);
		pack();
	}
	private void history_menuActionPerformed(ActionEvent evt) {
	
	}
	fr.esrf.tangoatk.widget.util.JSmoothLabel samp_period;
	fr.esrf.tangoatk.widget.util.JSmoothLabel fft_average;
	fr.esrf.tangoatk.widget.util.JSmoothLabel vibrationRMS ;
	fr.esrf.tangoatk.widget.util.JSmoothLabel vibrationRMSHistory;
	fr.esrf.tangoatk.widget.attribute.BooleanScalarCheckBoxViewer [] mach_number;
	fr.esrf.tangoatk.widget.util.ErrorHistory errorHistory;
	javax.swing.JMenuBar menu_bar;
	javax.swing.JMenu view_menu;	
	javax.swing.JMenuItem error_item;
	javax.swing.JMenu history_menu;	
	javax.swing.JMenuItem history_view;	

	public static void main(String[] args) {
		new JHqpsVibration(null, false).setVisible(true);
	}
}
