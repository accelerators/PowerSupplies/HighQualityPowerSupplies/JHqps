//+======================================================================
//$Source: /segfs/tango/cvsroot/jclient/jhqps/JHqpsFFTHisto_3D.java,v $

//Project:   JHqps

//Description:  HQPS java application for CTRM. .

//$Author: goudard $

//$Revision: 1.55 $

//$Log: JHqpsFFTHisto_3D.java,v $
//Revision 1.55  2009/08/25 13:53:22  goudard
//Modifications of HqpsBinIn and HqpsBinOut reading.
//Added :
// - Incoherent state
// - wago uninitialized state
// - conditions to normal and economy mode detection.
//
//Modification of ElecDropStat : isolation curve added.
//
//Revision 1.54  2009/08/25 11:39:18  goudard
//Change behaviour when wago plc is unreachable (network problem)
//
//Revision 1.53  2009/08/18 11:33:50  goudard
//File JHqpsParameters.java added to CVS
//elecdrops Live Mode window not modal anymore to be able to set RMS Chart options.
//
//Revision 1.51  2009/06/15 13:43:01  goudard
//Modifications in
//	- state management of HQPSGlobal state button.
//	- FFT for jhqps.elecdrops.
//	- Main Parameters window.
//
//Revision 1.4  2009/01/30 15:26:49  goudard
//In drop appli :
// - Correct -1 in DropDisplay.
//In vibration History :
// - Gradient correct setup
// - Correct scale between 9.81mg and maximum
//Check HqpsBinIn to display Hqps mode.
//
//Revision 1.3  2009/01/28 13:11:14  goudard
//Added cvs header.


//Copyleft 2008 by European Synchrotron Radiation Facility, Grenoble, France
//All Rights Reversed
//-======================================================================
package jhqps.hqps;
/**
 * @author : O. Goudard
 */ 

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;

//import tool_panels.spectrum_histo.Histo_3D._3D_Record;

import fr.esrf.tangoatk.widget.util.Gradient;

public class JHqpsFFTHisto_3D extends tool_panels.spectrum_histo.Histo_3D
{
	private static final	int	DefaultMaxSize = 1200;
	private Gradient	gradient;

	//===============================================================
	/**
	 *	Creates new Image Viewer to be used as a 3D history viewer
	 *  @param parent       Parent component.
	 */
	//===============================================================
	public JHqpsFFTHisto_3D(Component parent)
	{
		super(parent, DefaultMaxSize);
		setGradient();
		this.setZoom(2);
	}
	//===============================================================
	/**
	 *	Creates new Image Viewer to be used as a 3D history viewer
	 *  @param parent       Parent component.
	 *  @param nbMeasMax	Nb measure maximum
	 */
	//===============================================================
	public JHqpsFFTHisto_3D(Component parent, int nbMeasMax)
	{
		super(parent, nbMeasMax);
		setGradient();
		this.setZoom(2);

	}
	//===============================================================
	//===============================================================

	protected void setGradient()
	{
		gradient = super.getGradient();
		gradient.removeEntry(4);	
		gradient.removeEntry(3);
		gradient.addEntry(Color.green,0.5);
		gradient.addEntry(Color.green,0.25);
		gradient.addEntry(Color.yellow,0.75);
		super.setGradient(gradient);

	}
	//===============================================================
	//===============================================================
	protected void displayHistoValues(Point p, _3D_Record rec)
	{
		if (rec==null)
			return;

		//  And build message to display
		String	strval = String.format("%.3f",getData(p.x, p.y));

		String  strdate = formatDate(rec.time);
		String  str;
		int	idx =  p.y*2;// / y_step; -> should be f(sampling period).

		str = strdate + " | Vibration :  " + strval + " mg at  " + idx + " Hz";
		setMarkerText(str);

	}

}