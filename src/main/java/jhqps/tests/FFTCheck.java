//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,...................,2017,2018
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package jhqps.tests;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevSource;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import fr.esrf.TangoDs.TangoConst;


/**
 * This class is able to
 *
 * @author verdier
 */

public class FFTCheck {
    private DeviceProxy originProxy;
    private DeviceProxy testProxy;
    private float[] originData;
    private float[] testData;
    private static final String FFT_ATTRIBUTE = "VibrationFFT";
    private static final int ORIGIN = 0;
    private static final int TEST = 1;
    //===============================================================
    //===============================================================
    public FFTCheck(String originName, String testName) throws DevFailed {
        originProxy = new DeviceProxy(originName);
        testProxy = new DeviceProxy(testName);

        originProxy.set_source(DevSource.DEV);
        testProxy.set_source(DevSource.DEV);
    }
    //===============================================================
    //===============================================================
    public void readData(int source) throws DevFailed {
        long t0 = System.currentTimeMillis();
        DeviceProxy proxy = (source==ORIGIN)? originProxy : testProxy;
        DeviceAttribute attribute = proxy.read_attribute(FFT_ATTRIBUTE);
        if (source==ORIGIN)
            originData = attribute.extractFloatArray();
        else
            testData = attribute.extractFloatArray();
        long t1 = System.currentTimeMillis();
        System.out.println("elapsed time : " + (t1 - t0) + " ms");
    }
    //===============================================================
    //===============================================================
    public void displayState() throws DevFailed {
        DevState st = originProxy.state();
        System.out.println(originProxy.name() + " is : " +
                TangoConst.Tango_DevStateName[st.value()]);
        st = testProxy.state();
        System.out.println(testProxy.name() + " is : " +
                TangoConst.Tango_DevStateName[st.value()]);
    }
    //===============================================================
    //===============================================================
    private void displayData() {
        StringBuilder sb = new StringBuilder();
        for (int i=0 ; i<originData.length && i<testData.length ; i++) {
            if (Math.abs(originData[i]-testData[i])>0.001)
                sb.append(i).append(":\t").append(originData[i]).append("\t").append(testData[i]).append('\n');
        }
        if (sb.length()>0)
            System.out.println(sb);
        else
            System.out.println("FFT is not different !");
    }
    //===============================================================
    //===============================================================

    //===============================================================
    //===============================================================
    public static void main(String[] args) {

        String originName = "infra/hqps-vib/4a";
        String testName = "infra/hqps-vib-test/4a";

        try {
            FFTCheck fftCheck = new FFTCheck(originName, testName);
            fftCheck.displayState();
            fftCheck.readData(ORIGIN);
            fftCheck.readData(TEST);
            fftCheck.displayData();
        } catch (DevFailed e) {
            Except.print_exception(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
